# Ejercicios

Peter menja fruita cada dia
Peter eats fruit every day (present)

Peter no menja fruita cada dia
Peter doesn't eat fruit every day (present -)

Peter està menjant fruita,
Peter is eating fruit, (present continuous)

Peter pot menjar fruita.
Peter can eat fruit. ( present modal)

Peter va menjar fruita.
Peter ate fruit (past simple )

Peter no va menjar fruita.
Peter didn't eat fruit. (Past simple -)

Peter va menjar fruita?
Did Peter eat fruit? (Past simple ?)

Peter no estava menjant.
Peter wasn't eating (past continuous)

Peter ha menjat fruita.
Peter has eaten fruit (present perfect)

Peter ha estat menjant fruita
Peter has been eating fruit (present perfect continuous)

Peter ha menjat fruita?
Has Peter eaten fruit? (Present perfect continuous?)

Peter no havia menjat fruita.
Peter hadn't eaten fruit (past perfect -)

Peter havia estat menjant fruita.
Peter had been eating fruit (past perfect continuous)

Peter menjaria fruita.
Peter would eat fruit (modal)

Peter mejarà fruita.
Peter will eat fruit (future simple