# Les Subordinades Substantives
Si no hi ha preposicions pel mitj una oració subordinada substantiva pot fer les mateixes funcions que un substantiu. Si li posem preposicions pot fer de complement de regim verbal i de complement indirecte. La clau está en si porta preposició o si no la porta.

La preposició pot anar amagada:

> Pensa en les conseqüencia --> Pensa que demà no podrà dinar

Hi ha quatre tipus:
1. Subordinades substantives completives.
2. Subordinades substantives d'infinitiu.
3. Subordinades substantives interrogatives.
4. Subordinades substantives de relatiu.
<br>

## Substantives completives
Els nexes que utilitza són que i si, i com substantives, aquests neces no tenen cap funció dins de la preposició subordinada. 

Les funcions qeu fa poden ser les de qualsevol nom:
- No m'agrada *que surtis amb aquest noi* (Subjecte)
- Entenc *que vinguis tard a casa* -> Ho entenc (Complement Directe)
- *Que aproveu* mostra un bon rendiment (Subjecte)
- No s'adona *que tot depèn d'ell* (CRV) <- **Fenomen de caiguda de preposicions**


### Canvi i caiguda de preposicions
**SP = prep + N** (la preposició no varia) (Accedir a la casa)
**SP = EN + Subordinada d'infinitiu** (canvia a A/De) (Confial en el teu company // Confiar a guanyar) (Això nomès afecta regims verbals)
**SP = prep + QUE conjunció** (Caiguda de preposicions, totes) (Dubto de les teves possibilitats // Dubto que puguis fer-ho).

Si cae regim verbal, si no cau CD.

## Les Substantives d'infinitiu
Aquestes subordinades tenen coma nexe infinitiu, que alhora fa de verb de la proposició. La funció pot ser la de qualsevol substantiu.

Només pensa a *controlar-ho tot* (CRV, amb canvi de preposició)
M'agrada *venir a casa teva* (Subjecte)
Voldria *venir a casa teva* (CD)


## Les substantives interrogatives
Les suborindades substantives són aquelles proposicions introduïdes per un pronom interrogatiu (què, si, qui, quan, quan, on, com i quin). També fa funció dins de la subordinada, poden fer de CD, CRV, CN i Cadj. 


## Les substantives de relatiu
