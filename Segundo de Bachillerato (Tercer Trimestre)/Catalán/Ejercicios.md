# Ejercicios Catalán

10. página 115
	1. Hi ha caiguda de preposició. CRV.
	2. No hi ha caiguda. CD.
	3. Hi ha caiguda de preposició. CRV.
	4. No hi ha caiguda. CD.
	5. Hi ha caiguda de preposició. CRV.
	6. No hi ha caiguda. CD.
	7. Hi ha caiguda de preposició. CRV.
	8. Hi ha caiguda de preposició. CRV.
	
11. pàgina 116
	1. o

## Exercici Pissarra

1. pissarra
	1. _que hi havia un jove amb ella_, Completiva, CD
	2. _que ho diguessis tu_, Completiva, Completiva, Subjecte
	3. _que aprovis les oposicions_, Completiva, Atribut
	4. _qui t'ho ha dit_, Interrogativa, CD
	5. _si demà farà_, Interrogativa, 