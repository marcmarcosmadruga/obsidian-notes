# La Plaça del Diamant
## CAP 1
En el primer capitol sen's presenta a la Natalia que treballa en una pastisseria i després d'estar tot el dia treballant s'en va amb la seva amiga Julieta a ballar a la placa del Diamant, tot i que en principi Natalia no vol. Quan arriba a la plaça es posa a ballar i un home se li apropa per demanar-li que balli amb ell, ella li diu que no, que està promesa i l'home li diu que li esperi que ha perdut l'americana. Llavors Natalia es posa a ballar i l'home torna i al veure-la ballar li que ell sabia que podia ballar. Llavors Natalia fuig i i l'home corre darrere seu. Tot i que la Natalia li diu com es diu ell li diu que nomès es pot dir d'una manera, Colometa. De tant correr se li cauen els enagos i al final del capítol es fa una referencia com que el home anys després encara explica com va coneixer a la Colometa.

## CAP 2
En aquest segon capítol la Natalia i en Quimet han quedat per passejar. El Quimet arriba tard i abans de que ell arribi a la Natalia se li insinua un noi. Després ells dos van anar a fer un passeig. El Quimet sempre li preguntava si havia tingut problemes amb el Pere i avui justament, que si que n'havia tingut, no li va preguntar. Van veure un merlot negre i el Quimet, que es nota que era molt supersticiós, va dir que era mostra de desgracia. Llavors el Quimet li va dir a la Natalia que tot el que li agradava a ell li havia d'agradar per força també a ella, perquè ella no hi entenia. Llavors li va fer dos petons a la Natalia però li va dir que no havien d'haver vingut perquè el merlot estava xiulant. Van mirar a una nena i estava tocant una estrella de vuit puntes. Mentre baixaven el Quimet no parava de repetir-li que *Pobra Maria* sense donar-li gaire més explicació.

## CAP 3
En aquest tercer capítol el Quimet li diu a la Natalia que no vol que trevalli més com a pastissera, que ha vist al pastisser mirant-li el cul i que ell no vol. La Natalia llavors li diu que si li té confiança que la deixi i que si no, no ha valgut la pena. Es passen tres setmanes sense parlar i després la Natalia va a coneixer a la mare del Quimet. Ella té tota la casa plena de llaços i quan se'n van la mare li pregunta que si li agraden les feines de casa, i que ja li sembla be que li agradin. El Quimet li torna a recordar que la pobra Maria se'n va anar a passeig per ella (entenc que la Maria es la exparella del Quimet).

## CAP 4
En el quart capítol la Natalia està escoltant a la senyora Enriqueta, que li dona bons consells. Després van a veure el pis amb en Quimet i com volen canviar coses doncs canvien el paper de la pared del menjador i la cuina. El Quimet s'escaqueja per no treballar i després posa una excusa. Dies després es torna a enfadar pel pastisser i li diu a la Natalia que l'ha vista passejar amb en Pere. Ella ho nega, perquè es mentida, però acaba donant-li la raó per tal de que calli, llavors el Quimet s'enfada encara més dient-li que li ha posat una trampa i ha caigut.

## CAP 5
En el cinqué capítol el pare de la Natalia li diu de convidar al Quimet a dinar per coneixer-lo però aquest es nega. Després s'en van a dinar a casa de la mare del Quimet que està acollint a una veïna que s'ha discutit amb el seu marit. El Quimet s'hi fica i comença a desvariar parlant sobre sal i el dimoni. Després s'en van al pis a acabar-lo i es troben amb una taca d'humitat. Discuteixen amb el veí i amb els propietaris sobre qui l¡haurà de pagar, i no aclareixen res.

## CAP 6
En el sisé capitol Quimet i Natalia van a visitar al mossén Joan i aquest li diu a Quimet que el casament es una cosa important i que més val fer-la amb paciencia. De camí a veure'l el Quimet li diu a Natalia que hauria de pagar el lloguer a mitges, i aquesta accepta tot i que li costa de convencer al seu pare. El Cintent els va regalar la llum del menjador. El dia de la ceremonia va asistire l pare de la Natalia que la va acompañar fins a l'altar i a la tarda van anar a ballar i se'ls hi van unir uns senyors que celebraven una operació que havia anat be. El camarer li va demanar de ballar a la nuvia perquè tenia tradició de fer-ho perquè deia que li donava sort.

## CAP 7
En el seté capítol ja sen's mostra a Quimet i Natalia ja portant dos mesos de casats. Aquest se l'emporta amb la moto a voltar pel litoral i encara li nomena a la pobre Maria. en aquell mateix dia a la Natalia li sagna el nas i el cambrer l'acompanya al lavabo, això no agrada al Quimet. Això li senta molt malament a la Natalia. Un dia el Quimet porta a la Natalia a seva botiga i li presenta a l'aprenent (encara que ja es coneixen)

## CAP 8
El vuité capítol comença amb la historia sobre una cadira que habia construir el Quimet. Era mitj cadira, mitj butaca, mitj taburet, mitj balanci. Ell deia que era una cadira d'homes. La senyora Enriqueta no es porta massa bé amb la mare del Quimet i sempre fa preguntes sobre la nit de nuvis de la Natalia i del Quimet. El que no sap es que no va ser nit de nuvis, sinó setmana de nit de nuvis, perquè el Quimet va fer anar a la Natalia a per provisions i van estar una setmana. La Natalia tenia molta por de mantenir relacions perquè li havien dit que les dones morien partides.

## CAP 9
En aquest capítol la Natalia, el Quimet i el Cintet perden les claus de casa i han d'anar al taller a per eines per fer un forat a la porta i poder obrir desde dins. El Quimet per tal de que la Natalia estiguès sempre per ell li deia que tenia un dolor que nomès li fei mal per la nit i feia que li donès massatges per tot el cos per tal de que li marxès el dolor. Un dia, el dia de la festa major, la Natalia s'en va anar de casa per no haver d'aguantar-lo i es va trobar al Pere, el seu antic promès. Quan li va explicar al Quimet no li va agradar gaire.

## CAP 10
En aquest capítol Natalia ja està embarassada. Parla amb la mare del Quimet sobre la seva infancia i la senyora Enriqueta li dona consells per al dolor del Quimet, ella diu que té tuberculosi dels ossos.

## CAP 11
En aquest capítol neix el seu fill i tot i Natalia casi l'escanya quan estava parint. Tot i que el nen pesa 4 quilos comença molt ràpid a perdre pes perquè no vol llet ni aigua ni suc de taronja. Tothom pensa que es morirà però tampoc li donen gaire importancia. Al final del capítol però, comença a mamar llet.

## CAP 12
El nen es diu Antoni i continuen els dolors del Quimet, Enriqueta continua dient que ella no es creu res del seu dolor. Arriba un colom ferit a casa i l'intenten curar. Anaven a construir una gavia pero acaben construint un colomar i compren un altre colom perquè s'aparellin.

## CAP 13
En aquest capítol comencen a construir el palomar i a l'hora de pintar li toca a la Natalia perquè el Quimet té molta feina sempre els diumenges. Aprofitant també li fan pintar la barana. Els coloms son feliços. Al cap d'una setmana el Quimet porta una parella de coloms monja al que diuen Monjo i Monja. El colomar el pinten de blau. La setmana després porten a dos coloms de cua de gall d'indi.

## CAP 14
S'ens descriu en aquest capítol la plaça de ventes on hi ha una peixateria, una carnisseria i una hortalera. En aquest moment es proclama la república i el Quimet ho celebra anant amb una bandera pel carrer. El Cintent i el Mateu van a casa del Quimet i la Natalia i parlen sobre els rumors que envolten al rei. El Mateu està enamorat de la Griselda i s'hi casen. Llavors el Quimet els porta al colomar i el Quimet diu d'obrir la porta perquè volin els coloms, al Quimet no li fa gracia però la obre igual, els coloms volen i després tornen. La Natalia ja no pot estendre la roba perquè els coloms li taquen tota.

## CAP 15
El Quimet s'emporta a l'Antoni a fer voltes amb la moto, la Natalia pateix per si es fan mal. Al cap d'un any i mitj la Natalia es torna a quedar embarassada. Pateix un embaràs molt dolent i casi es mor al part. És una noia i li posen Rita. l'Antoni està molt celós de la Rita i quasi la mata dues vegades. L'Antoni també imita al seu pare que es torna a queixar de dolors.

## CAP 16
El Quimet diu que té engunia i resulta ser que té un cuc. El diumenge es pren el remei i després d'un parell d'hores de voltar marejat per la casa el treu per la boca. El cuc mesurava quinze metres (o això diu) i el van guardar a un pot de confitures. Al final del capítol l'Antoni tira el cuc a la seva germana i el Quimet s'enfada molt i el vol pegar, però la Natalia el tranquilitza.

## CAP 17
La feina del Quimet no va bé perquè en aquell temps la gent no estava per restaurar mobles ni per fer-ne de nous. Llavors la Natalia, al veure que ella i els seus fills passaven gana, va parlar amb la senyora Enriqueta que li va dir que hi havia una casa on buscaven una dona per fer feines els dematins. Va anar a aquesta casa i li van dir que li pagarien a 3 rals l'hora (3/4 d'una peseta) i que si volia que li paguessin les 4 hores de cop, hauriend de ser 10 rals, que li farien rebaixa.

## CAP 18
En aquest capítol li ensenyen tota la casa. Es una casa molt gran i laberíntica. Veuen un portaparaigües i la Natalia diu que si el seu marit la veiés s'enamoraria. La mare li explica que representa el dilema, l'amor. 

## CAP 19
El primer dia de treball de la Natalia. Renta els plats i espolsa la pols però la casa es queda sense aigua. Pujen a veure que li ha passat al diposit però sospiten que es cosa del diposit del carrer i potser trigaran tres dies a arreglar-ho. Llavors la señora li diu que porti aigua del pou, que no poden estar tres dies amb els plats bruts. La Natalia acaba de treballar i torna corrents a casa, al arribar els nens no li han fet cap destrossa, la Rita està dormint.

## CAP 20
L'home de la companyía repara l'aigua i ja hi torna haver aigua a la casa. La senyora li explica a la Natalia que el seu gendre havia tingut una enganchada amb el llogater que tenien, perquè li havien intentat pujar el lloguer i havia reaccionat de mala manera. Una parella arriba per l'oferta de la torre però com tenen nens no els accepten i després arriba un noi sense fill que sembla si estar interesat. Un dia el Quimet va a recollir a la Natalia al treball i passen per l'adroguer a buscar veces.

## CAP 21
La cria de coloms continua i un dia la Natalia torna de treballar i es troba a l'Antoni i la Rita dormint al terra. S'ensuma una cosa extranya i al dia següent torna abans per veure que fan, resulta que deixen entrar els coloms al pis i es per això que no fan escandol. El Quimet al enterar-se diu de construir una trepa per tal de que els coloms vagin de dalt a baix del pis sense necessitat de passar per la finestra. Comencen a fer calculs per veure quan caldria per poder viure nomès de criar coloms. 

## CAP 22
La Natalia ja està farta dels coloms pero no li diu a ningú, s'ho empassa ella sola. Quan va al treball nomès pensa en els coloms, olora a colom. Un dia reben la visita de la mare del Quimet i li sopta que tinguin els coloms tan dins de casa. Ja quasi no veuen a la mare del Quimet perquè la Natalia no té temps i  es va fent vella la mare del Quimet.

## CAP 23
La mare del Quimet es mort. Quan arriben hi han tres veines, ja han comprat la corona de fulles, sense flors tal com hauria volgut la mare del Quimet. I ja l'han vestida. El Quimet les hi dona les gracies i la veina amb la que havien dinat que s'havia discutit amb el seu marit arriba i diu que ja seria hora de trucar a la funeraria.

## CAP 24
La Natalia parla amb el Cintet de que ella no vol que el Quimet vagi a fer d'escamot. Tot i així tots els diumenges se'n va amb el Cintet, ella creu que a fer d'escamot. Un dia ve el Mateu a casa i parlen sobre que el Mateu té un amic que té coloms de corbata però que no li ha dit res al Quimet perquè l'últim que li falta son encara més coloms. A mesura que van parlant el Mateu li comença a a explicar a la Natalia que fa una setmana que no veu a la seva filla perquè com la Griselda treballa l'ha hagut de deixar amb els seus pares, i es posa a plorar. Això enterneix molt a la Natalia i li continua donant voltes després.

## CAP 25
Aquest capítol suposa el cansament definitiu de la Natalia. Trenca un got a la feina i li fan pagar, arriba a casa i li pega a l'Antoni i es posa a plorar. La Rita al veure que l'Antoni plora també es posa a plorar i la Natalia també acaba plorant. Arriba el Quimet i diu que això es el que li faltava. La Natalia es cansa i enjega a passeig tot. Puja al colomar i durant mesos i mesos turmenta als ponedors per tal de que no surtin cries, i acaben no surtint, i el Quimet rondina.

## CAP 26
El gas ja no arriba a casa on treballa la Natalia llavors la manen a comprar carbó i el senyor que reparteix la llet, diu que tampoc sap si a l'endemà els hi podrar repartir la llet. Confonen al marit de la senyora de la casa amb un capella i casi el maten i dies després venen 5 milicians amb escopetes i una parella a resoldre un problema amb una hipoteca.

## CAP 27
A la casa on treballava la Natalia ja no li poden pagar i el senyor de la casa li parla sobre perquè els pobres no poden viure sense els rics. El Quimet i el Cintet venen per portar-li menjar a la Natalia i despŕes d'anarse'n ve el Mateu. Ell li diu que s'en va al front d'Aragó i que es probable que no es tornin a veure. El Mateu abans d'anarse'n li diu a la Natalia que el Quimet té molta sort de tenir-la a ella, que li diu perquè probablement no es tornin a vuere. La senyora Enriqueta li consegueix treball a la Natalia de netejadora a l'ajuntament.

## CAP 28
La Natalia es posa a recordar tota la seva vida fins que arriba el Quimet. Li diu que està bé, que se'n sortiràn però uqe s'en ha d'anar i s'ha d'emportar dos matalassos que els hi fan falta. Llavors s'en va a l'adroguer per que li doni corda i sacs i es posen a parlar de quan l'adroguer va lluitar en la primera guerra mundial.

## CAP 29
Va vindre el Quimet i li porta taronges, llet i café. Li explica el seu viatge a Cartagena pels billets de banc i com quasi no arriben perquè anaven amb una avioneta vella. La Natalia li pregunta que quan tornaran, i ell li respon que potser tornarà quan torni en Quimet.

## CAP 30
A la Natalia la criden desde un cotxe i resulta que es la Julieta vestida de miliciana. Li explica que s'ha enamorat i que va estar tota una nit amb un noi en una casa que no era seva i amb el vestit de la senyora de la casa. La Natalia li diu que la vida alegre a ella se li ha acabat, que tot el que li queda son mal de caps. La Natalia li explica a la Enriqueta la historia de la Julieta i a ella no li agrada gens, diu que totes les noies revolucionaries son iguals.

## CAP 31
El Quimet torna i s'hi està 4 dies a casa. Sembla que ha agafat tuberculosis. Potser li cambien de front, i això significaria que encara veuria a la Natalia menys, i que li podria dur encara menys provisions. Passen tanta gana que han de portar a l'Antoni a una colonia per tal de que pugui menjar alguna cosa. A ell no li fa gens de gràcia.

## CAP 32
La Natalia i la Julieta acompanyen a l'Antoni a la colonia i quan el van a deixar ell es posa a plorar per intentar no anar-se'n. La Natalia no cedeix i se'n van. La senyora Enriqueta va cada diumenge a veure a l'Antoni i la Rita ll'anyora molt. Fins que arriba un milicià un dia i li diu a la Natalia que tant el Cintet com en Quimet han mort, i li porta l'última cosa que queda del Quimet, el seu rellotge.

## CAP 33
La Natalia rep la visita de la Griselda per donar-li el pesa'm i li va dir que el Mateu estava bé. L'Antoni va tornar de la colonia i semblava un altre nen, no li dirigia la paraula a la seva mare. La Natalia havia hagut de vendre-ho tot per sobreviure, fins i tot el rellotge d'en Quimet. La senyora Enriqueta li diu que havien afusellat al Mateu en mitg d'una plaça i a la Natalia li senta molt malament. Ha de tornar a la casa dels seus antics amos per buscar treball.

## CAP 34
La Natalia torna a la casa dels seus antics amos i li diuen que no tenen treball per ningú i menys per una roja com a ella. La Natalia té un somni sobre que unes mans agafen als seus fills transformats en ous i els sacsejen tal com va fer ella amb els ous dels coloms. Creu que la millor solució es matar als seus fills i a ella. Agafa l'embut i el renta i pensa que la millor manera serà amb salfumant.

## CAP 35
La Natalia surt amb l'intenció d'anar a l'adroguer a pel salfumant però no se'n porta l'ampolla, al final acaba passejant per sortir. Segueix a una senyora i arriben a l'esglesia. Ella no es pot agenollar i en mitj de la misa es dona conta de que comencen a sortir bombolles de sang de l'altar i son de la sang dels soldats morts a la guerra. Tots miren cap abaix i no veuen les bombolles pero la Natalia i la senyora es miren i la Natalia fuig de l'esglesia. Arriba a casa i el Mateu li diu que no te més remei que matar al seus fills. Està delirant.

## CAP 36
La Natalia va a buscar el salfumant a l'adrogueria i es dona conta que no te importancia com siguin les coses de boniques o de lletges perquè a l'endemà no les haurà de tornar a veure. L'adroguer li dona el salfumant i li diu que no passa res perquè no porti diners, que ja li pagarà. Quan la Natalia s'està anant cap a casa l'adroguer la crida, i li diu que si vol treballar per a ella, la Natalia li diu que si i instintivament li deixa el salfumant a sobre del taulell. Torna a casa i es posa a plorar.

## CAP 37
Descriu la casa del seu nou amo.

## CAP 38
La vida de la Natalia ha millorat molt. Els nens tornen a ser més que ossos perquè la Natalia a més dels diners que guanya sempre porta llegums perquè mengin. Després de tretze o quinze mesos de treballar amb l'adroguer aquest li diu que vingui el diumenge que ha de parlar amb ella. 

## CAP 39
La Natalia va a casa de l'adroguer per parlar i l'adroguer li diu que ell pensa molt en la vellessa i el que vol es ser un vell respectat, que vol tenir una familia i li proposa a la Natalia de casar-se amb ell. Ell està molt neguitós per fer-li aquesta pregunta i la Natalia no li dona resposta.

## CAP 40
La Natalia li explica la situació a la senyora Enriqueta i li acaba dient a l'adroguer que sí. La Natalia li diu les coses que vol a l'adroguer i aquest lis concedeix totes. Els nens no diuen ni una paraula i sis mesos després, es casen.

## CAP 41
L'Antoni li diu que no 