# Ones

## MHS
És un moviment periòdic que es repeteix al llarg del temps i té com a característica que sempre passa per una posició d'equilibri. Com per exemple un pèndol i una molla de la que penja un pes. 

Un moviment harmònic simple es pot considerar com la projecció d'un moviment circuilar uniforme sobre un eix que passi pel centre de la circumferencia. La funció per representar un MHS matemàticamente serà o un sinus o un cosinus. 

$$x = A · sin(\omega · t + \phi_0)$$
$X$ = elongació (m)
$A$ = amplitud (màxima elongació)
$sin$ = fase (rads)
$\omega$ = pulsació/freqüencia angular (rad/s)
$\phi_0$ = fase inicial
$T$ = període (s) --> Temps a fer una oscil·lació completa
$f$ = freqüencia (Hz)

Per a coneixer la velocitat derivarem $x(t)$ respecte del temps.

$$v = \frac{dx}{dt} = A · \cos(\omega · t + \phi)$$


Per a trobar la acceleració derivarem $v(t)$ respecte del temps.

$$a = -A · \omega² · \sin(\omega·t + \phi_0)$$


Conclusions: 
- El signe de l'elongació i la de l'acceleració seràn contraris.
- Quan està al punt d'equilibri l'elongació val 0.

La força que fa que el moviment harmònic torni al punt d'equilibri es una força recuperadora. 

$F = -k · x$


