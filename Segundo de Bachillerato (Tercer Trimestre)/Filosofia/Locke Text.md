# Locke Text
## CAP 1: seccions 1-3
\[1] Locke desmonta els arguments de Filmer afirmant que no tenim cap garantia de que Adam vagi tenir cap poder sobre els seus fills, ni de que aquest poder en cas d'existir fos successori, ni de que en cas d'existir i ser hereditari, no sabem qui es l'hereu legitim i que en cas de que tot això estiguès solventat, es va perdre la linia hereditaria d'Adam fa molt de temps. 

Locke diu que habent refutat la teoria de Filmer les persones que hostenten el poder estàn exercint violencia contra les persones a les que roben el poderi  uqe les persones s'organitzen com besties, el que es més for té el poder. 

Per tant, ha de trobar una altra teoria que expliqui el sorgiment del govern i del poder polític i una altra menra de designar i conèixer les persones que el tenen.

\[2] Locke diu que cal posar diferencia entre els diferents tipus de poder per tal de que no es confongui per exemple el poder que té un pare sobre els seus fills i el poder que té un monarca sobre els seus subdits.

\[3] Locke defineix el poder polític com el dret de dictar lleis sota pena de mort (i conseqüentment sota totes les penes inferiors) amb la finalitat de regular i preservar la propietat i ampliar la força de la comunitat. Això per tal de protegir l'Estat davant de conflictes amb els de fora amb l'única intenció d'aconseguir el bé públic.

## CAP 2: seccions 4-15
\[4] Parla de l'estat de natura, un estat de total llibertat on cadascú ordeni les seves accions i dposi dde possesions i persones com ell cregui oportú, sense demanar permí ni depende de la voluntat de cap altre. Tots els éssers humans, al ser de la mateixa especie i el mateix rang estan en completa igualtat, a no ser que el senyor de tots ells posi a un per sobre de l'altre.

\[5] Hooker considera que aquesta igualtat natural que hi ha entre persones es la base de l'amor al proïsme. 

\[6] L'estat de natura tot i ser un estat de llibertat, no és un estat de llicència. Tot i que els homes tenen la llibertat plena, no tenen dret a atemptar contra la propia vida ni contra la vida dels demès ni els seus bens. La única justificació per prendre la vida, entorpir-la o posar-li obstacles als mitjans que cal per preservar-la es quan es fa justicia quan algú cometi una ofensa. Aquí s'expliquen els drets naturals de les persones: la llibertat, l'existència i la salut i la propietat privada.

\[7] Per tal de dissuadir a aquells que vulgiun transgredir la llei natural, hi ha el dret natural de jurisdicció. Com tots els éssers humans en l'estat de natura son iguals, si un fa qualsevol cosa per que un compleixi la llei, tots els altres també tindran el mateix dret de fer-ho.

\[8] El càstig que se li pot aplicar a una persona que trenca la llei natural no pot ser arbitrari, bé dictat per la raó i la consciencia. Se li assigna un càstig proporcional a la ofensa que ha comès amb dues finalitats, que no torni a cometre una ofensa similar i per reparar el mal causat. Aquestes son les dues úniques raons per les que un home pot fer mal legalment a una ltre. 

\[9] Les lleis aplicades en un país, no incumbeixen a un ciutada estranger ja que per ell els magistrats i jutges son persones sense autoritat. Per tant, per al ciutadà estranger, aquests magistrats no tenen més poder que aquell que els hi confereix el dret natural de jurisdicció.

\[10] Sempre que es trasgredeix una llei hi ha un afectat i aquesta afectat té el dret de reparació. Aquest dret de reparació és únic a la persona que ha patit la trasgressió tot i que qualsevol altre que trobi la causa justa també s'hi pot afegir.

\[11] **No entiendo esta seccin la verdad**

\[12] Les infraccions menors no han de ser castigades necessariament amb la mort. Han de ser castigades amb el grau de severitat suficient per tal que el transgressor surti perdent, s'empenedeixi i per tal de dissuadir a altres potencials agressors.

\[13] Es cert que les persones poden no ser justes a l'hora de castigar a una persona que ha trasgredit la llei natural, pero tampoc podem confiar al propi transgressor que es castigui a si mateix. Tot i això confiar el càstig a un monarca absolut també es problemàtic perquè recordem que son persones normals i éssers humans normals que tampoc poden ser justos a l'hora de castigar.

\[14] L'estat de naturalesa si que existeix en el mon real. La prova d'això són els monarquest absoluts i els prínceps. 

\[15] De fet, els individus romanen en un estat de naturalesa fins que per consentiment propi passen a ser membres d'alguna societat política. 

## CAP 5: seccions 26-27, 31-34, 42-51
\[26] El dret a la propietat privada. Un individu pot ser propietari fins que trgui algun benefici per sustentat la seva vida.

\[27] El treball i la feïna produida amb les seves mans que una individu fa sobre el que li ha donat la natura el converteix en propietat seva. Això succeix perque al modificar-ho hi afegeix algun valor. Això perquè el treball es indubtablement propietat del treballador i això fa que cap home tret d'ell tingui drett al que ha estat afegit a la cosa en qüestió, tot això mentre quedin prou béns comuns per als altres.

\[31] La propietat privada té limits: tot allò que un individu pugui aprofitar per millorar la seva vida abans de que es malbarati. Déu no va crear res perquè es malbarati o per destruir-ho. 

\[32] La propietat de la propietat privad de la terra es igual a la propietat privada de les coses, el treball. Tot porció de terra qeu sigui llaurada, millorada i cultivada per un home, si és resultat del seu treball es com si posés tanques al voltant. Déu va ordenar als homes que cultivessin la terra i per tant, aquell que obeint a Déu ho fa, es pot apropiar de la terra sense cometre injuria. 

\[33] El límit de la propietat de la terra és el mateix que amb la propietat. El fet de que una persona s'apropii de un troç de terra no fa que deixi a la resta de persones sense terra, ja que en queden tantes com hi facin falta. 

\[34] no entiendo muy bien este

\[42] La major part de productes de consum que tenim avui en dia reben el seu valor graices al treball humà que hi afegim. El vi es millor que l'aigua gracies a tot el treball que té darrere, al igual que un camp de psatura es més profitós que un camp en estat silvestre gracies al treball que te darrere.

\[43] Al igual que amb els productes de consum, el valor de la terra ve en gran mesura donat pel treball que hi invertim. Un acre de terra que es llaura i es cultiva es molt més valuós que un acre de terra que deixem sense cultivar i on nomès creixen plantes naturalment sense intervenció humana. Per tant quan pensem en el valor d'un pà, també em de pensar en el treball que va suposar llaurar la terra d'on bé el blat, per exemple.

\[44] Tot i que les coses de la natura són donades en comú l'home amb el seu treball fa seves les coses que li acaben donant suport o comoditat.

\[45] El treball al principi va donar el dret a la propietat ja que hi havien més terres csomunals de les que l'home podia utilitzar. Quan la població va augmentar i amb l'utilització dels diners van començar a escassejar la terra. Les diferents comunitats fixen fronteres i renuncien al dret sobre la terra que ja estava en propietat d'altres Estats o regnes. Tot i així, en les societats que no es van unir a l'us dels diners com el reste de la humanitat encara es donen casos de que les terres comunals superen als homes que les volen utilitzar.

\[46] El fet de recollir cent quilos de gla et dona la propietat sobre aquests glas, però has de consumir-los abans de que es malbaratin perquè si no ho fas hauràs agafat més del que et pertoca i per tant estaràs robant a la resta de la humanitat. En canvi, si abans de que es fessin malbé canviava les glans per nous, no estava fent mal a ningú, ja que les nous estaven en bon estat i no es malbaratarien. I si en lloc de nous les canviava per diamants, tampoc feia mal a ningú perquè no es farien malbé. Es a dir, que la justa propietat no consisteix en la quantitat de coses posseiïdes sino a no deixar que les que té en el seu poder es fassin malbé sense utilitzar-les.

\[47] Aquí es on sorgeixen els diners, els homes els podien coservar sense que es podrissin i per consentiment mutu intercanviables per productes veritablement útils per a la vida però de naturales corruptible. 

\[48 y 49] En una illa sense diners i sense possibilitat de comerç, perquè algú acumularia tanta terra com pogués, si no podria intercambiar-la amb ningú? No valdria la pena que hi posés tanques al seu terreny i aviat entregaria les terres al comú de la terra natural. Això va pasar al principi a América però tan aviat com es van descobrir els diners es veu com els homes començen a augmentar les seves possesions.

\[50] Gracies als diners els homes descobreixen la manera de posseir més terra de la que es capaç de fer servir, rebent or i plata a canvi, ja que aquests no es fan malbé. 

\[51] El fet de que fos inutil treballar més terra de la que un poguès utilitzar servia per tal que no hi haguessin disputes entre persones per el dret i la propietat.

## CAP 3: seccions 16-21

\[16] L'estat de guerra és un estat d'enemistat amb una acció premeditada i establerta contra la vida d'un altre home. Aquesta acció posa als dos homes en un estat de guerra i el que fa l'acció posa en risc la seva vida, contra l'altre i qualsevol que es vulgui afegir. Perquè es just i raonable que tingui dret a destruir qui amenaça de destruir-me a mí. 

\[17-18] El fet de que si algú em vol tenir sota el seu poder absolut s'estigui posant en una situació de guerra amb ell. Aquesta declaració s'ha de pendre com una declaració d'atemptat contra la vida i la llibertat perquè per què voldria una persona tenir sota control un altre si no es per fer-li fer coses en contra de la seva voluntat? Aquell qui intenta prendre la llibertat d'un altre ha de ser considerat algú que vol prendre-li també tota la resta. Per tant, es legal que un home mati un lladre que no li ha fet cap mal ni ha declarat que vol matar-lo simplement perquè una vegada el lladre el tingui sota el seu control, quina garantia té de que no atemptarà contral a seva vida?

\[19] L'estat de natura és un estat de pau, bona voluntat, asistencia mútua i conservació mentre que el segon és un estat de enemistat, malícia, violència i mútua destrucció. 

\[20] Quan es viu en societat, l'estat de guerra cesa quan es deixa d'exercir la força mentre que si no es viu en societat l'estat de guerra no cessa fins que l'agressor ofereix la pau i deistgi la reconciliació en termes que puguin reparar el mal que ja ha fet i que doni seguretat futuresa a l'inocent. Tot i viure en societat, si les persones que administren la justicia actuan amb tergiversació i perversio de forma injusta (corrupció) també es roman en l'estat de guerra.

\[21] Per evitar aquest estat de guerra, els homes es posen ells mateixos en un estat de societat i abandonen l'estat de naturalesa.

## CAP 8: seccions 95-122
\[95] Els homes son lliures per naturalesa però poden renunciar als seus drets naturals per sotmetre's als lligams de la societat civil. Això ho pot fer qualsevol grup d'homes perquè no perjudica la llibertat dels altres, que es queden en l'estat de natura. A partir de llavors queden incorporats en un cos polític on la majoria té el dret d'actuar en nom de tots.

\[96] En aquest cos polític, per tal de que es mogui cap a una direcció definida, les lleis i decisions es sotmeten a la força més forta, que en aquest cas es la majoria. Com ja es va sotmetre a consentiment unanim el crear el cos polític, amb la decisió de la majoria ja basta. Això passa perquè si per cada decisió s'haguès de fer unanimament seria impossible de canviar.

\[97] Quan un home consenteix formar un cos polític sota un sol govern es posa a ell mateix sota l'obligació de sotmetrès a les decisions de la majoria. Perquè, si nomès es sotmetès a les lleis que li convinguessin, quina diferencia tindria amb el estat de naturalesa?

\[98] Els consens de la majoria és el millor criteri perquè si s'haguès de decidir tot unanimament el fet de que hi hauria gent malalta o la diversitat d'opinions. Si s'haguès de fer tot unanimament no es podrien prendre decicions i el cos polític s'hauria de disoldre. Els humans son criatures racionals i per què haurien volut formar societat nomès per disoldre-les?

\[99] Qualsevol que surti de l'estat de naturalesa per integrar-se en una comunitat ha d'entendre que ho fa entregant a la majoria tot el poder necessari perquè la societat assoleixi aquelles finalitats. Això es tot el que és necessari per tal que s'estableixi el pacte entre aquells que s'integren per formar un Estat. Aquesta es la forma en la que s'han format tots els governs legals del món.

\[100] Hi han dues objeccions, la primera que no hi ha cap exmple al llarg de la historia una formació similar d'homes i la segona que es impossible que els homes tinguin dret a fer-ho ja que tots neixen sota un govern al qual s'han de sotmetre.

\[101-104] sin energía mental como para leerlo

\[105] El poder quan no hi ha govern sol recaure en el pare perquè sol ser el més capaç. Un cop el pare mor o es separen families tornen a triar un cap, en completa llibertat. En els pobles d'America es preferia a l'hereu directe tot i que si era feble i incompetent el canviaven

\[106] nidea

\[107] En les societats primitives i petites el model de que una persona sola podia ostentar tot el poder tenia sentit tant perquè era el més evident com perquè no en coneixien cap altra. Com no havien viscut dins de la democracia i com tampoc eren avariciosos ni s'havien vist sota opressió, no els calia més que la monarquia. Per tant, escollien com a cap a la persona més savia i preparada. 

\[108] Per això els reis d'Amèrica (que son una rèplica dels que ho van ser en temps primituis a Àsia o Europa) en temps de guerra son els únic que ostenten el poder però que en temps de pau es prenen les decisions per consell.

\[109] 