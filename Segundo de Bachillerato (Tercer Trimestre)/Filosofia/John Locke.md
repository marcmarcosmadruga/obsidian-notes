# John Locke
Va estudiar nomès estudis a la filosofia escolàstica i ignorava la filosofia cartesiana i els avenços de la nova ciència. Va ser molt influit per Descartes (el veu com a bona alternativa a l'escolàstica). Reb l'influència de Gassendi, Hobbes i Shaftesbury.

Té dos tractats sobre el govern civil. En el primer refuta la teoria de l'origen diví i critica l'absolutisme i en el segon desenvolupa les seves idees polítiques a partir d'un anàlisi de la natura, origen i objectius dels govern civil. Té l'objetciu de justificar la no legitimació de l'absolutisme.

És el pare de l'empirisme modern (teoria del coneixement). També és el pare del liberalisme clàssic (àmbit polític i social) oposició a l'absolutisme i defensa de la llibertat dde la persona i la inciiativa individual sobre la col·lectiva. Les estructures de govern no intervinguin per a garantir la convivència. 

Elaborant les teories contractualistes es vol cambiar la forma de donar el poder teologicament a racionalment. Aquest contracte es resultat d'un acord racional, lliure i voluntari dels membres d'una societat.

Es parteix d'una situació hihpotètica en la que els individus viuen junts però encara no han fundat una societat. Ens doten de drets naturals (jusnaturalisme) (dret a la vida, dret a la propietat). Després analitzem els inconventients que té per a les persones el viure en aquest estat de natura i la manera com mitjançant la raó podríem evitar-los (el pacte). Així s'hauria d'establir els termes (finalitat, condicions i obligacions) del pacte que donarà origen a l'estat civil que servirà de legitimació del poder polític i serà la base de l'obligació política. Finalment es descriuran les característiques del poder constituït.

Estat de natura - (inconvenients) -> Pacte de contracte social --> Establi finalitat condicions i obligacions --> Descrivim les característiques del poder constituït.

### Primera teoria contractualista

Toms Hobbes (1651) amb la finalitat de legitimar racinoalment l'absolutisme. 
> L'esser humà en estat de natura és individualista i egoista, només busca el seu plaer i vol ampliar el seu poder, és insacbiable i immoral. **L'home és un llop per a l'home**.

> Encara que son diferents podem dir que son iguals. I aquesta igualtat és la base del seu enfrontament per aconseguir el poder. **La guerra de tots contra tots.**

Per evitar això es pacta i es forma una societat en la que cada individu renuncia als seus drets naturals, el poder i la seva llibertat i les diposita en mans de un sobirà. Aquest sobirà concentrarà el poder absolut, perquè el pacte és de submissió i es fonamenta en la por a la força d'aquest poder superior.

El pacte es realitza entre els individus i en favor del sobrià. Per tant aquest no pacta i es manté en l'estat de natura. Poder il·limitat i arbitrari. 

### Important

Per a Hobbes aquest pacte o contracte és una cessió de tots els drets naturals menys el de la vida i és **irrevocable**, es a dir, no es pot dissoldre. Qualsevol forma de govern és millor que l'estat de natura.

Locke fa una crítica als principis de l'absolutisme. Es preocupa per protegir als individus dels possibles excessos del poder establert. 


## Segon tractat del govern civil
Refutació de la teoria del dret diví a governar dels reis. En el primer capítol resumeix les idees del primer trctat. En aquest Locke refuta la teoria de Robert Filmer.

Filmer defensava que el poder era un dret diví i natural: l'oriden de l'autoritatp olítica es troba al poder que Déu li va donar a Adam per governar sobre Eva, els seus fills, els fills dels seus fills... Per això el poder ho reben per successió hereditària del propi Adam. El poder es indivisible i absolut (no sotmès a cap limitació humana)

Locke en canvi diu que és impossible establir com a font de tot el poder dels actuals reis el dret diví i natural que ve de Déu. (Basat en principis empiristes) No hi ha cap prova per poder demostrar que l'autoritat reial ha estat convedida per Déu a Adam. Si acceptem que ha estat concedida sobre els seus fills tampoc tendriem cap evidència de que es transmès per successió. I si hagués sigut transmesa, tampoc tindríem cap manera per establir a qui li correspon legítimament. 

El segon argument es fonamenta en la concepció de Locke dels drets naturals. Si acceptem que Déu va donar aquest poder a Adam hauríem d'admetre que no naixem lliures. Si existís aquesta subordinació natural al poder, al tindre caràcter successori, no podríem arribar a ser lliures i estaríem sotmesos a un poder absolut. Loke creu necessària una nova teoria política diferent a la de Filmer per explicar l'origne del govern i el poder polític. 

abans d'elaborar aquesta nova teoria va definir què és el poder polític

### L'estat de naturalesa
Locke inicia la seva teoria política imaginante com eren els humans abans d'unir-se per formar una societat (això anomenat estat de naturalesa). Segons ell els éssers humans son lliures, independents (no han de demanar permís ni estan obligats a seguirl a voluntat de cap altra persona) i iguals.

Els avantatges naturals con un drets que es doten a tots els éssers humans, naixem amb ells i son inalienables. Aquests són: la llibertat, l'existència i a la salut i la propietat privada (ens permet disposar dels béns necessaris per subsistir). La propietat privada és el resultat d'afegir l'esforç personal al productte natural. 

Els drets naturals són inalienables però no il·limitats:
- llibertat: existeix una llei de la naturalesa, dictada per la raó que tothom ha de complir. Com tots som iguals ningú pot causar un mal als altres en els seus drets naturals (no pot danyar la seva vida, salut, llibertat o possesions).
- propietat privada: Ningú pot acaparar els productes naturals de manera que no quedi res per a la esta o es puguin perdre abans de ser utilitzats. Per evitar aquesta pérdua es van crear els diners La creació dels diners va fer possible la acumulació de riquesea en aquelles persones més treballadores. 

Què passa quan algú viola els drets naturals de qualsevol individu, no respectant la llei de la naturalesa?

Com tots som iguals els éssers humans tenen el dret de jutjar i castigarr quan altra persona viola un dels seus drets naturals (dret natural de jurisdicció). Aquest dret també té límits: no és un poder absoluta ni es pot aplicar de manera arbitraria (crítica de l'absolutisme). 

Límit: La persona perjudicada exerceix el seu dret natural de jurisdicció i jutja al transgressor, utilitzant la raó, i li assigna un càstig proporcional al mal que ha fet per evitar que torni a cometre el delicte i repari el mal que ha causat.


Per tant, la violació d'un dret natural genera dos drets complementaris: 
- Dret de castigar: finalitat és dissuadir al transgressor (Dret per tothom)
- Dret de reparació: la seva funció es compensar l'individu perjudicat (dret per al perjudicat, pot renunciar a ell)

## **Salto de Contenido**

Locke arriba a la onclusió que l'origen de tots els governs, en temps de pau, ha sigut el consens del poble.

*Segon Tractat Sobre el Gover Civil. Capítol VIII. De L'Origen de les Societats Polítiques. Seccions 95-122*

### Les Característiques del poder constituït

A través d'un pacte lliure i voluntari s'estableix una societat política o estat. Té les següents característiques:
- el poder no pot ser absolut ni arbitrari. La cessió del poder de jutjar i castigar i el de llibertat natural es fa de manera provisional, delegada i condicional. Crítica a l'absolutisme.
- Delegada: El poble de comú acorda ha establert aquest poder i l'autoritza a legislar en representació seva.
- Condició i provisonalitat: Només el poble té el poder de revocar aquest poder quan no respecti les lleis o els fins pels quals ha estat establert. 

Aixó significa que per fundar un estat es produeix un doble pacte: entre els individus que el formen i entre el poble i els governants.

El poder s'ha de repartir per evitar els possibles abusos. S'estableix la divisió de poders:
- Poder legislatiu: el que elabora les lleis. Aquestes lleis han de ser conegudes i no podem canviar-les per a casos concrets. 
- Poder executiu:
- Poder federatiu:


