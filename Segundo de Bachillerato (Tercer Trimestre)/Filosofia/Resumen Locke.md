# Resumen Locke
Locke fa una crítica als principis de l'absolutisme. Es preocupa per protegir als individus dels possibles excessos del poder establert. 

En el primer capítol del segon tractat resumeix les idees del primer tractat. En aquest Locke refuta la teoria de Robert Filmer. 

Filmer defensava el següent (per tal de justificar l'absolutisme):
- El poder es un dret diví i natural: l'origen de l'autoritat política es troba al poder que Déu li va donar a ADam per governar sobre Eva, els seus fills, els fills dels seus fills, etc. Per aquest motiu el poder el tenen els monarques per successió hereditària del propi Adam que el va rebre de Déu.
- El poder és indivisible (tot pertany al monarca)
- El poder és absolut: no està sotmès a cap limitació humana i la societat ha d'obeir al monarca.

Locke pensa el següent:
- És impossible establir com a font de tot poder i legitimació de l'autoritat dels actuals reis el dret diví i natural que ve de Déu. Per tal de defensar això dona dos arguments:
	- \[Basat en els principis empiristes] No hi ha cap prova per demostrar que l'autoritat reial ha estat concedida per Déu a Adam.
	Si acceptem que ha estat concedida sobre els seus fills, tampoc tenim cap evidència de que aquesta autoritat fors transmesa per successió als seus hereus. Si haguès estat transmesa, tampoc tindríem cap manera per establir a qui li correspon legítimament.
	
	- \[Fonamentat en la seva concepció dels drets naturals de l'ésser humà] Si acceptem que Déu hagués donat una poder d'aquest tipus a Adam sobre els seus fills, hauríam d'admetre que no naixem lliures. Es més si existis aquesta subordinació natural al poder patriarcal, com té caràcter successori, no podríem arribar a ser lliures mai i estaríem sotmessos al poder absolut.

A través d'això Locke creu necessària una nova teoria política diferent a la de Filmer per explicar **l'origen del govern i el poder polític**. Abans d'elaborar aquesta teoria defineix què és el poder polític per diferenciar-lo d'altres tipus de poder. 

[[Locke Text#CAP 1 seccions 1-3]]

## L'estat de naturales
Locke inicia la seva teoria imaginant-se com eren els éssers humans abans d'unir-se per formar una societat (estat de naturalesa). 

Ell diu que són: lliures (per actuar i disposar de les seves possessions segons creguin oportú), independents (no han de demanar permís ni estan obligats a seguir la voluntat de cap altra persona) i iguals (entre si, sense suborinació, tenen el mateix rang, amb els mateixos avantatges naturals).

Aquests avantatges naturals son, segons Locke, uns drets naturals amb els que naixem i són inalienables:
- La llibertat (que és el fonament sobre de totes les altres coses)
- L'existència i a la salut
- Propietat privada (ens permet disposa rdels béns necessaris per subsistir). La propietat privada es el resultant d'afegir l'esforç personal al producte natural. D'aquesta manera si jo treballo una percel·la que abans ningú treballava, soc jo que crea el valor i el producte resultant em pertany (*herència*).

Quan algú viola els drets naturals de qualsevol altre individu, al estar en un estat de natura en una perfecta igualtat, tots els éssers humans tenen el dret de jutjar i castigar quan un altre persona viola un dels seus drets naturals (dret natural de jurisdicció o jurisdicció recíproca).

Aquest dret a castigar també té limits ja que no es ni absolut ni es pot aplicar de manera arbitraria (crítica a l'absolutisme). Aquest límit es que la persona perjudicada exerceix el seu dret natural de jurisdicció i jutja al transgressor utilitzant la raó i li assigna un càstig proporcional al mal que ha fet per tal de que no torni a cometre el delicte i per reparar el mal causat. 

Per tant, la violació d'un dret natural genera dos drets complementaris, el dret de castigar (dret que té tothom per tal de dissuadir a possibles transgressors) i el dret de reparació (compensar a l'individu perjudicat, nomès el té el perjudicat i pot renunciar a ell). 

[[Locke Text#CAP 2 seccions 4-15]]
[[Locke Text#CAP 5 seccions 26-27 31-34 42-51]]

## Les causes el pacte
La majoria de les persones actuen de forma justa seguint la llei natural però quan la violen, hi ha persones que no apliquen sense correció ni mesura, de forma injusta. La conducta d'aquestes persones pot desencadenar la violència, provocant una guerra de tots contra tots. 

Això succeeix perquè a l'estat de naturalesa no hi ha mecanismes que facin respectar els drets naturals dels individus ni sancionen de manera justa qui els violen. Cadascú fa justifica pel seu compte i al diexar-se portar per la venjança, l'estat ideal inicial es converteix en un estat d'enemistat, malícia, violència i mútua destrucció (l'estat de guerra).

[[Locke Text#CAP 3 seccions 16-21]]


## La finalitat del pacte
Per resoldre els inconvenients de viure en l'estat de naturalessa els humans accepten formar part d'una societat civil. Això per presrvar els seus drets naturals mitjançan l'aplicació justa de la llei per evitar  l'estat de guerra. Per aconseguir-ho establiran un poder terrenal que jutgi en nom de la comunitat. Això resulta en que qualsevol persona perjudicada podrà demanar justicia i reparació. L'aplicació de la llei serà imparcial i justa i resoldrà el problema i evitarà enfrontaments per la força. 

Els requisits per garantir una aplicació justa de la llei són:
- L'existència d'una llei establerta per consentiment comú que sigui fixa, coneguda i que tracti a tothom del a mateixa manera.
- Que hi hagi un jutge públic amb autoritat que apliqui aquesta llei imparcialment.
- Que hi hagi un poder que doni suport i força per executar la sentencia.

## Els termes del pacte
Els individus abandonen l'estat de natura i funden un estat o societat política a través d'un pacte.

Aquest pacte es basa en el consentiment lliure pel qual renuncien a la seva llibertat natural (substituïda per una llibertat civil) i al seu dret natural de jurisdicció (fer justicia pel seu compte). S'obliguen a sotmetre's a les decisions de la majoria i a ser guiats per aquesta majoria. S'utilitza el criteri de la majoria. Cedeixen el dret natural de jurisdicció a la comunitat per a que l'estat sigui que posseeixi el monopoli de la violència física legítima i faci d'àrbitre amb unes normes fixes, imparcials i idèntiques per a tots.

Aquesta ccessió es de manera provisonar i condicional.

Locke se enfronta a dues objeccions: cap estat o govern s'ha format mai d'aquesat manera i es posa en qüestió que els individus tinguin dret a establir per consens una societat política.

Locke desmunta aquestes objeccions fent una crítica a la teoria del poder patriarcal i aportant exemples (històrics i bíblics a favor de la seva resis). Conclu amb que l'origen de tots els governs, en temps de pau, ha sigut el consens del poble. 

[[Locke Text#CAP 8 seccions 95-122]]

## Les característiques del poder constituït
El poder no pot ser mai ni absolut ni arbitrari. La cessió del poder de jutjar i castigar i el de llibertat natural es fa de manera provisional, delegada i condicional. És delegada perquè el poble amb comú acord estableix aquest poder i l'autorita a legislar en representació seva. El poble té el poder de revocar aquest poder quan no respecti les lleis o els fins pels que ha estat establert.

El poder s'ha de repartir per evitar abusos:
- Poder legislatiu: que elabora les lleis, conegudes per tota la població i no podem canviar-les per a casos concrets.
- Poder executiu: vetalla perquè es compleixin les lleis.
- Poder federatiu: estableix les aliances amb altres estats, i en cas de conflite declara la guerra o firma la pau.

El poder ha d'assegurar i regular la propietat privada.

El pacte es pot dissoldre per causes justificades, internes  i externes. Una causa externa es la intromissió d'una força extrangera que s'imposa sobre els membres de la comunitat i les causes internes són quan el poder legislatiu s'altera i quan s'actua contràriament al fi pel qual va ser constituït. 