# La Fundación

Antonio Buero Vallejo usó la técnica del positivismo para no ser afectado por la represión de la dictadura franquista. 

Seis prisonieros comparten una habitación en un lugar cerrado pero comfortable. Tomás es el protagonista y el escenario va cambiando a. 

Usa la técnica de la inmersión (és decir, el público ve lo mismo que ve el protagonista).  

Es una obra de teatro circular porque empieza y acaba de la misma manera.

Estructura:
1. Primera parte
	1. Primer cuadro -> cinco silloncitos de cuero, un televisor, un teléfono blanco, libros, una revista ilustrada y periódicos, lámpara con pantalla de fantasia, menaje de cocina fino y elegante, un frigorífico i camas con sus limpias sábans floreadas i rica colcha.
	2. a
2. Segunda parte
	1.  Primer cuadro -> 
	2.  Segundo cuadro -> (vuelve la Fundación, por eso es una obra circular)


Estructura interna:
1. Primera parte: (primer cuadro, segundo cuadro i primer cuadro de la segunda) En esta primera parte Tomás recupera la cordura. 
2. Segunda parte: (segundo cuadro segunda parte) Tomás es consciente de la realidad i del lugar en el que está (cárcel)

La música también es igual al principio al final, haciendola todavía más circular. 

En la primera parte por la ventana ve un paisaje maravilloso mientras que en la segunda ya se da cuenta del ambiente real. 


Son un grupo de 6 hombres (Asel, Lino, Max, el hombre inmóvil i Tulio) tiene una relación correcta con todo el mundo pero con Tulio existe cierta tirantez. Conviven las 24 horas del dia, Tomás es el elemento discordante porque ve cosas que los demás no ven (como por ejemplo a su novia Berta). **La primera parte termina** cuando el encargado se da cuenta de que el hombre tumbado está muerto (todos lo sabían menos Tomás). Todos esperan una represalia de la Fundación (aquí se empiezan a transformar elementos del escenario). Asel y Tulio tenían un plan de fuga y les interesaba que hubiese una represalia para ir a las celdas de castigo y excavar un túnel. Al no tener represalia piensan que alguien les ha delatado, todos desconfían de Tomás. 

Se van llevando a los hombres uno por uno y les dicen que se lo lleven uno por uno. Asel no quiere que lo vuelvan a torturar y por tanto se suicida, para no volver a delatar a alguien al someterse a torturas. 

Descubren que Max es el traidor y cuando Asel se suicida tirandose por el hueco del ascensor, Lino tira a Max por el hueco del ascensor porque piensa que es el culpabe de la dos muerte.

Finalmente sacan a Lino i a Tomás diciéndoles que cojan todas sus cosas, dejando el final abierto.

La acción comienza in media res, en medio de unos acontecimientos que ya han ocurrido. 

Toda la historia se desarrolla en 4 dias, es una historia muy breve.