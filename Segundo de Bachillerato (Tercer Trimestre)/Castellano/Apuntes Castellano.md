# Apuntes Castellano

Raíz -> plenitud -> **plen**
Base láxica -> pleno (de la palabra donde viene)
Base de derivación -> **plen**. El segmento del que se parte en un proceso de derivación. plenitudismo -> **plenitud**

Soledad
Rraíz -> sol
Base léxica -> solo
Base derivación -> soledad

## Morfología Derivativa

Derivación -> Raíz + Afijos derivativos
Composición -> Raíz + Raíz *Recien + casado*
Parasíntesi
Acortamiento (abreviatura, acrónimos)
Siglación

Recién: adverbio apocopado del adjetivo reciente. Acción inmediatamente anterior a la situació comunicativa.
Recientemente: adverbio deadjetival de reciente. Algo poco tiempo antes pero no inmediatamente.

a) el barco está recién pintado.
b) el banco ha sido recién pintado.

La oración b es agramatical porque recién no puede aparecer con un tiempo verbal de aspecto perfectivo. En a si que es gramatical porque recién está acompañando un verbo en aspecto imperfectivo. 

### Morfema aspectual (Afijo flexivo verbal)
Acción acabada -> todos los tiempos compuestos + presente perfecto simple
Acción no acabada -> todo el resto de tiempos


5.2. 
a) El niño crecido
b) El niño caminado
B es agramatical porque el Complemeneto Nominal de niño es inergativo y estos verbos no pueden funcionar como Complemento Nominal al ponerse en participio. En cambio a si que es gramatical porque el complemento del nombre es un verbo inacusativo. 

Llegados los invitados, empezamos a cenar

Solo podemos formar esta clausula absoluta porque el verbo es inacusativo. En cambio llorados los invitados, reídos los invitados, no pueden formar clausulas absolutas.

## Morfemas Afijos Derivativos
Niñito -> -ito indica tamaño y no modifica base ni el significado
Niñero -> -ero modifica el significado pero no la base

Base: categoria gramatical

- Prefijos
- Sufijos:
	- Apreciativos:
		- Diminutivos, aumentativos, despectivos. Valor afectivo, tamaño y cantidad.
		- Diminutivo: -ito / -ico / -illo
		- Aumentativo: -ona / -ón / -azo
		- Despectivo: -aco  
	- No apreciativos:
		- Nominales: devolución 
		- Verbales: 
		- Adjetivos / Adverbios: devuelto
		
	Alegre:
	- Nombre -> alegría
	- Verbo -> alegrar (algr + ar (sufijo verbal))
	
	El sufijo -ble sirve para crear adjetivos a partir de verbos. El sufijo -ble no se puede usar con verbos inacusativos. 
	
	- amable
	- feíble	
	
	El segundo adjetivo es agramatical porque el sufijo -ble solo se puede usar con verbos transitivos e inergativos mientras que en *feíble* se usa con un adjetivo (*feo*).
	
Adverbio adjetival: con verbo, se forma con un adjetivo bloqueado en masculino singular. Ej:
- El avión vuela **bajo**. El pájaro canta **alto**.

El adverbio adjetival con el sufijo -mente en cambio solo admite adjetivos bloqueados en femenino singular. 


a) la avioneta vuela alta
b) la avioneta vuela alto

En a alta es un adjetivo que complementa a avioneta en b alto es un adverbio adjetival. Alta funciona com C.Pvo y alto funciona como un CC.Modo.  


a) perro mordedor
b) perro crecedor

Mordedor -> nombre que viene del verbo morder
Crecedor -> nombre que viene del verbo crecer

Los dos tienen el sufijo -dor. 

La secuencia b es agramatical porque crecedor lleva el sufijo -dor y este sufijo solo admite verbos inergativos e inacusativos. 


## Tipologia de los procesos compositivos

### Composición
Es un mecanismo para crear nuevas palabras. Consiste en unir dos o més raíces. 

*Tiovivo* -> Cambia de significado

Se clasifican en:
- Univerbal (ortográfica): Zampabollos. Se forman para juntar una única palabra, llevan un único acento ortográfico.
- Pluriverbal (sintagmàtica): Cajero automàtico, Físico-químico. Las raíces mantienen su independencia.
- Culta: Cardiopatía. Se forma únicamente con raíces grecolatinas. Palabras específicas de la ciencia y la tecnologia.

### Ejercicios 

- Vendeobreros -> universal (ortográfico)
- Fofisano -> universal (ortográfico)
- Riconvertir -> X -> Reconvertir -> Prefijo + Raíz, no hay composición.
- Culialto -> 
- Batamanta ->
- Pollocome -> X -> Comepollo -> La formula nombre + verbo no es productiva pero la de verbo más nombre sí lo es.

### Otros procesos de creación de palabras.

1. Desnivelas vs Desmelenar
Desnivelar > nivel -> nivel + ar (derivación con un sufijo) -> des + nivelar (derivación con un prefijo)
Desnivelar: raíz (nivel) + Base léxica (nivel) + base de derivación (nivelar)
Desmelenar > melena -> melen- + ar 
Desmelenar -> parasíntetica

2. Medioambiental vs Mediocampista
Medioambiental > unión de dos raízes. se puede decir medioambiente y ambiental.
Mediocampista > unión de dos raíces dependientes. no se puede decir mediocampo

### Parasíntesi
Mecanismo para crear palabras nuevas. Hay dos tipos de parasíntesi:

- Relacionado con la derivación -> Unir un prefijo y un sufijo simultáneamente a una raíz. 
Enrojecer: en (pref) + roj (raíz) + ec (inf) + er (sufijo)

- Relacionado con la composición -> Unir dos raíces simultáneamente y un sufijo.
Mileurista > mil (raíz) + euro (raíz) + ista (sufijo)