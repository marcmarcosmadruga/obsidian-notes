# Figuras Estilísticas

1. El autor del poema le habla al mar i se compara a si mismo con este, compara sus sentimientos con las olas del mar, porque van i vienen.

2. Reconocer
	1. Paradoja -> Que lejos, siempre, de ti mismo
	2. Epanadiplosis -> y vienen, van y vienen
	3. Apóstrofe -> mar
	4. Metáfora -> mil heridas (metáfora de las olas)
	5. Comparación -> tus olas van, como mis pensamientos
	6. Políptoton -> conocerse, mar, y desconocerse
	7. otro políptoton -> soledad, mar solo

Pollisíndeton -> Acumulación de conjunciones
Asínderton -> Eliminación de conjunciones 
(Ambos en enumeraciones)

- Repasar afijos flexivos

## Afijos

- Flexivos
	- Nominales 
		- Marca de palabra
		- Género
		- Número
	- Verbales
		- T/M/A
		- Persona y número
		- Vocal temática
- Derivativos

La marca de palabra aparece solo en aquellos sustantivos asexuados que acaban en vocal atona. Mes**a** 

Plenitud -> Es una palabra derivada porque sobre una raíz aplicamos un morfema afijo derivativo. 

a) maravilloso
b) fantasticoso

B es agramatical porque fantastico ya es un adjetivo y no se puede formar otro adjetivo añadiendole el sufijo -oso. Ya que el sufijo -oso se usa para formar adjetivos a partir de sustantivos (Marvilla -> Maravilloso). (*Afijo, Sufijo, Raíz, Base léxica*)

a) sobornable
b) llevable



