# Mi resumen de Morfología

## Morfemas
Los morfemas son la unidad básica con sentido.

La diferencia entre morfema flexivo y derivativo es que el flexivo no modifica ni la categoría gramatical ni el significado mientras que los morfemas derivativos sí que lo hacen. Los morfemas flexivos no crean nuevas palabras mientras que los morfemas derivativos sí. 

Los afijos derivativos intervienen en el proceso derivativo para crear nuevas palabras. Se clasifican de la siguiente manera:
- Prefijos: antepuestos a la raíz. *re*+hacer
- Sufijos: pospuestos a la raíz. nacion+*al*
- Interfijos: entre la raíz y sufijo o prefijo. en+*s*+anchar, cancion*c*ita

Los sufijos apreciativos añaden connotaciones afectivas, de tamaño o cantidad, sin modificar categoría o base léxica. Son los diminutivos, aumentativos o despectivos. Los sufijos no apreciativos pueden modificar tanto el significado como la categoría gramatical. 

