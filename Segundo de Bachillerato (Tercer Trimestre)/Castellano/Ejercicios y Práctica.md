# Ejercicios y Práctica

## 17-03-22

Fíjate, ¿Cómo segmentarías estas palabras?
- Arboleda:
	- Arb: raíz.
	- Ol: Afijo nominal, marca de palabra, infijo.
	- Eda: Afijo nominal derivativo.
- Cantaba:
	- Cant: raíz
	- aba: Afijo verbal tiempo
- Cochero:
	-	Coch: raíz.
	-	er: Afijo nominal derivativo.
	-	o: Afijo nominal flexivo de género.
- Mesa:
	-	Mes: raíz
	-	a: Afijo nominal marca de palabra
- Impresentable:
	- Im: es el prefijo in- pero la m es un alomorfo de la n al ir delante de una p.
	- present: raíz
	- able: Afijo verbal derivativo de tiempo
	
	
La Flexión Nominal y Verbal

1. María es una profesora excelente.
2. María es una persona excelente.
3. María es una tenista excelente.
4. María es una mujer excelente.

El primer caso tiene variación de género con marca flexiva, cambia la desinencia de _una profesora_ a _un profesor_. En el segundo caso no hay variación de género _una persona_ a _una persona_. En el tercer caso hay variación de género sin marca flexiva con un sustantivo común _una tenista_ a _un tenista_ y en el último caso hay variación de genero sin marca flexiva a través de heterónimos _una mujer_ a _un hombre_.

