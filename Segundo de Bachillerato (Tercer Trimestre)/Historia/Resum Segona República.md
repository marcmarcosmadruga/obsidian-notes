# Proclamació de la República i Període Constituent

> Falta aquí encima por resumir el contenido que está en /obsidian-notes/Segundo de Bachillerato/Historia

> También faltan por resumir todos los partidos políticos, no se bien bien como resumirlos

## Les Reformes de Bienni d'esquerres (1931-1033)
- Reforma religiosa
	- No confessionalitat de l'Estat
	- Supressió del pressupost de culte i clero
	- Divorci i matrimoni civil
	- Prohibició a l'educació
	- Llei de Congregacions (1933)
	- Eclesiastics s'oposen a la república

- Modernització exèrcit
	- Reduir efectius de l'exèrcit
	- Acabar amb la macrocefàlia
	- Fi al fur especial dels militars
	- Mesures:
		- Llei de Retir de l'Oficialitat
		- Suprimeixen rangs tradicionals
		- Redueixen nombres d'unitats i oficicials
		- Es tanca Academia General Militar de Saragossa
		- Suprimeixen capitanies generals, tribunals d'honor, Consell Suprem de Justícia Militar i premsa
	- Resultats militars, dificultat per modernitzar-ho per baix pressupost. La dreta anima a la revolta militar.

- Reforma agrària
	- Projecte de mes abast
	- Prohibir rescindició dels contractes d'arrendament
	- Jornada laboral 8 hores
	- Salaris mínims
	- Obligar als propietaris a posar en conrreu les terres aptes
	- Llei de Reforma Agrària (Setembre de 1932)
		- Expropiar sense indemnitzr les terres d'una part de la noblesa
		- Expropiar amb indemnització les arrendades sistemàticament, les que no es regaven i les ineficients.
		- Llei Encomanda al IRA
		- Resultats inicials molt escassos, molta burocracia, resistencia dels propietaris
	- Aquesta llei augmenta la tensió social
		- Els propietaris s'oposen i els jornalers estan decebuts amb els escassos resultats

- Reforma de l'Estat Centralista
	- Un altre qüestió prioritaria
	- Catalunya primera regió en iniciar el procès
		- Generalitat: Abril del 1931
		- Estatut: 1932
	- A Galicia el procès estatutari va trigar molt més
	- A País basc els nacionalistes del PNB redacten un projecte d'estatut. L'Estatut Estrella.
		- massa confessional, poc democràtica.
		- El PNB evoluciona cap al centre i un pacte amb les esquerres fa que s'aprovi l'autonomia al 1936.

- L'obra educativa i cultural
	- Important per la transcendència popular
	- S'hi destinen molts recursos
	- educació liberal i laica
		- laica, mixta, obligatoria i gratuit
		- Pressupost incrementat un 50%
		- Es creen 10000 escoles i 7000 places de mestre.
	- Es promouen campanyes culturals destinades als més pobres.

- Les reformes laborals
	- Millora de les condicons laborals (Francisco Largo Caballerao)
		- Llei de Contractes de treball: negociació col·lectiva
		- Llei de Jurats mixtos: 7 dies de vacances pagades.
	- S'estimula l'augment de salaris i la creació d'assegurances socials.
	- Setmana laboral establerta de 40 hores.
	- Sindicats agrícoles.
	- Rebudes amb rebuig per la patronal
	

## La Catalunya Autónoma
Reconeixement d'un govern autonòmic en mans de republicans i catalanistes d'esquerres. Volen elaborar un estatut d'autonomia.

L'estatut Núria proclama que la republica tenia un caràcter federal i que Catalunya era un estat autònom dins de la república Espanyola. La Generalitat assumiria competències exclusives en molts àmbits. L'Estatut es va aprovar. Les dones no van poder votar però moltes van signar per demanar-ho.

El 18 d'agost del 31 Macià presenta l'Estatut a Madrid. 