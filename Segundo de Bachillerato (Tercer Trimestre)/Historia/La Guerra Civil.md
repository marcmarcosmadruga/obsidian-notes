# La Guerra Civil
## Context Internacional
Etapa d'entreguerres (1919-1939), ascendeix el feixisme a Europa i crisi econòmica mundial Es consolida el comunisme a la URSS amb Stalin. Les democracies dèbils no podien aturar el totalitarisme (França, Regne Unit).
Els anys previs a la Segunda Mundial.
Gran polarització de la societat.

## Les causes
- Causes remotes: la dèbil revolució liberal, la mentalitat de l'exèrcit, l'enderreriment ideològic econòmic.
- Causes pròximes: l'equilibri d'incapacitats (cap grupo polític va ser estable), els motius internacionals (simpaties cap als règims feixistes).



## Del cop d'Estat a la Guerra Civil
El 17 de juliol de 1936, el coronel Yagüe, s'aixeca a Melilla en armes contra la República. La insurreció *Alzamiento Nacional* s'estén ràpidament a la resta del protectorat marroquí. Entre el 18 i 19 de juliol altres militars s'aixequen a diverses altres casernes de la Península. També s'afeigeixen els sectors civils falangistes i carlins (requetès). En dos dies s'havien fet forts a Pamplona, Sevilla Castella la Vieja (Castella i Lleó) i una part d'Aragó. El 19 de juliol Casares Quiroga va ser subtituit com a cap de govern per José Giral. 

José Giral va dissoldre l'exèrcit i va lliurar les armes a les milícies dels sindicats i dels partits del Front Popular (republicans, socialistes i comunistes). Una part de l'exèrcit es va mantenir fidel al govern. 

La insurreció va triomfar a l'Espanya interior, Galicia, Andalusia, a les zones agraries amb predomini dels grans propietaris o de petits propietaris molt conservadors, Sevilla i Saragossa. El cop d'estat va fracassar a zones industrials del País Basc, Catalunya, Madrid, València, Ast