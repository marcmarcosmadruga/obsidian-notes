## La generalitat provisional i l'Estatut de Núria

el 13 de Desembre de 1932 va ser president Lluís companys i Francesc Macià va ser triat president de la Generalitat. Macià mor un any després el succeix Companys. Macià va nomenar un govern format per Consellers del seu partit.

La Generalitat només va ser efectiva desde novembre de 1932 fins a l'octubre de 1934. 

A nivell econòmic es van crear els serveis d'estadística, l'Institut d'Investigacions Econòmiques i Caixes de Dipòsits. Es fomenta la creació de cooperatives i centre d'experimentació agrària. S'intenta solventar els problemes pagesos arrendataris, sobretot Rabassaires, redueixen un 50% de les rendes que pagaven als propietaris. S'inicia l'elaboració d'una llei per tal de permetre accés a la propietat.

Com a política social es fa la Llei de Bases (1934) que era un programa d'acció per organitzar els serveis de Sanitat i Assistència Social. Experiències per millorar la xarxa d'hospital, assistència medica, 

# 21-03-22
## Una Conjuntura Económica Desfavorable
El canvi de règim coincideix amb la depressió econòmica mundial. Va incidir de manera més febla que en altres països. La crisi també va paralitzar l'emigraico a Amèrica que suposava una valvula d'escapament. Aquesta crisi va agreujar els problemes interns: alt atur agrícola, repartiment desigual de la terra, poca competitivitat internacional, dèficit de balança comercial. 

S'afegeixen a més problemes derivats de la política econòmica del govern republicà:
	- 
	
## La Conflictivitat Social
La lentitud de les reformes va suposar la impaciència de molts treballadors. Es radicalitzen les posicions de ambdos bandos. Un paper fonamental va tenir la CNT que va aprofitar per al seu Projecte Revolucionari. Van augmentar les vagues, les insurreccions i les ocupacions de terres van anar augmentant progressivament. Tot això produeix el Desgast del Govern d'Azaña. El van desacreditar com a conseqüencia de les mesures adoptades per restablir l'ordre públic. 

## La reorganització de les dretes
Les forçes d'esquerra es van presentar per separat pero les forces de dretes es van presentar en coalició. Van conformar aquesta coalició el Partit Radical de Lerroux, els sectors catòlics i conservadors es van mobilitzar activament, es forma la CEDA (Confederació Española de Derechas Autònoms) i Renovación Española i altres grups minoritaris com la Comunió Tradicionalista, las JONS i falange.

Alguns sectors de l'exèrcit van voler aprofitar el descontentament per protagonitzar un cop d'Estat amb l'idea de fer un gir de la República cap a l adreta. L'any 1933 es crea la UME la Unión Militar Española (organització clandestina de militars de dretes i antireformistes). 

## Les eleccions 1933
Es van celebrar al 18 de novembre, l'abstenció va ser força alta. Van ser les primeres eleccions en que les dones podien votar. 

L'esquerra estava desunida, els republicans i els socialistes estaven enfrontats pels conflictes socials. Molts obrers es van abstenir (CNT). La dreta estava unida i organitzada: victòria dels partits de centredreta i alguns van anomenar aquest període com a Bienne Negre. 

Les dues forces polítiques amb millors resultats van ser el Partit Radical de Lerroux i CEDA de Gil Robles. Alcalà Zamora va confiar la formació de govern a Lerroux. 

## La Paralització de les Reformes
Es tornen les terres als nobles expropiats. S'anul·len les cessions de propietats mal conreades. Es dona llibertat de contractació que suposava la disminució dels salaris. 

S'intenta contrarestar la reforma religiosa amb l'aprovació d'un pressupost de culte i clero. 

S'amnistia als sublevats amb SAnjurjo i per als col·laborados amb la Dictadura de Primo de Rivera. 

Es respectà la educació pero es va reduir els pressupost. 

Les corts van paralitzar la discussió de l'estatu basc. 

Això va suposar la radicalització del PSOE i de la UGT. Proliferen vagues i conflictes l'any 1934. 

## La revolució d'octubre del 1934
Davant la mobilització obrera la CEDA reclama a Lerroux una acció més contundent o va retirar el suport parlamentari. El 5 d'Octubre del 1934 va atorgar 3 carteres ministerials a la CEDA. L'esquerra va interpretar això com una deriva cap al feixisme.

La UGT va dur a terme una Vaga General per impedir la consolidació d'un nou règim. Aquest moviment va fracassar a nivell nacional. El govern va decretar l'Estat de Guerra. Els esdeveniments van prednre un relleu especialment  greu a Astúries i Catalunya. 

### Astúries
Els minaires van anar a la Reolució Social. Va ser fruït d'un acord previ amb anarquistes, socialistes i comunistes. Els minaires armats van ocupar els pobles i també les casernes. Van crear comités revolucionaris als ajuntaments. Van assetjar la ciutat d'Oviedo i van enfrontar-se amb les forces de l'ordre. El govern va enviar desde Àfrica la Legió (F.Franco). La resistència va durar 10 dies. Després la repressió va ser molt dura.

### Catalunya
La revolta va començar amb el suport del president de la Generalitat, que volia evitar l'entrada al govern de la CEDA. El 6 d'octubre es proclama la Republica Catalana. Els partits i sindicat d'esquerra van organitzar una vaga general. Va fracassar per la negativa de la CNT i l'escàs suport. El govern declara l'Estat de guerra a Catalunya, l'exèrcit va ocupar el pala de la Generalitat. Va haver-hi molts detinguts entre ells tots els membres del govern de la Generalitat.

## La crisi del segon bienni
Les conseqüències de la revolució d'Octubre van ser importants:
- La CEDA va augmentar la influència en elgovern. Era partidària d'aplicar condemnes.
- Es tornen les propietats als jesuïtes
- Es nomena Jose M. Gil Robles ministre de la guerra i Francisco Franco, cap de l'Estat Major.
- Al Juliol del 1935, la CEDA presenta un projecte per modificar la Constitució: revisió del es autonomies, abolició del divorci, negació de les expropiacions de terres.

Es suspen l'autonomia de Catalunya i s'anul·là la Llei de contractes de conreu.

A la tardor del 1935 hi ha una forta crisi de govern: El Partit Radical es veu afectat per escàndols de corrupció: Cas de l'Estraperlo i cas de malversació de fons per part de diferents polítics radicals. S'agreujamen les diferències entre la coalició governamental. 	

Al desembre Alcalà Zamora, va decidir convocar Noves Eleccions Legislatives al febrer del 1936.

# EL Triomf del Front Popular
## Les eleccions de febrer del 1936
Els partits catalanistes d'esquerra s'agrupen en el front d'esquerra. Els seu programa integra amnistia política per a presos del 34, restabliment de l'Estatut, la Generalitat i la Llei de contractes de Conreu, donava suport al Front Popular que estava format per republicans d'esquerra socialistes i comunistes. 

Els partits dretans van formar el Front Català d'Ordre que era una coalició organitzada per la Lliga. A la resta d'Espanya el liderava la CEDA, alguns republicans de dretes i renovación espanyola. 

El nou govern va ser format majoritàriament per republicans d'esquerra, la resta de partits els hi donaven suport:
- Manuel azaña passa a ser president de la República.
- Santiago Casares Quiroga: cap del govern

El Front Popular va posar en marxa el programa pactat:
- Amnistia per als presos polítics
- S'obligà a les empreses a readmetre els obrers acomiadats
- País basc i Galícia van reprendre les negociacions per l'aprovació dels estatuts
- Es repremn el procés reformista

Els elements més radical de la CNT van defensar la necessitat d'accions revolucionàries, també un sector dels socaialistes. La dreta rebutja 