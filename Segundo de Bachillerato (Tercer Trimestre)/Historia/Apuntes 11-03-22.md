# Apuntes 11-03-22

## Generalitat provisional i l'Estatut de Núria
Reconeixement immediat d'un govern autonòmic amb el nom de *Generalitat de Catalunya*. Va estar en mans de republicans i catalanistes d'esquerres. Va assumir les competències de les 4 Diputacions. 

La tasca primordial va ser l'elaboració de l'Estatut d'autonomia. Es convocà l'assemblea de representants dels ajuntaments. Escollí una Diputació provisional que va rebre l'encàrrec de nomenar una comissió d'expert. Tenia 46 membres, presidits per Jaume Carner. Es va anomenar l'Estatut de Núria (20 Juny 1931). 

Aquest estatut tenia moltes competencies. Aquest estatut proclamava l'Estat de Catalunya dins de la federació espanyola. Possibilitat dels països catalans. 

Establia competències exclusives en ensenyament, cultura, policia, sanitat, obres públiques, agricultura, regulació del dret civil, ordanció territorial, règim municipal i tribunals de justicia. 

