# Resum Historia: La Segona República

> La primera parte la tienes en /obsidiant-notes/Segundo de Bachillerato/Historia

La dimissió dels polítics catòlics del govern va fer que hi hagués un moviment de fitxes. Manuel Azaña es va convertir en el cap del govern i Niceto Alcalà Zamora en president de la Ŕepública.

## Partits i Sindicats a la Segona República
**Izquierda republicana:** Fundada al 1934, fusió d'*Acción Repúblicana* i del Partit Republicà Socialista. Clases mitjanes i populars.

**PSOE:** Influència entre els treballadors. Dues corrents: socialdemòcrata (consolidar el règim republicà): Julián Besteiro i Indalecio Prieto; revolucionari (República camí cap al socialisme) Largo Caballero.

**PCE:** Nombre reduït de militants. Dolores Ibarruri (La Pasionaria)

La força sindical més important va ser la CNT de caire anarquista, molts afiliats. Fort arrelament a Catalunya. Dueslínies d'actuació: Trenstistes (sindicalistes de caire moderat, cert suport a la repúnlica), Federació Anarquista Ibèrica (FAI) més revolucionaris. Via insurrecional i armada.

### Formacions de Dreta

**Partit Radical:** Lerroux
**Dercha Liberal Republicana:** Niceto Alcalá Zamora

Els partits conservadors i catòlics es van esfondrar. Van sobreviure un grup petit: Partido Agrario, Partido Liberal Demócrata y Acción Española.

El partir més important va ser la *CEDA*. coalició electoral de 1933 José M. Gil-Robles. Defensa els interessos dels grans propietaris i de l'Església. Al País Basc va estar el Partit Nacionalista Basc. 

També hi havia grups monarquics: Renovación Española. José Calvo Sotelo. Volia liquidar el nou règim republicà. Va establir acords electorals amb els carlins (Comunió Tradicionalista).

Els grups de caire feixista van ser:
- JONS (1933)
- Falange Española (1933) Dirigit per José Antonio Primo de Rivera.

Es fusionen al 1934 i formen Falange Española de las JONS. Tenien una idologia antidemocràtica i una defensa a ultrança del nacionalisme espanyola. Hi destaquen Ramiro Ledesma, Onésimo Redondo i Jose Antonio Primo de Rivera.

## Partis a Nivell Català
La major part eren catalanistes. Les dues forces majoritaries eren:
- Lliga Regionalista (de dretes): Van acceptar la nova legalitat republicana. Tenia el suport d'una part dels industrials i grans propietaris agrícoles. Resultats electorals força reduits. Al 1933 adopten el nom de Lliga Catalana.
- Esquerra Republicana de Catalunya: Format un mes abans de les eleccions del 1931. Agrupa diferents secots: nacionalistes radicals, vells republicans i petites formacions republicanes i nacionslites. suport d'amplis grups de la petita burgesia, de la pagesia i dels obrers industrialitzats. Proposen ampli programa de reformes polítiques i socials. Militància àmplia, gran força electoral: Macià i Companys.

Els partits marxistes estaven representats per dos grups socialistes (Unió Socialista de Catalunya, Federació Catalana del PSOE) i quatre grups comunistes (Bloc Obrer i Camperol, Esquerra Comunista, Partit Català Proletari, Partit Comunista de Catalunya), escassa militancia, poca influència. 

Al 1935 s'ajunten Bloc Obrer i Esquerra Comunista i funden el POUM.
La resta de partits formen al 1936 el PSUC.


# Les Reformes de Bienni d'esquerres (1931-1933)
## La Reforma Religiosa
Un dels primers objectius va ser limitar la influència de l'església i secularitzar la societat espanyola. Es va plasmar a la constitució: No confessionalitat de l'Estat, llibertat d ecultes, supressió del pressupost de culte i clero, autoritzats el divorci i el matrimoni civil. També es prohibeix als ordres religiosos la dedicació a l'ensenyament.

Llei de Congregacions (1933) va posar límits a la possesió de bén per les comunitats religioses. Es preveu la possibilitat de dissoldre-les si suposen algun perill per a l'Estat. L'enfrontament més important va ser amb els jesuïtes, depenien d'un poder estranger. Va ser dissolta i els seus bén nacionalitzats. 

Una gran part del sector religiós va agafar aquesta reforma com una agressió. La jerarquia eclesiàstica manifesta llavors una oposició a la República, alguns son expulsats del territori espanyol. 

## La Modernització de l'exèrcit
L'exercit necessitava una profunda transformació. Manuel Azaña (que era el president del govern) va assumir la cartera de defensa. Va crear un exèrcit **profesional i democràtic**.

Per tal de crear-ho va: reduir els efectius de l'exèrcit, acabar amb la **macrocefàlia** (molts oficials per pocs soldats), posar fi al fur especial dels militars, assegurar-ne el sotmetiment al poder civil, acabar amb la tradicional intervenció en la vida política.

Per aplicar això va prendre diverses mesures:
- Llei de Retir de l'Oficialitat (1931): Tots els oficials havien de jurar lleialltat a la a la Constitució i a la República. Se'ls dona la opció de retirar-se amb sou als oficials i als capitans.
- Es suprimeixen alguns rangs tradicionals.
- Es redueixen el nombre d'unitats i oficials.
- Es tanca l'Academia General Militar de Saragossa.
- Es suprimeixen capitanies generals, reibunals d'honor, Consell Suprem de Justícia Militar i la premsa destinada a l'exèrcit.
- Es crea la Guàrdia d'Assalt (força d'ordre públic fidel a la República)

S'aconsegueixen resultats limitats. S'aconsegueix la disminució de les despeses de l'exèrcit, però la reducció de pressupost suposa dificultats per modernitzar material armament i equipament. La reforma també és rebuda com una agressió. La dreta aprofita aquest descontentament per anima la revolta militar contra la República.

## La Reforma Agrària
Va ser el projecte de més abast. El principal objectiu va ser posar fi al latifundisme i millorar les condicons de vida dels pagesos pobres. Els primers decrets van ser prohibir la rescindició dels contractes d'arrendament, van fixar la jornaada laboral en 8 hores, van establir salaris mínims i van obligar als propietaris de posar en conrreu les terres aptes.

La Llei de la Reforma Agrària va ser aprovada per les Corts al setembre del 1932. L'objectiu va ser aconseguir la modernització de l'agricultura i la milloora de la situació social de la pagesia. Permetia expropiar sense indemnitzar les terres conreades d'una part de la nobles. També es podia expropiar amb indemnització les arrendades sistemàticament, les que podien ser regades i no ho eren i les conreades ineficientment.

L'aplicació d'aquesta llei va ser encomanada a l'Institucio de Reforma Agrària (IRA). Els resultats inciails van ser molt escassos perquè era una tasca molt complexa, era molt lenta i tenia moltes dificultats burocràtiques, tenia una manca de pressupost per a les indemnitzacions, hi havia resistència dels propietaris que intentaven obstaculitzar-la. 

Degut a aquesta llei va augmentar considerablement la Tensió Social:
- Els grans propietaris es van oposar, van donar suport als grups d'extrema dreta.
- Els jornales estaven també decebuts amb els resultats, veiene frustrades les esperances de canvi, s'orientaven cap a posicions revolucionaries.


## La reforma de l'Estat centralista
També va ser una qüestió prioritària configurar un Estat que permetès dotar de autonomia a les regions que tinguessing sentiments nacionalistes. La Constitució del 1931 va ser el marc jurídic per iniciar aquest procés.

Catalunya va ser la primera regió a iniciar el procés. Es va crear la Generalitat de Catalunya a l'Abril de 1931 i es va aprovar l'Estatut al 1932.

Galícia tenia una consciencia nacionalista menys acusada. El procés estatutari va ser molt més lent i mai va ser aprovat.

Al País Basc els nacionalistes del PNB i els carlins, van redactar un projecte d'estatut. L'Estatut d'Estella. No es va arpvoar per massa confessional, poc democràtic, incompatible amb la Constitució republicana. L'evolució del PNB cap al centre i el pacte amb les esquerres fa que s'aprovi un estatut d'autonomia al 1936. Posteriorment es constitueix el govern basc presidit per Jose Antonio de Aguirre.

## L'obra educativa i cultural
Va ser una reforma molt important per la transcendència popular, s'hi van destinar molts recursos. El se objectiu era la educació liberal i laica. Era un model d'escola laica, mixta, obligatoria i gratuita. El pressuopst es va incrementar en un 50% i es van crear 10000 escoles i 7000 places de mestre a Espanya. Estaven convençuts de la Necessitat de millorar el nivell cultural de la població. Es va promoure campanyes culturals destinades als més pobres.

## Les reformes laborals
Tenien com a objectiu la millora de les condicions laborals. Les reformes van ser començades per Francisco Largo Caballerao i es van aprovar les següents lleis:
- Llei de Contractes de Treball: regualva la negociació col·lectiva.
- Llei de Jurats Mixtos: Poder d'arbitratge, 7 dies de vacances pagades.

Es va estimular l'augment dels salaris i la creació d'assegurancies socials. Setmana laboral establerta de 40 hores. Es reforça el paper dels sindicats agrícoles. Les reformes van ser ben revudes pels sindicats i els treballadors i amb irritació per part de les organitzacions patronals.


# La Catalunya Autónoma
Es va reconeixer immediatament un govern autonòmic (*Generalitat de Catalunya*). Va caure en mans de republicans i catalanistes d'esquerres. Va assumir les competències de les 4 diputacions. La seva tasca principal va ser la de la elaboració d'un projecte d'Estatut d'Autonomia. 

Es va convocar l'assemblea de representants. Es va escollir una Diputació provisional que va rebre l'encàrrec de nomenar una comissió d'experts. Va tenir 46 membres. Presidits per Jaume Carner, reunits al Santuari de Núeria. Es va redactar un avantprojecte d'Estatut (1931).

## L'Estatut de Núria
La sobriania havia de recaure en el poble de Catalunya i Catalunya es definia com un Estat autònom dins de la república Espanyola. El Català va ser la llengua oficial i es deixava la possibilitat de la federació dels països de Parla Catalana.

En aquest estatut la generalitat assumia **Competències Exclusives** en ensenyament, cultura, policia, ordre públic, sanitat, obres públiques, agricultura, regulació del dret civil, ordenació territorial, règim municipal i tribunals de justícia.

L'Estatut va ser aprobat per plebiscit. 99% dels votants i van votar un 75% del cens. Les dones encara no podien votar però es va signar un manifest per més de 400.000 per tal de que les deixessin votar, però encara no van poder. 

## L'estatut d'autonomia de 1932
Les eleccions a Corts del 28 de Juny del 1931 donen el triomf a Esquerra Republicana. Es van incorporar molts intel·lectuals, republicans i polítics d'esquerra. El 18 d'agost Macià presenta l'Estatut a les Corts de Madrid. Alguns sectors de la dreta van trobar que l'estatut posava en perill la unitat d'Espanya. Hi va haver tres posicions diferenciades:
- Govern Central: concedir a Catalunya una autonomia moderada.
- Parlamentaris catalans: front comú per reclamar una Autonomia Àmplia.
- Oposició de dretes que defensaven una Espanya Unitària.

La posició tan ferma del president del govern (Manuel Azaña) va tenir una gran importància en el resultat. El 9 de setembre de 1932 es va aprovar l'estatu d'autonomia de Catalunya, tot i que tenia diferencies amb l'estatut de Núria. 

Algunes d'aquestes diferencies van ser que Catalunya era una regió autònoma dins de l'Estat espanyol, tant el català com el castellà eren llengües oficials, les competencies van ser retallades i compartides amb l'Estat central i els confliectes entre la Generalitat i l'Estat es portarien al Tribunal de Garanties Constitucionals. 

## La Generalitat republicana
L'aprovació d'aquest estatu va suposar el tancament de l'etapa de la Generalitat provisional. Les eleccions al parlament de Catalunya es van convocar pel 20 de novembre de 1932 i es van formar dues coalicions: 
- Liderada per Esquerra Republicana: model social, republicà, progressista, laic i d'esquerra moderada.
- Al voltant de la Lliga: Projecte conservador, catòlic. Oposat al reformisme social i laïcisme de les esquerres. 

## La tasca de govern
la generalitat va ser força important durant el període efectiu entre el novembre del 1932 i l'octubre de 1934. 

### Terreny Econòmic
Es van crear serveis d'estadística, l'Institut d'Investigacions Econòmiques i Caixes de Dipósits. En l'Agricultura es va fomentar la creació de cooperatives i centres d'experimentació agraria. Els problemes dels pagesos arrendataris, es van reduir en un 50% les rendes que pagaven als propietaris. 

### Política Social
La llei de Bases (1934) va ser un programa d'acció per organitzar serveis de Sanitat i Assitència Social. Millora de les xarxes d'hospitals, assistència a malalts, atenció psiquiàtrica, campanyes de prevenció, vacunació i higiene pública, etc. Es va crear l'Institut Contra l'Autor Forçós. Es va crear el Consell del Treball i l'organització dels serveis d'assistència i previsió social.

### Ensenyament
Es fundaren moltes escoles. Noves escoles per formar mestres. Es dona autonomia a la Universitat de Barcelona. Es milloren les condicions laborals i salarials dels mestres. S'incorpora la coeducació i laïcitat. Es segueixen pautes dels moviments de renovació pedagògica. Colònies d'estiu per a nens. Més biblioteques populars, arxius, museus. S'aprofundeix en la Normalització de la Llengua Catalana. Bilingüisme a les escoles primàries i el català a tots els nivells d'ensenyaent i com a eina de comunicació social.

### Nova divisió territorial
Es proposa una nova divisió territorial. Era un model comarcalista. Les capitals de comarca van ser centres de serveis d'un conjunt de municipis. 38 comarques. Va ser finalment aprovada al 1936 amb la Guerra Civil ja començada. 

# Els Problemes de la Coalició Republicana
El canvi de règim va coincidir amb la fase més greu de la Depressió Econòmica. Tot i això, incideix a l'economia espanyola de manera més feble. La crisi també paralitza l'emigració a Amèrica. De totes maneres, la crisi va agreujar els Problemes Interns de l'economia espanyola: l'Atur agrícola, el repartiment desigual de la terra, la poca competitivitat internacional i el dèficit de la balança comercial. 

S'afegeixen altres problemes derivats de la política econòmica del govern republicà:
- El creixement dels salaris no va anar acompanyat d'un augment de la productivitat, disminueixen els beneficis de les empreses.
- Augmenta el descontentament i la desconfiança entre els empresaris industrials i els grans propietaris agrícoles.
- La inversió privada es va enfonsar.

S'opta per la disminució de la despesa pública, conseqüencies negatives sobre el sector desl bén d'inversió i la creació de llocs de treball.

## La Conflictivitat social
La lentitud de les reformes va suposar la impaciència de molts treballadors. L'atur va augmentar força i l'actitud patronal i dels propietaris agrícoles era de no negociació. Tot això va afovirar la radicalització. La CNT va aprofitar per portar a terme el seu Projecte Revolucionari. Les vagues, les insurraccions i les ocupacions de terres augmenten progressivament. Tot això produeix el desgast del govern d'Azaña. Es veu desacreditat com a conseqüència de les mesures adoptades per restablir l'ordre pública.

## La reorganització de les dretes
Les reformes i la conflictivitat social va distingir les elits econòmiques socials i ideológiques i també a les classes mitjanes.

Es va anar organitzant al voltant dels partits conservadors tradicionals o de noves organitzacions de caràcter feixista.

El centre dreta es va reestructurar al voltant del Partit Radical de Lerroux (atrau empresaris, comerciants i propietaris agrícoles). Es movilitza el sector catòlic i conservador. Es forma la CEDA al 1933 (José M. Gil Robles).

Renovación Española, la Comunió Tradicionalista i els grups feixistes de les JONS i de Falange van portar a terme una intenta activitat d'agitació.

Alguns sectors de l'exèrcit van volver aprofitar el descontentament dels grups més conservadors. El general Sanjurjo va protagonitzar un Cop d'Estat amb la idea de fer un gri de la República cap a la dreta però va fracasar.

# El Bienni Conservador
## Les eleccions del 1933: govern de dretes
Es van celebrar el 18 de novembre, l'abstenció va ser força alta i van ser les primeres eleccions on les dones van poder votar. Les dues forçes amb millors resultats van ser el partit radical de Lerroux i la CEDA de Gil Robles. Alcalá Zamora va confiar la formació del govern al Partit Radical tot i que va guanyar la CEDA.

L'esquerra estava desunida: els republicans i els socialistes estaven enfrontats pels conflictes socials. Molts obrers es van abstenir. La dreta en canvi estava unida i organitzada, va ser una victoria pels partits de centredreta. Alguns van anomenar aquest període com el Bienni Negre. 

## La Paralització de les reformes
Es va paralitzar gran part de la reforma agrària, es van retornaar terres a la noblesa, es van anul·lar la cessió de les propietats mal conreades i es va donar llibertat de contrctació que va suposar la disminució dels salaris. 

Es va intentar contrarestar la reforma religiosa aprovant un pressupost de culte i clero. 

Es va donar amnistia per als sublevats amb Sanjurjo i per als col·laboradors amb la Dictadura de Primo de Rivera. 

L'educació es va respectar però es va reduïr el pressupost.

Les corts van paralitzar la discussió del projecte d'estatut basc.

Com a conseqüencia es va radicalitzar el PSOE i la UGT i van proliferar vagues i conflictes (com els de l'any 1934).

## El conflicte rabassaire a Catalunya
A Catalunya, el govern de la generalitat continuava en mans del republicans d'esquerra. Moltes diferènces amb el govern central. Hi ha un enfrontament important arran de la qüestió rabassaire. A l'Abril del 34 s'aprova la Llei de Contractes de Conreu que garanteix als paesos rabassaires l'accés a la propietat de les terres que treballavel mitjançant el pagament als propietaris d'uns preus prèiament taxats pel govern. Els propietaris es mostren contraris. Envien la llei al tribunal de garanties constitucionals, la generalitat ho accepta i torna a aprovar una llei pràcticament igual. 

## La revolució d'octubre del 1934
