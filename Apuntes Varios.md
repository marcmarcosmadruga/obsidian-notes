## La Reforma Religiosa
Un dels primers objectius va ser limitar la influència de l'església i secularitzar la societat espanyola. Es va plasmar a la Constitució la no confessionalitat de l'Estat, la llibertat de culte, la supressió del pressupost de culte i el clero, la autorització del divorci i el matrimoni civil.

El govern va prohibir als ordres religiosos la dedicació a l'ensenyament. 

**Llei de congregacions:** (1933) Va posar limit a la possesió de béns per les comunitats religioses. Es preveia la possibilitat de dissoldre ordres religiosos si comportaven algun perill per a l'Estat. L'enfrontament més important va ser amb els jesuites. Bona part de l'esglesia manifesta l'oposició a la segona república.

### La Modernització de l'exércit
L'exercit necessitava una profunda transformació. Quan Manuel Azaña va arribar al poder va assumir la cartera de defensa. El volia reformar per crear un exèrcit professional democràtic. Volia reduir els efectius de l'exèrcit, acabar amb la macrocefàlia, posar fi al fur especial dels militars, assegurar-ne el sotmetiment del poder civil, acabar amb la tradicional intervenció castrense en la vida política.
