# pronom relatiu
Com a pronom, te la funció que faria el nom en el seu lloc.
- La persona que parlava se n'ha anat.
	- que sentíem parlar
	- a qui has donat el llibre


## Tipus de relatius
### Simples
- que: fa les funcions de qualsevol SN, subjecte i cd.
	- La noia que compra les entrades (subjecte) és molt simpàtica.
	- La noia que he conegut (cd) és molt simpàtica.

Mai va precedit de preposició pero pot anar precedit d'un article quan se sobreentén el nom de l'antecedent:
- Pensava en l'estudiant que fa filologia, pensava en la que fa filologia.

- què: fa les funcions d'un sintagma preposicional (menys el complement indirecte.) 
	- La conversa de què parlaves la vam tenir ahir.
		- **Malament -->** La conversa de la què parlaves la vam tenir ahir.
	- El cotxe amb què he vingut s'ha espatllat. 

- qui: fa les funcions d'un sintagma preposicional **i** el complement directe.
- on: equival a un CC de Lloc:
	- No he trobat la cosa on va néixer Verdaguer (CCL)
	- No he trobat la casa en què va néixer Verdaguer 


### Compostos
- Juntar els simples amb les particules el/la/els/les. Equival a les partícules simples pero nomès les podem utilitzar amb:
		- Rrelatives explicatives, quan van precedits de preposició feble, forta o locució i per fer equivocs.