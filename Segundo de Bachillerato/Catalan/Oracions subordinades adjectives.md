# oracions subordinades adjectives

També es poden dir oracions subordinades adjectives **de relatiu**. Perquè el seu nexe es un [[pronom relatiu]], aquest nexe té una triple funció: Fa d'antecedent, fa d'una funció sintactica dins de la subordinada i fa de nexe.

Solen estar al costat d'un nom (perquè els adjectius van al costat del nom generalment). 

Les Subordinades Adjectives poden ser especificatives o explicatives:
- Els excursionistes que estaven marejats no van fer el cim. (Especificatives)
- Els excursionistes, que estaven marejats, no van fer el cim. (Explicatives)