# Exercisis Oracions Compostes

## 4/2/2022
4. pàgina 113
	1. La pressió a la que està sotmesa és insuportables.
	2. Les noies amb qui simpatitzem són d'Alacant.
	3. Aquestes són les viles per on passarem en l'etapa d'avui.
	4. Els països on ha viscut són tots francòfons.
	5. La resolució a que feien referència és força desconeguda.
	6. La companya per qui sospirava nit i dia es diu Míriam.
	7. Coneixeu alguna altra persona a qui atribueixin poders màgics?
	8. S'ha posat malalta la jugadora amb quí comptàvem per al partit d'aquest vespre.
	9. L'escola on es va celebrar el campionat l'any passat l'acollirà també enguany.

5. pàgina 113
	1. Els companys a qui han estafat ja pugen a mitja dotzena.
	2. És un joc de rol en què ella no pensa participar.
	3. Aquestes són les qüestions de que s'ocupa durant el matí.
	4. els esports a què jugo més sovint són l'hoquei i l'handbol.
	5. es va negar a rebre les persones amb qui s'havia associat el seu ex.
	6. No vam entendre quin era el tema en que s'havia de centrar la sessió d'aquell dia
	7. Li van parlar molt bé d'un festival a què gairebé tots els seus amics ja havien assistit.
	8. L'home de qui es va seprrar era un maltractador i un bevedor empedreït.
	9. La notícia de que heu parlat tant ha resultat ser un muntatge publicitari.

6. pàgina 113
	1. Totes les empreses on havia treballat l'Oleguer han fet fallida.
	2. M'han fet una petició a què no puc accedir.
	3. Aquesta advocat ha de defensar uns casos on hi ha gat amagat.
	4. La paga amb què t'has de conformar no t'arribarà ni per comprar-te una bicicleta.
	5. L'incident de que tothom parla a hores d'ara es produï ahir a la nit.
	6. els projectes a que va donar el seu consentimenta han resultat un fracàs.
	7. Aquestes campanya publiiatàries en que heu participat no han proporcionat gaires guanys.
	8. *no la entiendo* 