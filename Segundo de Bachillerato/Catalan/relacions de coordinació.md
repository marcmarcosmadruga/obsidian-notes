# Relacions de coordinació
- Copulatives: i, ni
- Disjuntives: o, o bé
- Distributives: ni ... ni, o ... o, ara ... ara, no solament... sinó (qué) també
- Adversatives: però, sinó (que)
- Explicatives: és a dir, o sigui, això és
- Il·latives: doncs, per tant, per consegüent, de manera que
- Continuatives: doncs, a més d'altra banda