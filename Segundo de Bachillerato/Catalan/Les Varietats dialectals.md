# Les varietats dialectals

Varietat geogràfica = Dialecte

Organitzem els dialectes amb:
 - **Bloc**: oriental
 - **Sub-bloc**: sud-oriental
 - **Subdialecte**: tarragoní
 - **(Parlar)**: (si n'hi hagués, seria, per exemple, el calafellenc)
