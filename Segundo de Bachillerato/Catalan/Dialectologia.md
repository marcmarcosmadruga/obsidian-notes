# Dialectologia
## Fonètica
Té el mateix sistema voàlic que el nord-occidental, no tenen la voca àtona.
Tenen tendencia a obrir més les *e* i les *o*. 
Articula la labiodental sonora (menys en l'apitxat) a més algunes fricatives les africa (ditjous).
No emmudiment dels sons oclusius finals (camp, dolent) i pronunciació de la *r* final (sobretot l'apixat): *cantar, traballaor...*
Caiguda del a -d- intervocàlica: *llauraor, caminaor, mocaor, vesprà, cremà...*

## Morfosintaxi
**Molt important fixar-se en la primera persona del indicatiu, dona molt informació**
Desinencia \[e] en la primera persona de l'indicatiu de la primera conjugació (*cante, mire, menge...*), pero sense desinencia en la segona i tercera conjugació.
En apitxat s'utilitza l'ús del perfet simple: cantí, cantares, cantà
Us dels articles reforçats
Tres graus de demostratius (este, eixe, aquell, açò, això, allò)i ací i ahí.
Ús dels possesius àtons (mon, ton, son...) i tònics (meua, teura, seua).
No alteren l'ordre de CI+CD en la pronominalització.

## Lexic i semàntica
Ús freqüents de diminutius (exagerat)
Mots característics: agranar, atzucac, banyar, calces, colp, calbot, creïlla, meló d'Alger, eixir, gitar-se, llevar, llavar, rentar, escurar (els plats), volta (vegada)...