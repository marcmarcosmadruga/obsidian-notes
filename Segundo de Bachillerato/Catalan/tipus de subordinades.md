# tipus de subordinades
- [[Oracions subordinades adjectives]]: intercanviables per un adjectiu. *Participi*
- Oracions subordinades substantives: intercanviables per un substantiu. *Infinitiu*
- Oracions subordinades adverbials: intercanviables per un adverbi. *Gerundi*

La conductora (que havia estat víctima de l'accident) (Participi CN) va demanar com va poder que algu l'ajudès.

