## Fenomens de contacte consonantic
### Emmudiment
És quan certes consonants deixen de pronunciar-se segons la posició que ocupen en la paraula o sintagma. Això passa a final de mot en consonants oclusives (_penitent_) i vibrants (_carrer_). Les oclusives són mudes quan van darrerre de nasal i les vibrants en diversos més contexts.

Altres tipus d'emmudiments són les esses de certs plurals, en el demostratiu masculí (_aque**s**t_)

### Sensibilització
És el fenomen contrari a l'emmudiment: articular certes contextos quan harien de ser mudes.
- cen*t* anys [t] --> vint*t* daus []
- San*t* Antantoni [t] --> San*t* Cosme []
- Amagan*t*-ho [t]
- Canta*r*-la [r]
- am*b* algú [b]
- aque*s*t humà [s]

### Ensordiment
Les consonant oclusives sonores s'ensordeixen a final de mot, després d'una pausa o si els segueix una constontant sorda.
- club: la b final s'ensored [p]
- fred: la d final s'ensordeix [t]
- mag: la g final s'ensordeix [k

### Sonorització
**NO LO ENTIENDO PREGUNTAR AL PROFE**


### Assimilacions
**TAMPOCO ENTIENDO NADA LULERS**

### Geminacions
**JODER TAMPOCO ENTIENDO NADA LOLOL**