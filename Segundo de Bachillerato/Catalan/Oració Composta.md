# Oració Composta
Una oració composta es aquella que es compon de dues o més oracions.

- Coordinació: mateix nivell sintàctic
	- Hem nedat a l'estany _i_ ens hem quedat arrugats.
	- Es poden construir amb diferentes [[relacions de coordinació]]

- Subordinades: una de les oracions depen de l'altra (la subordinada depen de la principal)
	- diferents [[tipus de subordinades]]

- Juxtaposades: 
	- Les oracions son coordinades o bé subordinades, però no tenen un nexe d'unió.
	- per coordinació: *Plou; fa fred; vull marxar*
	- per subordinació: *No badis: (perquè) cauràs*

