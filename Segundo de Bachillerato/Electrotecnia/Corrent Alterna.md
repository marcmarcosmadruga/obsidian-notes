# Corrent Altern
## El corrent altern. Valors fonamentals.
El corrent altern és aquell el valor i sentit del qual va canviant en funció del temps. 
La expressió del corrent altern es:
$\epsilon = -\frac{d\phi}{dt}$
$B·S·cos(\phi)$

$\omega = ct.$
$\epsilon = \frac{d(BS\cos(\omega · t))}{d·t}$
$\epsilon =\omega · B · S · \sin(\omega · t)$
$\epsilon_{max} = \omega · B · S$
$\epsilon = \epsilon_{max} · \sin(\omega · t)$

$i = \frac{epsilon}{R} · \sin{\omega · t}$
$i = I_{máx} · \sin{\omega · t}$

#todo Poner foto de la representación de la onda

$Y_A = A · \sin{\omega · t}$
$Y_B = B · \sin{\omega · t + \phi}$
$\phi =$ angle de desfasament
<br>
## Paràmetres del corrent altern
- Valor màxim (Amplitud): 
- Valor eficaç (valor que donen els aparells de mesura): el efecte que faria un corrent de les mateixes característiques si fos continu. És el que s'utilitza per fer càlculs. 
	- $V_e = \frac{V_{máx}}{\sqrt{2}}$
	- $I_e = \frac{I_{máx}}{\sqrt{2}}$
	- $v = V_{màx} · \sin{\omega · t + \phi}$

- Valors instantanis:
	- Valors en una determinat $t$ --> $i, v$

- Valor mitjà --> Mitjana algeraica dels valors instantanis d'un semiperíode
	- $V_{mitja} = \frac{2·V_màx}{\pi}$
	- $I_{mitja} = \frac{2·I_màx}{\pi}$

- Temps que triga la ona en fer un cicle. T -> (s) (temps)
- Freqüència -> Número de cicles per segon. f -> (s^-1)
	- $T = f^{-1}$
- $\omega = \frac{2 · \pi}{T} = 2 · \pi · f$


## Circuits de corrent alterna

Resistencia --> Impedancia ($z$)

### Circuits de corrent altern amb un component passiu
#### Impedància i llei d'Ohm
En corrent altern l'oposició al pas de corrent s'anomena impedància --> ($z$)
Llei d'Ohm = $V = \frac{I}{z}$

#### Circuits amb una resistència Ohmnica pura
En un circuit amb una resistència ohmnica pura la Intensitat màxima i la V màxima estan fase. 

![[SCAN0366 (3).jpg]]

$p = v · i$
$v = V_{màx}·\sin{\omega·t}$
$i = I_{màx}·\sin{\omega·t}$
$p = V_{màx}·I_{màx} · (\sin{\omega · t})^2$

$P_{mitjana} = \frac{P_{màx}}{2} = \frac{V_m·I_m}{2} = \frac{V_m}{\sqrt{2}} · \frac{I_m}{\sqrt{2}} = V_e · I_e$

#### Circuit amb una auto-inducció (bobina)
En un circuit amb nomès una bobina la  $\epsilon$ està en fase. Mentre que la $i$ està desfasada 90º ($\frac{\pi}{2}$)
- Regla nemotecnica: Bobina -> bo**vi** (el voltatge va abans de la intensitat)


#### Circuit RC
![[circuit_rc.jpg]]
![[circuit_rc (2).jpg]]

<br>

#### Circuit RL
![[circuit_rl.jpg]]
![[circuit_rl (2).jpg]]



#### Circuit amb condensador
#todo poner foto del circuito con el condensador
$v = V_{max} · \sin{\omega · t}$
$i = I_{max} · \sin{\omega · t + \frac{\pi}{2}}$
$p = \frac{V_{max} · I_{max}}{2} · (\sin{2·\omega·t})$

$R = \frac{V}{I}$
$X_c = \frac{1}{c·\omega} = \frac{1}{2 · \pi · f · c}$


## Circuit sèria (RL, RC, RLC)
$\vec{V_t} = \sum{\vec{V}}$
$\vec{I_t} = \vec{I_i}$
$\vec{z_t} = \sum{\vec{z_i}}$