# Formulario

## Magnetismo
$L = \frac{\mu · N^2 · S}{l}$
$B = \frac{\mu_0 · I}{2 · \pi · d}$ <-- Fil indefinit
$B = \frac{\mu_0 · I}{2 · r}$ <-- Espira
$B = \frac{\mu_0 · N · I}{l}$ <-- Bobina/Solenoide
$\rm I\!R = \frac{l}{\mu · S}$ <-- Reluctancia magnètica

$F_{mm} = I · N$
$\rm I\!R = \frac{F_{mm}}{\phi}$
$\phi = B · S$


## Corriente Alterna
$p = v · i$
$v = V_{màx} · \sin{\omega · t}$
$i = I_{màx} · \sin{\omega · t}$
$p = V_{màx} · I_{màx} · (\sin{\omega · t})^2$

$X_c = \frac{1}{c·\omega}$
$X_L = L · \omega$

$V_e = \frac{V_{màx}}{\sqrt{2}}$
$I_e = \frac{I_{màx}}{\sqrt{2}}$

$V_{mitjà} = \frac{2·V_{màx}}{\pi}$
$I_{mitjà} = \frac{2·I_{màx}}{\pi}$

### Paralel 
$\vec{Y} = \frac{1}{\vec{z}}$ --> $\vec{Y} = \vec{G} + \vec{B}$
$B_L = \frac{1}{\vec{X_L}}$
$B_C = \frac{1}{\vec{X_C}}$
$G = \frac{1}{\vec{R}}$

### Potencies
$\epsilon · I = V_R · I + V_x · I$
Aparent (VA) = Activa (W) + Reactiva (VAr)
- Aparent: S
- Reactiva: Q
- Activa: R

$Freq = \frac{1}{2 · \pi · \sqrt{LC}}$