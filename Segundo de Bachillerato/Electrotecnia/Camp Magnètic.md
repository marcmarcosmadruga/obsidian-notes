# Camp Magnètic Electrotecnia
$\overrightarrow B = Tesla (T)$
$1 \; T = 10000 \; G$ (Tesla → Gauss)
$1 \; Wb = T·m^2$

$B=\mu_0 · \frac{I}{2\pi d}$ (Fil conductor infinit)
$B=\mu_0 · \frac{I}{2 d}$ (Espira)
$B = \mu \frac{NI}{l}$ (Solenoide)

$\mu_r=\frac{\mu}{\mu_0}$
$H = \frac{N·I}{l}$
$B = \mu H$
$R = \frac{Fmm}{\phi}$
$R = \frac{l}{\mu · S}$
$Fmm = I·N$ ( Intensitat pel numero de voltes)
$\phi = B·S·\cos(\alpha)$
Reluctancia = Amperes·Volta / Webbers (És la "resistencia magnètic")
$\mu_0 = 4\pi·10^{-7}$

La suma de reluctancies funciona igual que les sumes de resistencies (paralels i serie). Diferenciem entre circuits homogenis i heterogenis (potser hi ha un entreferro o un altre tipus de ferromagnètic).


## Bobines
Si fem circular un corrent elèctric per una bobina sen's crea un camp magnètic.
L'autoinducció es un fenoment que consisteix en el fet que un corrent elèctric variables genera un camp magnètic variable que indueix un corrent addicional que s'oposa a l'inicial.

$\epsilon = - \frac{d\phi}{dt}$ <-- Força electromotriu induida.

La força electromotriu induida s'oposa en sentit a la  força electromotriu inicial. El paràmetre que regula aquest fenomen és la inductancia (coeficient d'autoinducció) ($L$)

En un circuit de una pila dues bobines amb un nucli en mig és la base d'un transformador. El corrent elèctric crea un camp magnètic que així mateix indueix un corrent elèctric que rep la segona bobina amb una altra diferencia de potencial (depenent de les caracterísitiques de la bobina).
La bobina per tant pot actuar com a generador (si la fem inicial decreix) o com a receptor (si la fem inicial creix).

L'efecte de les bobines en un circuit elèctric és la inductancia.
$\epsilon = - L \frac{\triangle i}{\triangle t}$
$L = N \frac{\triangle \phi}{\triangle i}$ <-- N = Numero d'espires
Les unitats de la inductancia es el henry ($H$)

La autoinducció de la bobina ($L$) es un valor constant que depend de la bobina i les seves característiques (Numero d'espires, permeabilitat magnètica ($\mu$), superficie de cada espira ($S$) i la longitud de la bobina($L$)).
$L = \frac{\mu · N^2 · S}{l}$

La inducció mútua té lloc entre bobines molt properes. Es representa amb el coeficient d'inducció mutua ($M$). Es la relació entre el flux creat per una de les bobines quan travessa l'aletre i la intensitat de corrent que genera aquest flux.
$M = \sqrt{L_a · L_b}$ <-- Té Henrys com a unitat ($H$)
$L_t = L_1 + L_2 + 2M$
<br>

## Comportament d'una bobina dins d'un circuit elèctric
Si obrim i tanquem un circuit elèctric a on hi ha una bobina, tenim una variació del corrent.

**Insertar imagen de los apuntes del circuito #todo**

Posició 1: Apareix al circuit un corrent que passa de zero a un valor determinat --> genera un flux magnètic creixent que crea una fem d'autoinducció ($\epsilon '$) oposada a la fem.
$\epsilon '= - L \frac{\triangle i}{\triangle t}$
$i = \frac{\epsilon}{R} · (1-e^{-R·t/L})$ <-- Després de resoldre la equació diferencial.
$z = \frac{L}{R}$
Aquesta es la constant de temps (al igual que amb els condensadors), es el temps qeu triga la bobina tenir el 63,21% de la Imàx.

**Insertar imagen del gráfico de carga de una bobina #todo**

Al desconectar el circuit, el corrent no s'anul·la instantàniament sinó al cap d'un temps (degut a la fem induida). 

Energia emmagatzemada en una bobina: $\frac{1}{2}·L·I^2$
Associació en sèrie de bobines: $\epsilon = \epsilon_1 + \epsilon_2$
