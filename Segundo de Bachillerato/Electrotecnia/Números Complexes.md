# Números Complexes

Els nombres complexes surten de donar resultat a equacions de segon grau amb nombres negatius. 
$$\sqrt{-1} = i$$
En aquest cas com no hi ha cap nombre que doni arrel quadrada de menys u, li assignem el nombre i, que es un nombre imaginati.
Un número complexe és un parell ordenat de números reals.

$z = (a , b)$
a --> part real
b --> part imaginària

$z = a + bi$ Representació matemàtica
$z = a + bj$ Representació física

$z = r · \phi$ R = mòdul, phi = angle
$r = \sqrt{a^2 + b^2}$
$\tan{phi} = \frac{b}{a}$
$z = 4_{30º}$

## Operacions amb números complexes
### Suma i resta
$z_1 = a + bj$
$z_2 = a' + b'j$
$z_1 + z_2 = (a + a') + (b + b')j$

### Multiplicació
$z_1 = r_\phi$
$z_2 = s_o$
$z_1 · z_2 = (r · s)_{\phi · o}$

$z_1 = a + bj$
$z_2 = a' + b'j$
$z_1 · z_2 = a·a + ab'j + ba'j + b·b'·(-1) = (a'a - b'b) + (ab' + ba')j$

### Divisió
$z_1 = r_\phi$
$z_2 = s_o$
$\frac{z_1}{z_2} = (\frac{r}{s})_{\phi - o}$

### Per treure numeros imaginaris del denominador
$\frac{z_1}{z_2} = \frac{a+bj}{a'+b'j} · \frac{a' - b'j}{a' - b'j} = \frac{aa' - ab'j + ba'j + bb'}{a'^2 - b'2 · (-1)}$
La $j^2$ es transforma en $-1$.