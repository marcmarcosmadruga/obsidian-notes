# La Fallida de la Restauració

## L'impacte de la Gran Guerra
La Primera Guerra mundial va escalatar al 1914 durant el govern d'Eduardo Dato. Van declarar la neutralitat d'Espanya. Tot i mantenirse neutrals la societat espanyola es va dividir en dos: aliadofils i germanofils (partidaris dels aliats i els alemanys). 

Espanya es transforma llavors en el rebost d'Europa. Va vendre productes industrials i agraris al reste d'Europa, als dos bandos de la guerra. La pujada de preus degut a la guerra va provocar una __inflació__ que va disminuir el poder adquisitiu del poble espanyol. Això va augmental la conflictivitat social provocant la primera gran crisis de la segona fase de la Restauració. 

Aquesta va ser la crisi del 1917. Comportava dificultats al sistema polític, descontentament militar i conflictivitat social:
- La protesta militar: L'exèrcit presentava un __nombre excessiu d'oficials__ en relació al de soldats, els ascensos s'obtenien per mèrits de guerra (això feia que els d'Àfrica tinguessin més que els de la península). Aquesta desigualtat va generar un descontent generalitzat. Aqest descontent va provocar que es fundessin les __Juntes de defensa__. Aquestes reclamaven un augment salarial i que s'ascendìs per antigitat.
- La crisi política: al 1916 el govern estava presidit per Romanones que va __clausurar les Corts__ per acusacions de corrupció política. Al 1917 el conservador Dato va ser el nou cap del govern i li van reclamar la reobertura de la cambra. Dato __es nega__ a reobrir les cambres i es declara __l'Estat d'Excepció__ (s'anulen les garanties constitucionas) i s'endureix la censura de premsa. Al Juliol del 1917 es va celebrar a Barcelona una assemblea de paralamentaris catalans que exigeix la formació d'un govern provisional que convoquès corts constituents per reformar el sistema polític i descentralitzar el Estat. Aquesta convocatoria va ser dissolta per la guardia civil. Aquest moviment parlamentari va desapareixer perquè ni forces monarquiques ni juntes de defensa va donar suport, discrepancies entre catalanistes de esquerra y de dreta y per inhibició de la burgesia.
- La crisi social: la CNT i la UGT escriuen un manifest conjunt que demana la **intervenció del govern per aturar la pujada de preus**. A l'Agost el conflicte ferroviari a València provoca la crida a la vaga general. La protesta s'estèn per tota Espanya i **adquireix un caràcter polític** (republicans). Demandaven el fi de la monarquia, la formació del govern provisional per convocar Corts Constituents i el pas a un sistema republicà. Aquesta revolta va fracasar perquè **no van participar els pagesos** i el govern va declarar la **llei marcial** i va enviar l'exèrcit. La vaga va fracassar però la resposta va fer que **s'afeblís el regim** i va fer que es **radialitzès l'oposició** obrera.

Per solucionar la crisi del 1917 es va recorrer a la formació d'un govern de concentració. Un govern de concentració pretén aglotinar diferents tendencies polítiques per eliminar diferencies. Aquest govern va fracasar perquè tots eren massa diferents. 

A l'any de 1918 es torna al sistema del torn, però no hi havia gaire estabilitat per una gran fragmentació dels partits. Aquest partit aprovava les lleis a base de decrets llei, saltant-se la constitució i deixant de banda les cambres.

## La campanya per l'autonomia

La lliga i els dirigents de la Mancomunitat van encertar al 1918 una campanya en defensa de l'autonomia. A principis de 1919 es presenta a Madrid un projecte d'estatut de Catalunya que proposava la formació d'un govern català i un parlament elegit per sufragi universal.

El govern central es va oposar durament. Aquesta campanya va coincidir amb un gran moment de conflictivitat social a Catalunya. La lliga acaba abandonant el projecte de autonomia per tal de sufocar les revoltes dels obrers (per tal de no perdre els diners).

### L'enfortiment del catalanisme d'esquerres
- Al 1917, Frances Layret i Lluís Companys, organitzen el Partit Republicà Català. 
- Al 1922, un grup descontent amb la política de la Lliga abandona el partit i crea Estat Català, amb postulats independentistes.
- Al 1923 es crea una organització estrictament catalana, Unió Socialista de Catalunya, encapçalada per Manuel Serra i  Rafael Campanals. 

### La conflictivitat social
Els anys posteriors a la Primera Guerra Mundial van estar marcats per una gran conflictivitat a Europa. A Espanya, el final de la guerra va suposar un canvi brusc en les condicions econòmiques.
A Espanya, la producció disminueix, l'atur augment i els preus també van augmentar. Això va provocar més mobilització social i l'augment del sindicalisme.

A Andalúsia, la situació misèria dels pagesos va donar lloc al **Trienni Bolxevic (1918-1921)**.
Els anarquistes i també els socialistes van impulsar revoltes pageses. Motius: fam de terres i deteriorament condicions de vida.
Es declarà l'Estat de Guerra, es van il·legalitzar les organitzacions obreres, es van detenir els seus líders (la Mano Negra). S'especula que va ser una organització fantasma creada per l'Estat per justificar les repressió en contra del camp Andalús.

A Catalunya el conflicte rabassaire es va accentuar. Aquests contractes es feien a rabassa morta (el contracte dura fins que es mor la vinya). Quan va arribar la filoxera, que fa caure la producció de raïm, no poden pagar les terres.  Es creà la Unió de Rabassaires (1922) que va ser un sindicat de pagesos.

Al 1921, hi va haver una vaga a La Canadenca (empresa elèctrica), un dels punts més importants sobre aquesta vaga va ser la reivindicació de la jornada de vuit hores. Es paralitza el 70% de la indústria de tota l'area metropolitana de Barcelona durant un mes i mig. Va acabar amb un acord amb la patronal que no es va complir del tot. Es va reempendre la vaga i la patronal va respondre amb el tancament d'empreses (*lockout*) i la repressió dels sindicats.

La federació patronal creà al **Sindicat Lliure** per restar força a la resta de sindicats i enfrontar-s'hi. Van llogar pistolers per assasinar els principals dirigents obrers.

Els grups vinculats amb la CNT van portar a terme atemptats contra: autoritats, patrons, encarregats i forces de l'ordre en conseqüencia. El governador de Barcelona va protegir els pistolers de la patronal, va exercir una dura Repressió contra els Sindicalistes. Aplicà la **Llei de Fugues**, la policia podia disparar contra qualsevol detingut que intentés fugir. 

## La dictadura de Primo de Rivera
