# Resumen de la crisi de la Restauració
## El Reformisme Dinàstic
La reina confía el govern al conservador: **Francisco Silvela**. Impulsa una política reformista. Descentralització administrativa. Imposa impostos nous (deutes de Cuba). Tancament de Caixes.

El govern volia embargar als que no pagaven. El alcalde de Barcelona dimiteix i succeïx una vaga general de comerciants. El govern empresona als morosos, suspent garanties constitucionals i declara l'estat de guerra. Duran i Basa i Polavieja dimiteixen dels seus càrrecs.

El tancament de caixes trenca la relació entre els partits dinàstics i les elits catalanes. El catalanisme es consolida com a alternativa. Es reprèn el torn dinàstic.

### Reformes de Maura i Canalejas
Al 1902 puja al tró Alfons XIII i es renova la presidencia. El partit conservador serà presidit per Maura i el liberal per Canalejas. El primer govern de Maura (1904) impulsa les següents mesures:
- *Reforma des de dalt*
- Les masses neutres com a base social. Estat fort que desbanquès als cacis i limites el protagonisme de les classes populars.
- La nova llei electoral de 1907 va més difícil el frau.
- Projecte de Reforma de l'Administració Local
- Aprova llei de descans dominical i l'Institut Nacional de Previsión.
- Adopta mesures econòmiques per reactivar l'industria.
- Llei de Colonització Interior per estimular l'agricultar.
- Setmana Tràgica (1909).

Al 1910 Canalejas accedeix al govern:
- Intenta apropar-se als sectos populars
- Ley del Candado, separació església-estat
- Impost progressiu sobre les rendes, llei de lleves (1912) (obligatorietat del servei militar), lleis per millorar les condicions laborals.
- Negociació d'una llei de Mancomunitats.
- Assasinat al 1912.

## Les forces d'oposició
### Republicanisme i lerrouxisme
Principal força d'oposició, però molt fragmentats. 
Al 1903 es crea Unió Republicana encapçalada per Nicolás Salmerón. S'apropa al catalanisme arran de les eleccions de 1907, es va integrar en la coalició Solidaritat Catalana, rebutjar per un sector. 
El lerrouxisme va ser molt influent a Barcelona entre les classes populars. Lerroux es separa a l'any 1908 i funda el **partit radical**.

### Carlisme i tradicionalisme
Mort Carles de Borbó converteix el seu fill Jaume no pretendent a la corona.
Un dels seus dirigents Juan Vàzquez de Mella funda el Partit Catòlic Tradicionalista (1919). L'any 1906 es funda el Requetè: organització de caràcter paramilitar contra republicans i obrers.

Més tarde (1931) es reunifiquen en un sol partit: **Comunió Tradicionalista**.

### Les forces obreres: socialisme
Augmenten el nombre de militants i les seves actuacions.
El PSOE defensava l'acció política, aprofitar oportunitats que donava el sistema. El 1910 Pablo Iglesias participa a les eleccions a l'acta de diputat de la coalició republicanosocialista. UGT creix més (Francisco Largo Caballero 1918).

El Partit Comunista d'Espanya es funda al 1921 amb militants molt actius a Biscaia i Astúries.

### L'anarcosindicalisme
Es crea al 1907 Solidaritat Obrera. CNT al 1910. Es definia com a revolucionaria amb 4 principis bàsics:
- Independència del proletariat respecte de la burgesia i les seves institucions.
- Apoliticisme del moviment obrer (absentirme electoral).
- Unitat sindical.
- Enderrocar el capitalisme.

Això ho pretenien aconseguir amb vagues i boicots **Vaga General Revolucionaria**. Pujen afiliats després I Guerra Mundial. Salvador Seguí, Àngel Pestaña i Joan Peiró.

## Catalanisme i Republicanisme
### Consolidació de la Lliga Regionalista
Partit hegemònic a Catalunya. Enric Prat de la Riba a Nacionalitat Catalana (1906). Francesc Cambó era el portaveu dels interessos catalans a Madrid. Reclamen el dret de Catalaunya a autonomia. Disposats a intervenir a política per modernitzar l'estat.

Va tenir dificultats: lerrouxisme, discrepàncies ideologiques internes.
El sector més crític fundarà una nova formació de caire republicà.

### Solidaritat Catalana
El rebuig a la llei de jurisdiccions va formar solidaritat catalana, que reafirmava la personalitat i drets de Catalunya. Aplega totes les tendencies catalanes menys lerrouxistes i dinàstics.

Al 1907 presentent **el programa de tívoli**, derogació de la llei de jurisdiccions i autogovern. Va ser un triomg.

Maura negocia certes lleis amb ells. La discrepancia entre partits va ser l'element més important pel trencament. Algunes d'aquestes discrepancies van ser:
- Projecte de reforma de l'administració local. Uns el rebutjaven per poc democràtic els altres volien negociar-ho.
- Projecte de l'ajuntament de Barcelona (escoles en català, neutralitat religiosa). La Lliga defensà l'església.
- Fets de la setmana tràgica (1909), alguns veuen la repressió amb bons ulls.

### Catalanisme d'esquerres
Els escindits de la Lliga funden el Centre Nacionalista Republicà (1906). Nacionalista, democràtic i republicà.

Activisme al diari el Poble Català i fundació de molts ateneus. Sindicat *Centre Autonomista de Depdendents del Comerç i del a Indústria*. L'apropament entre les forces d'esquerra va fer que el 1910 es fundés la *Unió Nacionalista Republicana*. Bons resultats a les eleccions. Aliança amb Partit Radical de Lerroux -> pacte de Sant Gervasi (descontent electoral). Crisi de la Unió Federal Nacionalista Repúblicana, desapareix l'any 1916.

### Impacte de la Setmana Tràgica de Barcelona
La conferencia d'Algesires (1906) i el Tractat Hispanofrancès (1912) suposen el protectorat francoespanyol al Marroc. 

Interès al territori del Rif. Gran derrota a la Batalla del Lobo (1909). Es crida als reservistes. Grans protestes. Les mobilitzacions comencen a BCN el 18 de Juliol. El 24 de Juliol es construeix un comitè que crida a la vaga general. La revolta dura una setmana (antimilitarista, rebuig a l'hegemonia social i cultural de l'Església). Es declara l'estat de guerra. Repressió molt dura.

L'actuació del govern genera protestes que fan que caigui Maura.

## La Mancomunitat de Catalunya
Procés de formació difícil. El 1911 la Diputació de Barcelona promou la iniciativa de mancomunar les 4 diputacions catalanes i va presentar una proposta. El 1913 Dato publica un decret que autoritza a les diputacions a mancomunar-se per a fins **exclusivament administratius**.

La Mancomunitat de Catalunya es constitueix el 6 d'Abril de 1914. Enric Prat de la Riba com a president i assemblea amb 8 membres. Només va ser una delegació de les funcions de les diferents diputacions. La Lliga tenia una presencia majoritaria. Política orientada a estimular el desenvolupament cultural i la moderanització econòmica i social de Catalunya.

Tres presidents: Enric Prat de la Riba (1914-1917), Josep Puig i Cadafalch (1917-1924) i Alfons Sala (1924- 1925).

Va serguir la política iniciada per la Lliga des de la Diputació: creació serveis públic i foment de llengua i cultura catalana. Millora xarxa viària, sistemes postal i telefònic. Pla d'acció agrària.

Projecte en la reafirmació de la llengua i cultura catalana: la creació de biblioteques, escoles experimentals i estudis normals, la tasca d'unificació ortografica (Pompeu Fabra), divulgació i protecció del patrimoni cultura i renovació pedagògica.

## La Crisi del 1917
La neutralitat d'Espanya implicà exportació de productes industrials i agraris als països bel·ligerants. Pujada de preus interiors, redueix nivell adquisitiu. Moviment vaguístic al 1917.

Tres sectors estaven descontents:

**La protesta militar**: Nombre excessiu de oficials en relació als soldats, africanistes progressaven més rapid que els peninsulars, salaris baixos. Juntes de defensa (1918) es llença un manifest cridant a la renovació política.

**La crisi política**: El 1916 amb el control de Romanones, clausura les corts per acusacions de corrupció política. El 1917 es nomena a Dato que no reobre la cambra, decalara l'Estat d'excepció i s'endureix la censura de premsa.

Es celebra a Barcelona una assemble de parlamentaris catalans que exigeixen govern provisional i convocament corts constituents per reformes i descentralització de l'Estat. Nomès assisteixen el 10%, el govern prohibex la convocatoria i la guardia civil la disol. El moviment parlamentari desapareix perquè ni monàrquics, ni les juntes de defensa donden suports. Discrepancies entre catalanistes. La burguesia es va inhibir.

**El conflicte social**: Al Març la CNT i UGT manifest per intervenció de la pujada de preus. A l'Agost conflicte ferroviari a València provoca la Vaga General d'UGT. S'estén per tota Espanya. Es reclama fi de la monarquia, convocar corts constituents i els pas a un sistema republicà.

## La Descomposició del Sistema
Per solucionar aquest conflicte es recorrer al Govern de Concentració. Integrar líders Dinàstics i Catalanistes de la Lliga, massa diferents. Al 1918 es torna al torn, sense gaire estabilitat.

Al 1921 es crea un nou govern de concentració (Gobierno Nacional). Integral líders de difernts fracciosn dinàstiques, un de la Lliga i un del Partido Reformista.

Entre 1918 -> 1923: 10 canvis de govern. Alguns suspenen les garanties constitucionals.

La Lliga i els dirigents de la Mancomunitat van fer al 1918 campanya per la autonomia. Es presenta un Projecte d'Estatut, que proposa la formació d'un govern català i un parlament escollit per sufragi universal. El govern espanyol i el monarca s'oposen.

Les forces d'esquerra propsen una renovació dels seus principis per catalanitzar el republicanisme. Volien atraure obrers. Al 1917 Franscesc Layret i Lluís Companys organitzen el Partit Republicà Català. Al 1922 Francesc Macià funda Estat Català amb postulats independentistes. Al 1923 es crea la Unió socialista de Catalunya (Manuel Serra i Rafeal Campanals).

Després de la primera guerra mundial la situació de misèria dels pagesos andalusos dona lloc al Trienni Bolxevic.

Al 1921 la vaga a la Canadenca paralitza un 70% de la industria durant un mes i mig. Acord amb la patronal que no es va complir del tot. Es reempren la vaga i la patronal respon amb el tancament d'empreses i la repressió de sindicats.

La patronal va crear el Sindicat Lliure per restar força als altres (sorgeix el pistolerisme) (1916-1923). El governador de Barcelona protegeix els pistolers però reprimeix als sindicalistes. Va aplicar la llei de fugues.

### El problema del Marroc. Annual.
Al 1921 s'envia al general Silvestre que era partidari d'atacar les tribus. L'exercit va ser derrotat a la batalla d'Annual. S'anuncia la discusió al Parlament del cas d'Annual. Els parlamentaris obren l'Expedient Picasso, que revelava la relació del rei amb el problema del Marroc.

## Dictadura de Primo de Rivera
El 13 de Setembre de 1923, Primo de Rivera declara l'Estat de Guerra. Ho justifica com una solució per posar fi a la crisi política i a la conflicitivitat. Causes per justificar-ho: Inestabilitat i bloquig sistema parlamentari, desprestigi per frau electora, por a una revolució social, perill de trencament d'Espanya.

Va anunciar per mitjà d'un manifest que tenia la voluntad d'acabar amb el caciquisme, posar fi a la corrupció política, a la indisciplina social i a les amenaces a la unitat nacional.

La dictadura de Primo de Rivera va tenir dues fases: el directori militar (1923-1925) i el directori civil (1925-1930).