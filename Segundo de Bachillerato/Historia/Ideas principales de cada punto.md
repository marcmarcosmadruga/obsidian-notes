# Ideas principales de cada punto

## La modernització de l'estructura demogràfica
Al 1900 Espanya estava molt endarrerit en quant a estructura demogràfica es referia, les seves xifres eren més propies de l'àntic regim que de una societat moderna. Tot i això a les primer decades del sigle XX va patir canvis que a Catalunya ya havien succeit decades abans.

La mortalitat va baixar molt i per tant la població va augmentar però seguidament va disminuir també la natalitat i la demografia es va estabilitzar.

El descens de la mortalitat va venir donat gracies a la millora de l'alimentació, la sanitat i la higiene de calvegueram i la conducció d'aigua potable a les ciutats. Tot i això la epidemia de grip de 1919 va matar a l'1% de la població a Espanya. La mortalitat infantil també va disminuir enormement.

El descens de la natalitat es va donar per l'increment de la població urbana i les practiques de planificació familiar. Com a conserqüencia d'aquests canvis demografics la població espanyola va crèixer de manera considerable. A Catalunya va creixer de manera més marcada pero va ser degut a una onada migratoria.

Desde finals del segle XIX va haver-hi un gran  augment de l'emigració exterior, dirigida fonamentalment cap a l'Armèrica del Sud i cuba. Els emigrants eren majoritariament joves en edat productiva. Dins del territori espanyol va descendir la població activa agrícola, aquesta població va emigrar cap a zones industrialitzades.

Les ciutats també van patir un creixement molt marcat. Madrid i Barcelona van arribar al milió d'habitants i Bilbao, València, Sevilla, Màlaga i Saragossa van incrementar molt la seva població. Les ciutats costaneres van creixer en població mentre que les ciutats del interior (a excepció de Madrid, Saragoss i Valladolid), van decreixer.

Les activitats industrials i de mineria concretes també van fer que creixès la població als voltans de Barcelona, Biscaia i Astúries.

Tot i aquest augment de la població urbana, Espanya seguia molt enraderida respecta a altres potencies europees en el que respecta a densitat de població i població urbana.

## L'expansió Urbana: El cas de Barcelona
Barcelona va ser un cas particular amb una gran transformació urbana. Molta de la immigració va ser deguda a que anaven a treballar a les noves indústries i a les obres públiques. A més a més, al 1897 es va afegir a Barclona: Gràcia, Sant Gervasi, Sant Martí de Provençal, etc i al 1921 Sarrià.

Amb aquest gran augment de població els transports i les comunicacions es van convertir en un problema cabdal. Calia obrir grans vies de comunicació entre la zona de muntanya i la de mar. Per tal de solventar això es van augmentar i electrificar la xarxa de tramvies, reformar el port o construir el primer aeroport. També va tenir una gran importancia la construcció del metro i l'afegiment d'espais poc integrats com la muntanya de Montjuïc o el Tibidabo, convertits en grans zones d'esbarjo. 

## L'agricultura: Crisi i Diversitat d'una Societat Pagesa
A l'Espanya del 1900 predominava una política proteccionista que aplicava aranzels molt alts sobre els productes d'importació, això sumat a la baixa productivitat loal feia que la major part de la renda fos absorvida per necesitats bàsiques reduint la capacitat per comprar productes manufacturats i frenant el desenvolupament de la indústria.

 Els conreus més dinàmics eren els que s'acabaven expotant com per exemple la vinaya, l'olivera, els cítrics, la fruita i les hortalises.
 
 El principal problema agrari que patia Espanya era la desigualtat en la propietat de la terra. Al Sud de la peninsula els latifundis eren propietat de conservadors que arrendaven la terra a jornalers que quasi bé no tenien per viure mentre que a la part nord de la península, la pobra terra de la Meseta i els minifundis gallecs feien que la terra no donès quasí beneficis, produint una agricultura de subsistencia.
 
 Un altre problema que patia la agricultura Espanyola era una conseqüencia directa de la millora dels transports, a Espanya arribaven productes agrícoles i ramaders a preus molt més baixos al europeus. Això va provocar una profunda crisi agricultural europea. Per remeiar això els governs van posar aranzels que augmentaven els preus dels productes estranjers. A això se li va sumar la crisi de la filoxera, que va afectar a un terç dels ceps espanyols.
 
 Aquestes crisis es van solventar bàsicament gracies a la política proteccionsita, l'augment de la superfícies conreada i la intensificació i especialització dels conreus. 
 
 Gracies a l'ús de fertilitzants i l'augment de la mecanització i el conreu de regadiu, la agricultura espanyola es va intensificar. Per millorar les situació els governs van promouse l'extensió del regadiu a través de les Confederacions Hidrogràfiques (a la dictadura de Primo de Riviera) i el pla d'Obres Hidràuliques més tard.
 
 El problema dels pagesos sense terra va ser intentat de remeiar als anys 1907 i 1917, tot i que va ser totalment insuficient. Una autèntica reforma va haver d'esperar fina a la segona República, a l'any 1932.
 
 El transit cap al segle XX va ser també un periode de crisis agraria a Catalunya, especialment per la plaga de la fil·loxera. La mort d'aquests ceps també va desencadenar una gran conflictivitat social amb els contractes de rabassaires (tenient contracte fins que la vinya es morís) i amb tots els ceps que estaven morint amb la fil·loxera els propietaris exigien rescindir el contracte i renegociar les condicions. 
 
 Superada aquesta crisi però, la agricultura catalana va augmentar la productivitat i va reduir la població que s'hi dedicava. es va comenar a mecanitzar el camp i a introduir adobs. 
 
 ## L'expansió de la Indústria
 La transformació de l'economia al principis del segle XX es pot explicar amb el canvi de la manera d'utilitzar l'energia. Un dels problemes que havia tingut abans Espanya era la baixa qualitat del carbó autocton, però amb la popularització de l'ús de l'electricitat es va solventar el problema.
 
 Aquesta popularització va estar donada en dos fases, primer en l'àmbit de les ciutats i després en l'àmbit industrial. Les principals invencions que van protagonitzar aquest periode van ser la destilació del petroli i l'invent del transformador (que evitava perdúes quan es transportava l'electricitat).
 
 Aquests avenços tecnològics van permetre revolucionar tant els transports com la transmissió de la informació gracies al telegraf, el telèfon i la ràdio.
 
 ### El Creixement Industrial
 Va augmentar també el volum industrial a Espanya, a la vegada que l'estructura industrial es va transformar de manera importante.
 
 Als sectos tradicionals, el tèxtic català va continuar en expansió però perdent pes, mentre que les indústries alimentaries van decaure.
 
 La indústria química es va assentar gràcies a la fàbricacio de fertilitzants, medicaments, pintures i explosius.
 
 La indústria siderúrgica va creixer considerablement a Biscaia, Cantabria i Sagunt. A més va apareixer la indústria elèctrica, la primera compañía va ser _Compañía Sevillana de Electricidad_ pero la producció de electricitat es va concentrar sobretot a Barcelona (La Canadenca)
 
 També va creixer la indústria metal·lúrgica, tot i que la gente encara no disposava d'electrodomèstics, aquest va ser un sector en creixement.
 
 ### Un Creixement desequilibrat
 A principis del segle XX Espanya era un país amb una industria encara migrada i poc competitiva, a causa dels alts costos de protecció. Nomès hi havia inversió i capitalització a Catalunya, País Basc i es va sumar Madrid.
 
 Per solucionar això el govern va recorrer al proteccionisme i a l'intervencionisme. 
 
 L'activitat pesquera va influir al desenvolupament de Cantàbria i d'Asturies, mentre que València i Alacant es van especialitzar en la indústria del moble i de les joguines.
 
 L'aparició de noves empreses era una minoria ja que quasi bé totes eren de caire familiar i tradicional. La gran desigualtat de la distribució de la activitat industrial va provocar que Castella la Mancha estiguès un 45% per sota de la mitjana espanyola mentre que Catalunya estava gaire bé un 90% per sobre de la mitja.
 
 ### La industria a Catalunya
 
 
 ## La Política Econòmica: Proteccionisme i Intervencionisme
 Al primer terç del segle XX la política economica espanyola es va caracteritzar pel proteccionisme i per cercar l'equilibri presupostari. Tant liberals com conservadors van aplicar el nacionalisme econòmic.
 
### Els efectes de la crisi del 98
Els efectes econòmics de la crisi del 98 van ser menors als esperats tot i que la industria d'exportació de cereals i la industria textil catalana van patir perdues ja que el seu principal importador era Cuba. 

En aquest moment el govern va tenir per primer cop superàvit (no estaven endeutats, sinó que els hi sobrava diners). La repatriació del capital que hi havia a Cuba va facilitar la creació de bancs i empreses modernes. La majoria de bancs actuals van ser creats en aquest periode.

### La política intervencionista de l'Estat.
La política espanyola en aquell moment es va caracteritzar per la limitació de la lliure competència tant exterior com interior. La limitació d'aquesta competencia s'aconsguia fixant impostos a la importació i mitjançant acords entre les empreses per establir els preus o per repartir-se el mercat nacional. Aquestes pràctiques inflaven els preus i limitaven les possibilitats de l'economia.

L'intervencionisme estatal també es va dur a terme concedint ajudes a empreses espanyoles i promoven sectors industrials que requerien molta inversió i que necessitaven ser protegits.

### El foment de les infraestructures
L'esforç investor de l'Estat es va centrar sobretot en la millora de camins i carreteres però tambe en la electrificació de la xarxa ferroviària. Aquestes inversions tan fortes van fer pujar la despesa pública, però tambè va fer pujar la productivitat del sector privat.

## De quina manera la gran guerra va afectar l'economia?
La neutralitat espanyola durant la Primera Guerra Mundial va suposar que Espanya podia vendre productes als països beligerants com podien ser el cató, el calçat, els productes metal·lúrgics o els productes químics. Això va suposar una gran quantiat de beneficis per a les empreses espanyoles, pero la majoria d'aquests beneficis no van ser re-invertits a la industria espanyola. 

La pujada de preus, no acompanyada de la pujada de salaris, va implicar una caiguda en el nivell adquisitiu de la ciutadania i va significar que Espanya va entrar en crisi entre els anys 1919 fins al 1924. 

## La Política Econòmica Durant la Dictadura de Primo de Rivera
Tot i que les polítiques aranzelaries abans de la Dictadura ja eren altes, es van considerar insuficients i es van pujar encara més. A més a més, el lliure merca es va alterar amb la fixació dels preus dels productes bàsics i de primera necessitat. 

Per tal de millorar les infraestructures i les obres públiques es va posar en marxa la confederació de l'Ebnre, el circuit Nacional de Ferms Especials (9455 quilòmetres de carreteres i es van millorar les que ja hi havia), es van invertir 1300 milions de pessetes a renovar el material ferroviarai, es van electrificar linies de ferrocarril i es va portar l'electricitat a les arees rurals.

Per tal de sufragar la despesa pública la dictadura va emitir deute públic per atendre les gran despses del Pressupost extraordinari.

La crisi del 29 no va tenir massa efecte a Espanya gracies a les polítiques proteccionistes i de la importancia del merca interior en el model econòmic espanyol. Tot i això la recessió dels 30 va fer impossible la crescuda de la economia durant la segona república. Els sectors més afectats van ser els exportadors: productes agrícoles, els minerals i derivats. 

La devaulació de la pesseta va atenuar els efectes de la crisi perquè va incrementar la competitivitat dels productes espanyols.

## Les Transformacions Socials
 El procés de modernització social i econòmica d'Espanya va tenir efectes més limitats que en altres països europeus i va presentar moltes més desigualtats. Això venia donat per la mancança de grans reformes necessàries, per l'aliança de grans propietaris amb industrials catalans i bascs que apujaven els preus i altres factors.
 
 Tot i aquesta desigualtat, a les zones rurals de menys de 5000 habitants només hi vivia un 34% de la població al 1930.
 
 Els canvis més profunds els van patir les societats urbanes on van apareixer forces i grups socials que innovaven en l'economica, la ciència i la cultura i lluitaven per modernitzar els hàbits socials.
 
 La industrialització va reforçar el paper de la burgesia i també va provocar un creixement de les classes mitjanes. L'augment del republicanisme reflexava l'aspiració de les classes mitjanes urbanes de dur a terme el procés de modernització d'Espanya que la Restauració havia frenta i impedit. El grup més nombrós eren els obrers industrials i això va afavorir l'augment d'afiliació a organitzacions obreres com la UGT i la CNT.
 
 Una de les transformacions més rellevants va ser la millora en la qualifiació educativa, es va pasar d'un index d'analfabetització del 55% al 30% en 30 anys. La millora d'aquesta educació primaria també va provocar la millora de l'educació professional i superior. 
 
 L'increment de l'alfabetització va provoca més demanda d'oci cultural i l'expansió de la premsa escrita. També va sorgir un nou associacionisme de caràcter cívic. Va estendre's la pràctica l'excursionisme i de l'esport entre capes més amplies de la població. 