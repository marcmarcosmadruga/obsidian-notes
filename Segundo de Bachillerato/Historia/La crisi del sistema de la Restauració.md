# La crisi del sistema de la Restauració
Al 1899 la reina regent confia el nou govern a un líder conservador: **Francisco Silvela**, que va impulsar una política reformista. Va proposar un projecte de descentralització administrativa. Va impulsar l'augment dels tributs sobre els productes de primera necessitat i va imposar impostos nous (deutes guerra de Cuba). A Catalunya hi va haver una forta protesta (Tancament de Caixes).

El govern volia embargar a aquells qui no volien pagar però l'alcalde de Barcelona com no hi estava d'acord va dimitr. Va donar lloc una vaga general de comerciants. El govern va respondre empresonants als morosos, suspenent les garanties consitucionals a Barcelona i declarant l'estat de guerra. Duran i Bas i Polavieja van dimit dels seus càrrecs.

El tancament de caixes va significar el trencament entre els partits dinàstics i les elits catalanes. El catalanisme es va consolidar com a alternativa, es va acabar amb l'esperit de la regeneració i al 1901 es va reprendre el torn dinàstic.

### Les reformes de Maura i Canalejas
Al 1902 puja al tron Alfons XIII, coincidint amb una renovació del lideratge dels partits dinàstics: al Partido Conservador, Antoni Maura; i al Partido Liberal, José Canalejas. Al 1904 Maura es va convertir en cap de govern. Va impulsar una serie de mesures:
- Impulsar la revolució des de dalt.
- Va intentar dotar el sistema d'una nova base social (les masses neutres). Estat fort que desbanqués als cacis i que impedís que les classes populars tinguessin massa protagonisme.
- Al 1907 amb la nova llei electoral es fa més difícil el frau.
- El **Projecte de Reforma de l'Administració Local**
- Va aprovar alunes lleis socials com la llei de decans dominical i l'Institut Nacional de Previsión.
- Va adoptar mesures econòmiques per reactivar la indústrial.
- Llei de Colonització Interior per estimular l'agricultura.
- Manteniment de l'Ordre Públic d'una manera molt intransigent: Setmana Tràgica (1909).

Al 1910 Canalejas accedeix al govern:
- Intenta apropar-se als sectors populars
- Ley del Candado, separació església-estat
- Impost progressiu sobre les rendes, llei de lleves (1912), lleis per millorar les condicions laborals.
- Negociació d'una llei de Mancomunitats.
- Assasinat al 1912.

## Les forces d'oposició
### Republicanisme i lerrouxisme
Van ser la principal força d'oposició, però estaven molt fragmentats entre ells.
Al 1903 es crea Unió Republicana, ecapçalada per Nicolas Salmerón.
A Barcelona el republicanisme liderat per Alejandro Lerroux va ser molt influent entre les classes populars.
La unió republicana es va apropar al catalanisme (arran de les eleccions de 1907), es va integrar en la coalició Solidaritat Catalana, rebutjat per un sector.
Lerroux separà l'any 1908 i funda el **partit radical**.

### Carlisme i tradicionalisme
La mort de Carles de Borbó converteix el seu fill Jaume de Borbó no pretendent a la corona.
Un dels seus dirigents Juan Vazquez de Mella al 1919 va fundar el Partit Catòlic Tradicionalista.
L'any 1906 es funda el Requetè: organització de caràcter paramilitar. Força de xoc contra els republicans i obrers.
Amb la República (1931) es reunifiquen en un sol partit: **Comunió Tradicionalista**.

### Les forces obreres: socialisme
Augmenten el nombre de militants i les seves actuacions.
El PSOE defensava l'acció política, partidari d'aprofitar les oportunitats que donava el sistema parlamentari.
Va participar en les eleccions. El 1910, Pablo Iglesias (acta de diputa de la coalició republicanosocialista).
El sindicat UGT va créixer encara més:
- Acció política per aconseguir reformes socials i laborals.
- Participació dels representants en organismes estatals.
- Líder important: Francisco Largo Caballero. 1918. Secretari general de la UGT.

Al 1921 es funda el partido comunista de España, amb militants molt actius a Biscaia i Astúries.

### L'anarcosindicalisme
Es va crear al 1907 Solidaritat Obrera (favorables a la lluita revolucionaria). A l'any 1910 van impulsar la fundació de la confederació nacional del treball (CNT). Es definia com a revolucionaria. Amb quatre pressupòsits bàsics:
- Indpendència del proletariat respecte de la burgesia i les seves institucions.
- Apoliticisme del moviment obrer = absentisme electoral.
- Necessitat de la unitat sindical
- Enderrocar el capitalisme

Això ho pretenien aconseguir amb vagues i boicots. **Vaga General Revolucionaria**. Després de la I Guerra Mundial va augmentar molt els seus afiliats. Salvador Seguí, Àngel Pestaña i Joan Peiró.

## Catalanisme i Republicanisme
### Consolidació de la Lliga Regionalista
La Lliga va ser el partit hegemònic a Catalunya. El seu ideari va ser Enric Prat de la Riba a la Nacionalitat Catalana (1906). Frances Cambó era el portaveu dels interessos catalans a Madrid. Reclamaven el dret de Catalunya a l'aautonomia política. Disposats a intervenir a nivell de la política espanyola per tal de modernitzar l'Estat.

Va tenir dificultats: lerrouxisme, discrepàncies ideologiques internes.
El sector més critic fundarà una nova formació de caire republicà.

### Solidaritat Catalana
El rebuig a la llei de jurisdiccions va formar solidaritat catalana, que reafirmava la personalitat i els drets a Catalunya. Aplegava totes les tendencies catalanes menys els partits dinàstics i lerrouxistes.

Al 1907 presenten el programa de tívoli, derogació de la llei de jurisdiccions i necessitat d'autogovern. Va ser un gran triomf.

Com tenien representació al congrés dels diputats, Maura va haver de negociar certes lleis amb ells. Les discreptancies entre els diferents partits van ser l'element més important pel trencament. Algunes d'aquestes discrepancies van ser:
- Projecte de reforma de l'administració local. Uns el rebutjaven per poc democràtic els altres volien negociar-lo amb Madrid.
- Projecte de l'ajuntament de Barcelon (escoles en català, neutralitat religiosa). La Lliga defensà l'església.
- Fets de la setmana tràgica (1909), uns van veure amb bons ulls la repressió i d'altres no.

### El catalanisme d'esquerres
Els escindits de la Lliga funden el Centre Nacionalista Republicà (1906). Nacionalista, democràtica i republicà.

Notable activisme al diari el Poble Català i fundació de molts ateneus. Sindicat *centre autonomista de dependents del comerç i de la indústria*. L'apropament entre les forces d'esquerra va fer que el 1910 es fundés la *Unió Nacionalista Republicana*. Va obrenir bons resultats a les eleccions. Aliança amb Partit Radical de Lerroux -> pacte de Sant Gervasi (no va agradar als votants). Crisi de la Unió Federal Nacionalista Repúblicana, despaareix l'any 1916.

### Impacte de la Setmana Tràgica de Barcelona
A partir del 1900 Espanya consolida la penetració al nord d'Àfrica. La conferència d'Algesires (1906) i el Trectat Hispanofrancès (1912) suposen el protectorat francoespanyol al Marroc.

A Espanya li va correspondre el territori del Rif i tenien interès per possibles beneficis econòmics, restaurar el prestigi i intentar aconseguir que Espanya fos novament una potencia. Al 1909 els espanyols van patir una gran derrota a la Batalla del Lobo. Es van cridar als reservistes, grans protestes.

Les mobilitzacions comencen al port de Barcelona el 18 de juliol. El 24 de Juliol es va constuir un comitè que cridà a la vaga general. La revolta popular va durar una setmana (fort component antimilitarista i rebuig a l'hegemonia social i cultural de l'Església). Es va declarar l'estat de guerra. La repressió va ser molt dura (216 consells de guerra, 17 penes de mort)

L'actuació del govern va suposar una Onada de Protestes que va fer caure el govern de Maura. 


## La Mancomunitat de Catalunya
Va ser un procés de formació llarg i difícil. Es va crear l'Institut d'Estudis Catalans i la Biblioteca de Catalunya. El 1911 la Diputació de Barcelona va promoure la iniciativa de mancomunar les 4 diputacions catalanes i va presentar una propsta al govern de Madrid.

El 1913 el nou govern conservador de Dato va publicar un Decret que autoritzava a les diputacions provincials a mancomunar-se per a fins exclusivament administratius. 

La Mancomunitat es va constituir el 6 d'Abril de 1914. Va tenir com a president a Enric Prat de la Riba i una assemblea amb 8 membres. Només va ser una delegació de les funcions que tenien les diputacions, un ens administratiu. La Lliga Regionalista va tenir una presencia majoritària a la Mancomunitat, van per política de govern orientada a estimular el desenvolupament cultural i la modernització econòmica i social de Catalunya.

Va tenir tres presidents: Enric Prat de la Riba (1914-1917), Josep Puig i Cadafalch (1917-1924) i Alfons Sala (1924-1925). 

Va seguir la política iniciada per la Lliga des de la Diputació: la creació d'una infraestructura de serveis públics i administratius i el foment de la llengua i la cultura catalanes. Va millorar la xarxa viària, els sistema postal i telefònic. A més de començar el pla d'acció agrària (modernització de les formes de producció, augment de la productivitat agricultural i ramadera).

Es va iniciar un projecte en la reafirmació de la llengua i cultura catalanes: la creació de biblioteques populars, escoles experimentals i estudis normals, la tasca d'unificació ortografica, la divulgació i protecció del patrimoni cultural i es va impulsar una rrenovació pedagògica.

## La Crisi del 1917
Quan va explotar la gran guerra el govern va declara la neutralitat d'Espanya. Va exportar productes industrials i agraris i es va beneficiara exportant productes als països bel·ligerants. Va provocar una puja de preus interiors, reduint el nivell adquisitiu. Això va provocar un moviment vaguístic al 1917.

Les dificultats del sistema polític, el descontentament militar i la conflicitivitat social va provocar una protesta generalitzada.

**La protesta militar:** nombre excessiu d'oficials en relació als soldats, africanistes progressaven més rapid que els peninsulars, salaris baixos. Es formen les juntes de defensa i al 1918 es llença un manifest que crida a la renovació política.

**La crisi política:** El 1916 el govern estava en mans de Romanones que va clausurar les corts per acusacions de corrupció política. El 1917 es nomena Dato com a cap de govern, es neguen a reobrir la cambra i es declara l'Estat d'excepció, s'endureix la censura de premsa.

Es celebra a Barcelona una assemblea de parlamentaris catalans que exigien la formació d'un govern provisional que convoqués corts constituents per reformar el sistema polític i descentralitzar l'Estat. Nomès assisteixen el 10%, el govern prohibeix la convocatoria i la guardia civil la disol.

El moviment parlamentari desapareix perquè ni les forces monàrquiques ni les juntes de defensa van donar suport, discrepàncies entre catalanistes conservadors i d'esquerra i la burgesia es va inhibir.

**Conflictivitat social:** Al marça de 1917 la CNT i la UGT proposen un manifest que demana la intervenció del govern per aturar la pujada de preus. Al més d'agost el conflicte ferroviari a València provoca la crida a la Vaga General d'UGT. La protesta s'estén per tota Espanya. Es reclamava la fi de la monrquia, la formació de govern provisional per convocar Corts Constituents i el pas a un sistema republicà. 

## La Descomposició del Sistema
Entre el 1918 i el 1923 hi havia forta conflictivitat social, tensions polítiques i incapacitat per reformar el sistema polític.

Per solucionar-ho van recorrer a la formació d'un Govern de Concentració. Estava conformat pels Dinàstics i els Catalanistes de la Lliga Regionalista, eren massa diferents.

L'any 1918 es torna al sistema de torn, però no hi havia gaire estabilitat. 

L'any 1921 es crea un nou govern de concentració (Gobierno Nacional). Integrava els líders de les diferents fraccions dinàstiques, un de la Lliga (Cambó) i un del Partido Reformista.

Entre el 1918 i el 1923 van haver-hi deu canvis de govern. Alguns van recorrer a la suspensió de les garanties constitucionals (clausura del parlament).

La Lliga i els dirigents de la Mancomunitat van encertar al 1918 una campanya per la autonomia. Es va presentar a Madrid un Projecte d'Estatut de Catalunya que proposava una formació d¡un govern català i un parlament elegit per sufragi universal.

El projecte va ser mal rebut a Madrid. El govern es va oposar i el monarva no hi va donar suport. 

Les forces d'esquerra van proposar una renovació dels seus princips basada en la necessitat de Catalanitzar el republicanisme. Van voler atraure obrers per tal de que fossin base social del catalanisme d'esquerra. Al 1917 Frances Layret i Lluís Companys organitzen el Partit Republicà Català. Al 1922 un grup desconent amb la política de la Lliga abandona el partit i crea Acció Catalana. Al 1922 Francesc Macià funda l'organització Estat Català amb postulats independentistes. Al 1923 es crea una organització socialista estrictament Catalana, la Unió socialista de Catalunya (Manuel Serra i Rafael Campanals). 

Els anys posteirors a la Primera Guerra Mundial van estar marcats per una gran conflictivitat a Europa. A Espanya les condicions econòmiques van canviar bruscament. La producció va disminuir, l'atur va augmentar i els preus també van augmentar. Gracies a això va augmentar el sindicalisme.

A Andalúsia la situació de misèria dels pagesos va donar lloca l Trienni Bolxevic.

Al 1921 la vaga a la Canadenca va paralitzar un 70% de la industria durant un mes i mig, va acabar amb un acord amb la patronar que noes va complir del tot. Es va reempendre la vaga i la patronal va respondre amb el tancamnet d'empreses i la repressió de sindicats.

La federació patronal va crear el Sindicat Lliure per restar força als altres i enfrontar-s'hi (sorgeix el pistolerisme per assasinar els principals dirigents obrers).

El governador de Barcelona va protegir els pistolers però va reprimir els sindicalistes. Va aplicar la llei de fugues, la policia podia disparar contra qualsevol detingut que intentés fugir. 

El pistolerisme va succeir entre el 1916 i el 1923. Més de 800 atemptats i 226 morts.

### El problema del Marroc. Annual.
Al començament de la decada dels 20 a prop de Melilla, les tribus rifenyes hostilitzaven l'exèrcit espanyol permanentment. Al 1921 s'envia al general Silvestre que era partidari d'atacar les tribus. L'exercit va ser derrotat a la batalla d'Annual. 13000 baixes.

El 1923, un moment de governs inestables, de tensió social greu. S'anuncia la Discusió al Parlament del cas d'Annual. Els parlamentaris obren l'Expedient Picasso, que revelava la relació del rei amb el problema del Marroc.

## Dictadura de Primo de Rivera
El 13 de Setembre de 1923 el capità general de Catalunya, Miquel Primo de Rivera declara l'Estat de Guerra. Ho justifica com una solució per posar fi a la crisi política i a la conflictivitat. Causes per justificar-ho: Inestabilitat i bloqueig del sistema parlamentari, desprestigi pel frau electoral, por a una revolució social, perill de trencament d'Espanya.

Va anunciar per mitjà d'un manifest que tenia la voluntat d'acabar amb el caciquisme, posar fi a la corrupció política, a la indisciplina social i a les amenaces a la unitat nacional.

La dictadura de primo de Riviera va tenir dues fases: el directori militar (1923-1925) i el directori civil (1925- 1930).

### Directori militar
Mesures:
- Caràcter dictatorial
- Suspensió del règim constitucional.
- Dissolució de les cambres legislatives.
- Cessament autoritat civils.
- Prohibició de partits polítics i sindicats.
- Militarització de l'ordre públic.
- Repressió de l'obrerimse més radical.
- Es va elaborar un estatu municipal i un estatut provincial, per eliminar el caciquisme. Es dissolen ajuntaments i es substitueixen per juntes (integrades pels contribuents més importants).

Al 1925 s'organitza, juntament amb França, el Desembarcament D'Alhucemas. Va ser un exit i al 1927 l'exèrcit espanyol dóna per acabada l'ocupació. 

### Directori civil
A partir del 1926 s'institucionalitza el règim per donar-li continuïtat i permanència. Va ser un model influenciat pel feixisme italià.

L'any 1927 es crea una assemblea nacional consultiva i es va crear un partit únic: Unión Patriòtica. Els afiliats eren conservadors, funcionaris de l'administració i cacis rurals. No tenia un programa definit, tenia com a missió proporcionar suport social a la dictadura i seguir les directrius del poder.

<hr>

La dictadura es va beneficiar de la Bona Conjuntura Econòmica. Feliços anys 20.
El règim va iniciar un programa de foment de l'economia en el terreny industrial i els de les infraestructures.

Les principals idees van ser la nacionalització d'importants sectors de l'economia, la intervenció estatal i el foment de les obres públiques. Es va aprovar un Decret de protecció de la indústria nacional Es van concedir monopolis a Telefònica i Campsa. En el terreny agrari es promou el regadiu i es creen les confederacions hidrogràfiques. L'any 1929 es porta a terme l'Exposició Internacional de Barcelona amb fins propagandístics.

En el terrreny social es va posar en marxa un model de regulació del treball. Pretenia eliminar els conflictes laborals per mitjà de la intervenció de l'Estat. Es creà l'Organització Corporativa Nacional. Agrupava els patrons i els obrers en grans corporacions (sindicalisme vertical). Es van reglamentar els salaris i les condicions de treballs. Els anarcosindicalistes i els comunistes van ser perseguits.

### Actuació anticatalana de la dictadura
Mentre Primo de Rivera va ser capità general a Catalunya no va manifestar rebuig al catalanisme. 

Tot i això, una vegada va donar el cop d'Estat.
- Va publicar un decret per a la repressió del separatisme. S'inicia un procés de desmantellament de les institucions públiques i privades catalanes.
- Es clausuren institucions catalanistes.
- Es prohibeix la llengua catalana.
- Es prohibeix l'ús públic de la senyera, la celebració de l'11 de Setembre i els Jocs Florals.
- Censura a la premsa diària i a la publicació de llibres.
- Es van produir depuracions del magisteri i de les institucions educatives i culturals vinculades a la Mancomunitat.
- Es va mantenir una dura pugna amb el Col·legi d'Advocats i amb certs sectors eclesiàstics.
- La persecució va arribar al FC Barcelona.

### L'oposició a la dictadura
Es van oposar a la dictadura:
- Alguns líders dels partits dinàstics.
- Republicans.
- Catalanistes.
- Comunistes.
- Anarquistes.
- Alguns sectors de l'exèrcit.
- Gairebé tots els intel·lectuals.

Es va exercir una gran censura sobre el món universitari. Es va originar un gran sindicat, la Federació Universitària Espanyola (FUE), de caràcter republicà.

Al 1924, Unamuno, Ortega y Gasset, Blasco Ibáñez i Menéndez Pidal signen un manifest de protesta per la repressió de la dictadura sobre la llengua i la cultura catalana. Unamuno va ser desterrat a Fuerteventura, Blasco Ibáñez va marxar a l'estranger.

El conflicte més persistenct va ser amb el republicanisme i el catalanisme. Alianza republicana va unir les diverses faccions del republicanisme i va desenvolupar una campanya propagandística a l'exterior. Va haver un intent d'invasió armada desde Prats de Molló protagonitzat per Estat Català, l'any 1926 i dirigit per Francesc Macià que estava exiliat a França.

Els més radicals de la CNT van crear al 1927 la FAI (Federació Anarquista Ibèrica). El PSOE va canvir la seva posició cap al 1929 quan va rebutjar obertament els intents constinuistes del règim i es pronuncià a favor de la república.

### La caiguda del dictador
Alfons XIII va reitrar la confiança al dictador perquè creia que la monarquia també estava en perill si el dictador continuava. El dictador va dimit el 30 de Gener de 1930. El general Berenguer va ser l'encarregat de substituir-lo. 

Berenguer tenia coma a missió organitzar un procés electoral progressiu que permeté el retorn a la normalitat constitucional (dictablanda). L'oposició va començar a organitzar-se: els republicans, els catalanistes d'esquerra i el PSOE.

La oposició va crear el Pact De Sant Sebastià (agost de 1930), que va ser un programa per presentar-se a les eleccions i construir un comitè revolucionari (futur  govern de la república). Compromís de reconeixement del dret a l'autonomia de Catalunya, el País Basc i Galicia.

