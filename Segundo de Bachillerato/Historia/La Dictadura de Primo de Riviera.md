# La Dictadura de Primo de Riviera

A partir del 1926 s'inicia la Institucionalització del Règim (per donar-li continuitat i permanència). A l'any 1927 es crea una assemblea nacional consultica (de caràcter corpooratiu, els seus membres eren designats entre els ciutadans que pertanyien a les grans institucions públiques i corporatives).

Per promoure l'adhesió al nou sistema es creà un partit únic: **Unió Patriòtica**. Els afiliats eren els conservaodrs, els funcionaris de l'administració i els cacics rurals. No tenia un programa definit, tenia la missió de proporcionar suport social a la dictadura i de seguir les directrius del poder.

A l'any 1927 es crea una assemblea nacional consultiva (de caràcter corporatiu, els seus membres eren designats entre els ciutadans que pertanyien a les grans institucions públiques i corporatives). 

## La política econòmica i social

La dictadura es va beneficiar de la bona conjuntura econòmica (els feliços anys 20). L'inici del consumisme.

Primo de Riviera implanta el nacionalisme econòmic (comprar tots els bens que es puguin de Espanya en lloc d'importar-los). Va nacionalitzar importants sectors de l'economia (estatalitzar), es va fomentar les obres públiques. 

En el terreny agrari es promou el regaiu i es creen les confederacions hidrogràfiques. 

L'any 1929, es porta a terme l'Esposició Internacional de Barcelona amb fins propagandístics.

## Terreny social

Es va posar en marxa en un model de regulació del treball. Pretendia eleminar els conflictes laborals per mitjà de la intervenció de l'Estat.

Es creà l'Organització Corporativa Nacional. Agrupava els patrons i els obrers en grans corporacions (sindicalisme vertical): reglamentació salaris, reglamentació condicions treball i mediació i arbitratge. 

Els anarcosindicalistes i els comunistes van ser perseguits, mentre que el PSOE i la UGT no van oposase en un primer moment a la dictadura.

### L'actuació anticatalana de la dictadura

Tot i que Primo de Riviera va ser capità general a Catalunya, al setembre es va publicar un decret per a la repressió del separatisme. S'inicia un procés de desmantellament de les institucions públiques i privades catalanes. Es van clausurar institucions catalanistes. Es va prohibir la llengua catalana. Es va prohibir l'ús public de la senyera, la celebració de l'11 de setembre i els Jocs Florals. Censura a la premsa diària i la publicació de llibres. Es produirien depuracions del magisteri i de les institucions educatives i culturals vinculades a la Mancomunitat. Es va mantenir una dura pugna amb el Col·legi d'advocats i amb certs sectors eclesiàstics. La persecució del FC Barcelona.

### Oposició a la dictadura

Alguns dels opositors a la dictadura de Primo de Riviera van ser alguns líders dels partits dinàstics, els republicans, els catalanistes, els comunistes, els anarquistes, alguns sectors de l'exèrcit i gairebé tots els intel·lectuals. Alguns dels dirigents dels partits dinàstics van participar en conspiracions militars.

Sanjuanada --> Intent de cop d'estat.

La dictadura va exercir un fort control sobre els intel·lectuals i el món universitari per mitjà de la censura i de la limiticació de la llibertar universitari. Tancament algunes universitats. Es va originar un gran sindicat, la federació universitària espanyola (FUE), de caràcter republicà.

El conflicte més persistent va ser amb el republicanisme i el catalanisme.

Francesc Macià va fer un intent d'invasió armada de Catalunya de de Prats de Molló protagonitzat per Estat Català, l'any 1926. 

La forta persecució de la CNT va agreujar el seu enfrentament intern. El sector més radical i violent va crear la Federació Anarquista Ibèrica (FAI). El PSOE va canviar la seva posició quan va rebutjar obertament els intents continuistes del regim i va apoyar la república. 


### La caiguda del dictador

Alfons XIII va retirar la confiança al dictador, perquè creia que la monarquia també estava en perill si el dictador continuava.

Al 1930 Primo de Riviera dimiteix.

El General Berenguer va ser l'encarregat de substituir a Primo de Riviera. El periode entre la dimisió de Primo de Riviera i la restauració de les garanties constitucionals es va anomenar **dictablanda**.


