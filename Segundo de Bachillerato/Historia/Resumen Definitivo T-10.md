# Resumen Definitivo T-10
## La modernització de l'estructura demogràfica
A principis del segle 20 la mortalitat va baixar, a la vegada que també disminuïa la natalitat, fet que va suposar que no es va desestabilitzar la demografia.  
  
La millora de l'alimentació, la sanitat, la higiene de clavegueram i la conducció d'aigua potable a les ciutats va suposar un descens marcat de la mortalitat. Per altra banda, el descens de la natalitat es va donar per l'increment de població urbana i les pràctiques de planificació familiar.

Des de finals del segle XIX va augmentar l'emigració exterior, especialment cap a l'Amèrica del Sud i Cuba. Els emigrants majoritàriament eren joves en edat productiva. Dins del territori espanyol la població activa agrícola va emigrar cap a zones industrialitzades.  
  
Les ciutats van patir molt creixement i les ciutats de l'interior (a excepció de Madrid, Saragossa i Valladolid) van decréixer. Tot i aquest augment de població urbana, Espanya seguia molt enrederida respecte a altres potències europees en el que respecta a densitat de població i població urbana.

### L'expansió Urbana: el cas de Barcelona
El cas de Barcelona va ser un cas particular. La majoria de la immigració va ser provocada per les noves indústries i per les obres públiques. A partir del 1887 se li van afegir a Barcelona diversos municipis com Gràcia, Sant Gervasi, Sant Martí de Provençal i Sarrià.  
  
Per millorar les comunicacions es va augmentar i electrificar la xarxa de tramvies, reformar el port i construir el primer aeroport. També es va construir el metro i es va afegir Montjuïc o el Tibidabo com a zones d'esbarjo.

## L'agricultura
La política proteccionista que aplicava aranzels molt alts sobre els productes d'importació i la baixa productivitat local del camp espanyola feia que la major part de la renda fos absorbida per les necessitats bàsiques, reduint la capacitat per comprar productes manufacturats i frenant el desenvolupament de la indústria.  
  
Els conreus més dinàmics eren els que s'acabaven exportant com la vinya, l'olivera, els cítrics i les hortalisses.  
  
El principal problema era la desigualtat en la propietat de la terra. Al Sud els latifundis eren propietat de conservadors que arrendaven la terra mentre que al Nord de la península, la pobra terra de la Meseta i els minifundis gallecs feien que la terra no donés quasi beneficis, produint una agricultura de subsistència i no pas de mercat.  
  
Un altre dificultat que patia Espanya era la importació estrangera a preus molt més baixos. Per remeiar això es van posar aranzels que augmentaven els preus. A això se li va sumar la crisi de la fil·loxera, que va afectar un terç dels ceps espanyols.  
  
Gràcies a l'ús de fertilitzants i l'augment de la mecanització i del conreu de regadiu, l'agricultura espanyola es va intensificar. Es va promoure l'extensió del regadiu a través de les Confederacions Hidrogràfiques i el pla d'Obres Hidràuliques.

El problema dels pagesos sense terra es va intentar remeiar, però va haver d'esperar fins a la segona República per solucionar-se.  
  
A Catalunya va afectar especialment la fil·loxera. La morta dels ceps va a més desencadenar una gran conflictivitat social amb els contractes de rabassa morta, això feia que els propietaris volien rescindir el contracte i renegociar les condicions. Superada aquesta crisi, però, l'agricultura catalana va augmentar la productivitat i va reduir la població que s'hi dedicava. Es va començar a mecanitzar el camp i a introduir adobs.

## L'expansió de la Indústria
La transformació de l'economia a Espanya es pot explicar amb el canvi d'utilitzar l'energia. El carbó era de molt baixa qualitat, però amb la popularització de l'electricitat es va resoldre.  
  
Aquesta popularització es va donar en dues fases, primer en l'àmbit de les ciutats i després en l'àmbit industrial. Les invencions que van suportar aquest període van ser la destil·lació del petroli i l'invent del transformador (per reduir pèrdues amb el transport de l'electricitat).  
  
Aquestes invencions també van permetre revolucionar la transmissió d'informació gràcies al telègraf, el telèfon i la ràdio.

Als sectors més tradicionals el tèxtil català va continuar en expansió, però perdent pes, mentre que les alimentaries van decaure. La indústria química es va assentar i la indústria siderúrgica va créixer considerablement a Biscaia, Cantàbria i Sagunt. A més va aparèixer la indústria elèctrica, la primera companyia va ser _Compañía Sevillana de Electricidad_ però la producció d'electricitat es va concentrar sobretot a Barcelona. També va créixer la indústria metal·lúrgica però amb menys força que les anteriors.  
  
Espanya era un país amb una indústria poc competitiva a causa dels alts costos de protecció. Només hi havia inversió a Catalunya, País Basc (i des de feia poc) Madrid. Per solucionar això el govern va recórrer al proteccionisme i a l'intervencionisme.  
  
L'activitat pesquera va influir al desenvolupament de Cantàbria i Astúries, mentre que València i Alacant s'especialitzaren en la indústria del moble i de les joguines.  
  
L'aparició de noves empreses era minoritària, ja que quasi totes eren de caràcter tradicional. Aquesta gran desigualtat va provocar que Castella la Manxa estigués un 45% per sota de la mitjana espanyola mentre que Catalunya estava gairebé un 90% per sobre.

### Falta el tema de la industria a Catalunya pero me da pereza
Al primer terç del segle XX a Catalunya, el creixement industrial es diferencia del rest d'Espanya fins al punt que es diu que va haver-hi una veritable Segona Revolució Industrial. Tots els sectors van augmentar la producció i la productivitat. El nombre de treballadors es va triplicar i el nivell d'industrialització era cinc vegades superior al de la resta de l'Estat espanyol.  
  
Va baixar el pes del tèxtil, però van créixer considerablement el metall i la química, que ja eren el 27% de la riquesa industrial. La producció metal·lúrgica es va concentrar especialment a la província de Barcelona, amb un 20% de la producció **espanyola**.  
  
Un dels pilars del creixement i del canvi de període va ser el sector elèctric. L'ús de l'electricitat es va generalitzar per tot Catalunya. L'expansió de l'electrificació la van protagonitzar capital i enginyers estrangers: als catalans els faltava el capital i l'experiència necessaris. El 1911 es va fundar la Canadenca, la companyia elèctrica més important de Catalunya.

## La Política Econòmica: Proteccionisme i Intervencionisme
Tant liberals com conservadors van aplicar el nacionalisme econòmic per cercar l'equilibri pressupostari i potenciar l'economia espanyola evitant tant la competència exterior com interior.

### Els efectes de la crisi del 98
Els efectes econòmics de la crisi colonial van ser menors als esperats tot i que la indústria cereal i tèxtil catalana van patir perdúes, ja que el seu principal importador era Cuba.  
  
En aquest moment el govern va tenir per primer cop superàvit (no estaven endeutats, sinó que els hi sobrava diners). La repatriació del capital que hi havia a Cuba va facilitar la creació de bancs i empreses modernes.

### La política intervencionista de l'Estat
La política econòmica es va marcar per la limitació de la lliure competència tant exterior com interior. La limitació d'aquesta competència la van aconseguir fixant impostos a la importació i fent acords entre empreses per establir els preus o repartir-se el mercat. Aquestes pràctiques inflaven els preus i limitaven el poder adquisitiu de les famílies.

També es concedia ajudes a empreses espanyoles i promovia sectors industrials que requerien molta inversió i que necessitaven ser protegits.

### El foment de les infraestructures
Les inversions de l'Estat es van centrar sobretot en la millora de camins i carreteres i en l'electrificació de la xarxa ferroviària. Aquestes inversions van fer pujar la productivitat del sector privat.

## De quina manera la gran guerra va afectar l'economia?
La neutralitat d'Espanya a la Primera Guerra Mundial va suposar una gran venda de productes com podien ser el cotó, el calçat, els productes metal·lúrgics o els productes químics als països bel·ligerants. La majoria d'aquests beneficis no van ser reinvertits a la indústria espanyola.

La pujada dels preus no acompanyada d'una pujada de salaris va significar que Espanya va entrar en crisi entre l'any 1919 i el 1924.

## La política econòmica durant la Dictadura de Primo de Rivera
La Dictadura de Primo de Rivera va pujar encara més les polítiques aranzelàries. A més el lliure mercat es va alterar amb la fixació dels preus dels productes bàsics i de primera necessitat.  
  
Es va invertir molts diners en obres públiques, tant que es va haver d'emetre deute públic per atendre les grans despeses dels pressupostos.  
  
La crisi del 29 (crac de la borsa de Nova York) no va tenir massa efecte a Espanya gràcies a les polítiques proteccionistes i de la importància del mercat interior. Tot i això, la recessió dels trenta va fer impossible la crescuda de l'economia durant la Segona República. 

La devaluació de la pesseta va atenuar els efectes de la crisi perquè va incrementar la competitivitat dels productes espanyols.

## Les Transformacions Socials
El procés de modernització va tenir efectes més limitats que en altres països europeus i va presentar més desigualtat. Calien reformes i els grans propietaris s'aliaven amb industrials catalans i bascs que apujaven els preus.  
  
Tot i això, només un 34% de la població vivia en nuclis urbans de menys de 5000 habitants.  
  
Els canvis més profunds els van patir les societats urbanes on van aparèixer forces i grups socials que innovaven en l'economia, la ciència i la cultura. La industrialització va reforçar la burgesia i va provocar el creixement de les classes mitjanes. El grup més nombrós eren els obrers industrials i això va afavorir l'augment d'afiliació a organitzacions obreres com la UGT i la CNT.  
  
La qualificació educativa va millorar i l'índex d'analfabetisme va disminuir un 25% en trenta anys. La millora d'aquesta educació primària també va provocar la millora de l'educació professional i superior.  
  
Aquest increment va provocar més demanda d'oci cultural, també va sorgir un nou associament de caràcter cívic. Va estendre's la pràctica de l'excursionisme i de l'esport entre capes més amplies de la població.  