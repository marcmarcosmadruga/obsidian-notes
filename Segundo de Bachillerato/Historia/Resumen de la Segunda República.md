# Unitat 11: La Segona república
## La Proclamació de la República i el Període Constituent
El 12 d'Abril de 1931 es celebren eleccions municipals amb un augment notable de la participació. guanyen el partits units al Pacte de Sant Sebastià (republicans, socialistes i nacionalistes) a les grans ciutats i a la mejor part dels nuclis industrials.

El 14 d'Abril es proclama la república per tota Espanya (començant per Éiber (Guipúscoa)). Alfons XIII renuncia a la potestat reial i abandona el país cap a l'exili.

Els representants dels partits del pacte de 