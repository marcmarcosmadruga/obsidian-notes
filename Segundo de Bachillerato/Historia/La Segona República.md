# La Segona República

A Catalunya triomfen les candidatures republicanes i d'esquerres.
El govern provisional convoca eleccions a Corts el 28 de Juny. Abans d'aquestes eleccions però, s'havia de normalitzar el pais, per tal de fer això, es prenen mesrues d'urgencia:
- Concessió amnistiaa general als presos polítics.
- Proclamació de llibertats polítiques i sindicals.
- Mesures protecció pagesos expulsats de les terres per no pagar les rendes.

El gran vencedor a Catalunya va ser Esquerra Republicana de Catalunya. Lluís Companys proclama la república des del balcó de l'Ajuntamnet de Barcelona.

Aquest fet va suposar un conflicte amb el govern provisional. Per què? Perquè els acords del Pacta de Sant Sebastià fixaven que la descentralització de l'Estat i la manera com s'articularien les diferents nacionalitts series establertes per la Constitució. 

Com es va solucionar? Una comissió del govern provisional va viatjar a Barcelona i va concedir un règim d'Autonomia a canvi de sotmetre la decisió de les Corts Constituients.

## La Constitució del 31
Les eleccions van tenir una participació molt alta (70%). La victoria va ser per la coalició d'esquerres. Van formar noves corts constituents i el govern va quedar en mans de la coalició vencedora. Niceto alcalà Zamora va ser nomentat cap de govern.

Es va nomenar una comissió encarregada d'elaborar un projecte de Constitució, aprovada al desembre.

Aquesta constitució configura l'estat de manera integral pero amb possibilitat de governs autònoms. El poder legislatiu residia en les Corts (una sola cambra). El poder executiu, govern format per consells de ministres i cap del govern; President de la República. El poder jusicial, jutges independents.

Àmplia declaració de Drets i Llibertats, gran preocupació pels temes socials:
- Igualtat absoluta davant  la llei, educació, treball.
- No discriminació per raons: origen, sexe, riquesa.
- Vot als 23 anys. Sufragi Universal.
- Facultat del govern per expropiar béns d'utilitat social.
- Treball definit com una obligació social.
- Afirmava la laïcitat de l'Estat, cap religió social.



## SALTO DE CONTENIDO 

