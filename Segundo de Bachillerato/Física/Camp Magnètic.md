# Camp Magnètic

## Fonts de Camp Magnètic
$\overrightarrow{B}$ = Vector de camp magnètic
La unitat del sistema internacional es el Tesla (T). Un equivalent es el Gauss (1 Gauss = 10000 Teslas)
$B = \frac{\mu_0 · I}{2 \pi r}$ <-- Formula pel camp magnètic en un conductor  infinit.
$\mu_0 = 4\pi · 10^{-7} \ TmA^{-1}$
$\mu_r = \frac{\mu}{\mu_0}$

### Camp magnètic d'una espira
Una espira es un conductor rectilini tancat (normalment són circuilars pero poden ser circulars,  quadrades i rectangulars) per a on circula un corrent. Nomès calcularem el camp magnètic de l'espira al centre, perquè això ens dona un vector perpendicular a l'espira.
$B = \frac{\mu_0 · I}{2R}$

### Camp magnètic d'una bobina o solenoide
Una bobina es un conjunt d'espires (més o menys atapeides) per a on circula un corrent elèctric. Aquest camp magnètic és en funció de la Intensitat, el nombre d'espires i la longitud de la bobina o solenoide.
$B = \mu_0 · \frac{N · I}{l} = \mu_0 · I · n$
$n = \frac{N}{L}$ <-- Densitat d'espires

### Camp magnètic d'un electroimán
$B = \mu · I \frac{N}{L}$

<br>

## Força de Lorentz
Si una càrrega electrica se està movent genera un camp magnètic. Quan aquesta càrrega elèctrica entra a una regió del espai afectada per un camp magnètic experimenta una força, aquesta es la Força de Lorentz. 
Aquesta força es calcula amb un producte vectorial, que es una operació matemàtica entre vectors. 
$\vec{F} = q \ (\vec{v} \times \vec{B})$

Mòdul --> $F = q · v · \sin(\phi)$ <-- El més habitual es que la velocitat i el camp siguin perpendiculars, això fa que el sinus de phi sigui 1, simplifica molt el problema.

Direcció:
$\vec{F} \perp \vec{v}$
$\vec{F} \perp \vec{B}$

Sentit:
$\vec{F} > 0$ si de $\vec{v}$ a $\vec{B}$ girem en sentit antihorari
$\vec{F} < 0$ si de $\vec{v}$ a $\vec{B}$ girem en sentit horari

A no ser que la carga sea al revés, lul.

**Insertar ejemplo Lorentz #todo**


## Força de Lorentz: trajectoria helicoïdal o circular

**Insertar ejemplo helicoidal** #todo  

## Força electromagnètica

Un selector de velocitat es un dispositiu on aconseguim escollir la velocitat d'un feix de partícules carregades. Combinant el valor del camp elèctric i del camp magnètic. Pàg 90 del llibre. En un selector sempre hi ha un camp elèctric i un camp magnètic. En qualsevol selector de velocitat aconseguirem que la força elèctrica i la força magnètica tinguin el mateix modul i direcció però sentit contrari.

**Insertar problema 6** #todo 

## Força entre dos fils conductors rectilinis indefinits
$\vec{F} = I ·(\vec{L} \times \vec{B})$

**Insertar foto de 27/01/22** #todo 

$F_2 = \mu_0 · \frac{I_1 · I_2 · L}{2 · \pi · d}$

