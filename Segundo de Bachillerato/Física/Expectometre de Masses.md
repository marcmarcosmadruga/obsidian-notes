## Expectometre de Masses

L'expectometre de masses es un aparell que separa partícules amb diferent massa i mateixa càrrega. Consta d'un selector de velocitat que selecciona part´ciules amb una determinada velocitat i un camp magnètic que les desvia en funció de la seva massa. 
<br>
![[espectrometro.gif]]
<br>
#todo insertar ejercicio 7 de selectividad

## Ciclotró
Un ciclotró es un accelerador de particules que combina camps elèctrics i camp magnètics.