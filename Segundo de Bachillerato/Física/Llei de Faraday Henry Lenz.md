# Llei de Faraday Henry Lenz

*Quan s'undiex corrent a l'amperimetre?*
Quan hi ha moviment realtiu entre la bobina i l'imant.

Quan el flux de camp magnètic a una superfície tancada no es constant s'indueix un corrent al circuit tancat de la superfície. 

Quan s'indueix un corrent per una variació de flux, aquest corrent tendeix a restablir les condicions inicials. Per això apareix el signe negatiu.

Estem generant corrent elèctric sense pila ni generador.