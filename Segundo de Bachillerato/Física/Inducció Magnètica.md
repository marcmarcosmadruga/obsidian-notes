# Inducció Magnètica

Flux Magnètic --> $\phi$.  ($Tm^2 =$ Webber)

El flux magnètic depèn de la superficie, de l'angle entre les línies de camp i la superfície i de la intensitat del camp.

$\phi = B · S · \sin{90º}$

$S = \pi · R^2$ <-- cercle
$S = a^2$ <-- quadrat
$S = b·a$ <-- rectangle
