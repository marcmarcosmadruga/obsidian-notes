# Impersonal Passive Voice
We use this sentences to express opinion. 

**It + passive (think/ believe) + that sentence**

For example:
- Nowadays it is believed that technology is addictive.
- Nowadays it is thought that technology is addictive.
- In the past it was believed that black cats brought bad luck.
- In the past it was thought that black cats brought bad luck.

