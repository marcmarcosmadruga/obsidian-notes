# Speaking TREC

- My TREC was about domotics, which is the field in informatics that aims to solve daily problems of people in their houses. For example this could be controling the lights, the plant watering system, the heating, the blinds of the windows and the alarm system through your smartphone. 
- In order to know more about domotics I did a lot of research on the topic and explained the history of domotics, all the parts in a domotics system and how do domotics help in everyday life.
- When I had all the theory down I started developing my own domotics prototype that unfortunately I can't show because I have it in my other house, and I don't any way of getting there. 
- The prototye consisted in a model that represented a five room house that had some domotics systems in it. Some of the things that it had were lights independent for each room, thermometer, alarm system and light detecting systems, among others. 
- All of these systems are controlled using an app that I also developed and processed using a script in my laptop that I also developed.
- While I was doing research I also found some really interesting cases of people with disabilities that used domotics to be more independent and life an easier life. This cases were really inspiring and a great portion of my TREC was about them.