## Exercises Passive Voice

4. Page 37
	1. Zara was founded by Amancio Ortega Gaona and Rosalía Mera in 1975.
	2. Their new product will be advertised on TV.
	3. A new book was published by that expert.
	4. All our customer are being sent details of the offers.
	5. The new shopping centre is being opened by The Queen.

1. Page 94
	1.  are attended
	2.  are being helped
	3.  was held
	4.  was set up
	5.  had been donated
	6.  were given
	7.  were told

2. Page 94
	1. A shoplifter was arrested this morning.
	2. Shoppers are offered discounts next week.
	3. The ring was stolen last week.
	4. Will the new shopping centre be opened by the mayor tomorrow?
	5. Will David Becham wear this shirt next season?

<br>

## Ejercicios Inglés 17-01
- A successful novel is written by Jack every two years.
- The exams are being revised by the English teacher.
- The flat tyres have been changed by the mechanic.
- The results were being revised by the scientists.
- The engine was checked by the pilot before the race.
- The document should be sent to England right now.
- Many languages are spoken here.
- Her birthday had been forgotten.


1. page 38
	1. sustainability
	2. compost
	3. consumption
	4. reuse
	5. landfill sites
	6. toxic
	7. landfill sites
	8. polute
	9. dumped
	10. recycled
	11. biodegradable

2. page 28
	1. I'm using recycled paper.
	2. The incineration caused environmental problems.
	3. We live ina diposable society.
	4. Yes, throw it away - it's disposable.
	5. Sustainable agriculture doesn't harm the environment.
	6. Manufactures are producing more and consumers are buying more.

## Fotocopia frases pasiva

1. A lie has been told to the police.
2. A letter will be sento to John (by us).

<br>

## Ejercicios 18/01
1. page 106
	1. changing room
	2. department store
	3. sweatshop
	4. browse
	5. get a refund
	6. save up
	7. in 

2. page 107
	1. Customer
	2. Online Store
	3. Sweatshop
	4. Ripping off
	5. Save up
	6. Department store

3. page 107
	1. You can’t feel the fabric of the product you are buying, you don’t have a changing room.
	2. They can rob you because you are paying online with a credit card.
	3. Most of the time you can’t get a refund
	4. The prices usually are cheaper and you can save up.

4. page 107
	1. that control hsa been lost


5. page 107
	1. He is in retail therapy.
	2. She's feeling down.
	3. They are hooke on to the mobile phones.
	4. She is up to her eyeballs in debt.
	5. The ring costs an arm and a leg.
	6. She is in a high.

<br>

	
## Ejercicios 24/01

5. page 37
	1. A new credit card has been sent to you.
	2. Is said that surnames make bad passwords.
	3. Is thought that online shopping is relatively safe.
	4. I was promised a complete refund.
	5. A guarantee was given to us by the company.
	 
7. page 95
	1. Nowadays organics items are popular. // Organic items are reported to be popular.
	2. A personal shopper is thought to be a luxury // Nowadays personal shoppers are a luxury.
	3. The products are rumoured to be made in factories with poor conditions.
	4. Buying products from a website is thought to be risky.
	5. This shopkeeper is thought to be dishonest.


5. page 30
	1. is said
	2. was reported
	3. to be employing
	4. to be
	5. is believed


## Ejercicios 27/01
1. page 36
	1. What's the value of goods which were stolen from its stores every year.
	2. Which of these can't be sold on eBay?
	3. What is the most that has ever been paid for a watch?
	4. Approximately how many items are sold by Amazon every second?
	5. In this photo, people are shopping at the Dubai Mall.


## Ejercicios 03/02/22
6. page 37
	1. My hair's very long. I need to get it cut.
	2. It's my mum's birthday. We're going to get a special cake made for her.
	3. Haver you ever got your password stolen by a hacker?
	4. He's a bit shy and doesn't like having his photo taken.
	5. Your car sounds strange. You should have it checked.
	6. Order from this website and you can get everything delivered straight to your door.

<hr>

1. page 50
	1. a curfew
	2. witnesses
	3. disorderly conduct
	4. jury service
	5. judge
	6. courts
	7. admit
	8. sentence


<hr>

4. page 48
	1. If a giant asteroid hadn't hit the Earth Dinosaurs wouldn't have became extint.
	2. If president Lincoln hadn't banned slavery a lot of African Americans wouldn't have lived freely.
	3. If Hitler hadn't came to power The Second World War wouldn't have started.
	4. If European countries wouldn't have exploited their colonies many African nations wouldn't have suffered poverty.
	5. If citizens hadn't protested many communist governments wouldn't have changed.

## Ejercicios 14-02-22

3. page 96
	1. If it rains I won't play football.
	2. If you hadn't overslept you wouldn't have seen him.
	3. Sara would volunteer if she had time.
	4. If 


4. page 96
	1. If the police had got more money, they would put more officers on the street.
	2. I'll call you if I get lost.
	3. Will you go fishing if it rains?
	4. If you won the lotter, what would you do?
	5. If I hadn't seen his speech, I wouldn't have changed my min.


5. Exercici Dictat
	1. Ningú et creurà si menteixes. *Nobody will believe you if you lie*
	2. Sara et perdonaria si et disculpesis. *Sara would forgive you if you apologized*
	3. Si els clients s'haguessin queixat, el venedor els hi hauria tornat els diners. *If the clients had complained, the vendor would have returned them all the money*
	4. Si Raquel fos una bona cap els seus empleats serien més eficaços. *If Rachel was a good boss, her employees would be more efficient*
	5. No resoldran el misteri si marxan ara. *They will not solve the mistery if they leave now*


## Ejercicos 17-02
7. page 109
	1. go to prison, find someone guilty of a crime
	2. commit a crime, reacah a verdict

8. page 109
	1. he was sentenced to five months in prison.
	2. The judge dismissed the case due to lack of evidence.
	3. Have the jury reached a verdict yet?
	4. The police arrested Michele for vandalism.
	5. you are charged with disorderly conduct.
	6. A dangerous criminal is being released from prison today.
	7. That's the second time you've accused me of lying!

<hr>

1. Conditional sentences
	1. If Sam painted the car, it would look like new.
	2. If you hadn't deleted the files we would have had the prove of our innocence.
	3. We wouldn't have to pay a fine if we had parked in another spot.
	4. You would have a discount if you paid in cash.
	5. I would trust you if you were honest.
	6. They would have received the package in time if they had made de order three days earlier.
	7. Will I find a bargain if I go shopping today? No, sales have ended.