# Formal Address

## Example
| Thing             | Example                   |
| ----------------- | ------------------------- |
| Recipient         | Dr. Martin Strange        | 
| Role or position  | Head Marketing Department |
| Enterprise        | Lenovo                    |
| Street            | 432 Chapel Street         |
| City and postcode | Manchester WIP 6HQ        |
| Country           | England                   |