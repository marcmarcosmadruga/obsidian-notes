## 1st type of conditional sentence
This conditional express probable situations.

_If + S + present simple, S + future_

**Examples: **
_If Tom overcharges us again, we won't come to his shop again._
_If Tom doesn't overcharges us again, we will come to his shop again._
_Will you come to Tom's shop if he overcharges you again?_

## 2nd type of conditional sentence
This conditional is used for hypotesis.

_If + S + pas simple (subjuntive) + S + would/could + infinitive_

**Examples:**
_If I had a job, I would rent a flat._
_If I didn't have a job, I wouldn't rent a flat._
_Would you rent a flat I you had a job?_