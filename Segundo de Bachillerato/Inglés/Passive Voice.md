# Personal Passive Voice

The police arrested the murderer <-- Active voice
The murderer **was** arrested by the police <-- Passive voice
The verb to be has to be in the tense of the active tense, in this case past simple.

_People will spend much money on the sales. --> Much money will be spent on the sales (by the people)_
In this phrase spend is infinitive and the direct complement is much money. In this case is assumed that people will be the agent so we don't need to put it. As much money is the subject we put it first and then we put will + be in present (be) because is the tense of the active phrase. And finally the past participle of the verb.

The police should find the murderer soon --> The murderer should be found by the police.
The police has arresteed the murderer --> The murderer has been arrested.
The police is arresting the murderer --> The murderer is being arrested by the police.

We are going to send these documents to our British partner this week.
Our British partner is going to be sent these documents this week.