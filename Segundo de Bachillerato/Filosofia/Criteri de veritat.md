# Criteri de veritat

- Servia per diferenciar allò vertader, d'allò fals.

- L'escolàstica feia ús de dos complementaris: la fe en la veritat revelada i l'argument d'autoritat.

Utilitzava com a argument d'autoritat una persona amb prestigi en la correcta interpretació de la Bíblia. Per tant la biblia era la font principal de coneixement i l'església era la autoritat útlima.