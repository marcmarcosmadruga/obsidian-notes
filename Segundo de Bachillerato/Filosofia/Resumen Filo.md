# Resumen de Descartes
## Context Historic i Filosofic

Va publicar el discord del mètode al 1637 i va ser considerat l'inici de la filosofia moderna. Està considerat el pare de la filosofia moderne perquè la seva filosofia suposa una nova proposta filosofica (racionalisme modern).

Descartes considera que abans d'intentar respondre a la pregunta què es realitat? s'ha d'inversitgar tant el procés com les facultats amb les quals s'elaborarà la resposta. Per tant, el tema central serà l'origen i constitució del coneixement, les seves possibilitats i límits.

Descartes va tenir tant de compte perquè la filosofia escolàstica (la que ell havia rebut) estava desacreditada. La escolàstica estava posada en dubte perquè els dos fonaments sobre els que s'edificava estaven posats en qüestió:

- [[Criteri de veritat]]
- [[Mètode]]

Aquest mètode a més va trontollar peruquè la societat medieval havia canviat i no tot girava entorn a Deu. Això provocava que el recurs a la fe es considerava insuficient. A més desde la reforma protestan l'autoritat s'havia posat en qüestió (es demana una interpretació personal de la biblia) (això fa que trontolli el criteri de veritat).

Davant de tot el descredit que estava sofrint la l'escolastica, l'objectiu de Descartes va ser restablir l'edifici del saber sobre unes bases sòlides i fermes. Per aconseguir això, necessitava un nou criteri de veritat i un nou mètode.

A més d'aquest obstacle també necessitava superar els arguments dels escèptics de l'hel·lenisme que s'utilitzaven per demostrar que de res podíem estar segurs. Per superar això Descartes pensa que nomès sent radicalment esceptic podría superar l'escepticisme (**dubte metòdic**) pero sempre amb l'intenció d'arribar a una veritat que fos totalment certa i indubtable.

  

A Descartes sobretot li preocupa l'error, es va donar conta de que allò que em donat per vàlid/correcte no ho és (creences). Llavors va arribar a les quatres normes/regles que necessitem per evitar l'error i arribar a tindre un coneixement vertader/indubtable/amb bona fonamentació:

1. Evidència (intuïció): només acceptar com a vertader les coses que es presentent al meu enteniment de manera clara i distinta.

2. Anàlisi: dividr les dificultats en parts més simples.

3. Síntesi: ordenar els meus pensaments desde les qüestions més simples a les més complexes.

4. Enumeració: qüestionar-nos tots els coneixements rebuts i buscar les veritats indubtables. El dubte és provisional.

## 1ª Meditació

En aquesta primera meditació exposa les raons per dubtar de totes les coses (en particular del món) i la utilitat del dubte metòdic. Tenia com a objectiu trobar uns primers principis que siguien evidents i indubtables sobre els que construir l'edifici sencer de la filo i la ciència. El farà amb el dubte metòdic.

_En que consisteix aquest dubte i com s'aplica?_

Fent ús dels arguments esceptics preten eliminar falses opinions i creences per començar de nou i establir bases fermes e indubtables. Per aconseguir-ho nomès necessita trobar el més petit motiu de dubte per rebutjar-les.

El dubte metòdic s'aplica de manera sistemàtica seguint els següents passos:

1. Primer exposa una raó que ens permet dubtar d'alguns aspectes del nostre coneixement (criteri de dubte)

2. Com hi haurà coneixements que resistint aquest dubte hem de agafar un nou criteri de dubte més exigent.

3. Aixó anirem proposant criteris de dubtes per veure si hi halgun coneixement que els pugui resistir tots.

4. Si hi ha algun coneixement que superi tots els criteris de dubte tindríem la certes de que es una veritat totalment certa e indubtable.

### Criteris de dubte

1. **Error dels sentits**. Desconfiança de la informació que ens proporcionen els sentits.

 Aquest criteri no ens permet rebatir la propia existencia de les coses.

2. **Distingir l'estat de vigília i els somnis**. I si la vida és un somni?

 Aquest criteri no posa en dubte les veritats matemàtiques, perquè tant desperts com dormits 2+2=4.

3. **La hipòtesis del geni maligne**: Aquest es el criteri més radical perquè fa que dubtem dels coneixements independents de l'experiència. I si hi ha una consciència superior que m'enganya?

Amb aquest ultim criteri de dubte. el dubte es torna hiperbòlic (tot és sospitós). Si trobo una veritat en el dubte, aquesta serà indubtable i superaré l'escepticisme.

### Resultat d'aplicar el dubte metòdic

El dubte metòdic ens ha portat a dubtar de tot allò que creiem saber. Com no hi ha una cosa de la que no pugui dubtar, hem de suspendre el judici. Em de persistir en el dubte i recordar-lo, perquè la costum ens inclina a creure en les antigues opinions.

<hr>

Per acabar aquesta primera meditació fa una recapitulació de allò que ha posat en dubte:

- El món material (el propi cos) **Món**

- Les veritats matemàtiques

- Els sentits.


Hi reflexiona també sobre la utilitat d'aquest tipus de dubte tan radical com el que ha desenvolupat:

- Ens allibera de tots els prejudicis.

- Acostuma al nostre esperit allunyar-se dels sentits (fonts principal de l'error)

- Fa que no puguin tindre cap dubte respecta d'allò que més endavant descobrirm com a vertader.

## 2ª Meditació

Descartes arriba, a través del dubte a l'afirmació de que existeix i de que es una subtància pensant.

Per descobrir-ho repren del dubte i es centra en la possibilitat de l'existència del jo fent dues preguntes:

- Soc tan dependent del cos i dels sentits que sense aquest no puc ser?

- Si no hi ha res al món, jo tampoc no existeixo?


En aquest moment arribem a una primera afirmació indubtable (1ª veritat). Descartes diu que durant tot el procès, no ha fet res més que pensar, potser les coses en les que estaba pensant eren erronies (el contingut) però el que es indutable es que ha pensat.

A través d'això arriba a la conclusió de que _pensa, aleshores existeixo_ **Cogito Ergo Sum**.

Aquesta veritat és una certesa, un coneixment absolutament cert que utilitzarà com a **criteri de evidència** Nomès acceptarà com a vertader allò uqe es presenti a la raó amb el mateix grau de claredat i distinció que l'afirmació Penso, aleshores existeixo.

Desprès d'obtindre el criteri d'evidència (la seva primera veritat), comença preguntant-se: _sé amb certesa que soc, però no sé amb claredat què soc?_. Llavors va començar a examinar el que creia ser.

Dels seus pensaments dedueix l'existencia del cos i de l'ànima (posats en dubte encara però) i repassa els atributs que sempre havia cregut que tenien.


- Cos: dubta del cos per la hipòtesi del Geni maligne i perquè aquests atributs els coneixem mitjançant els sentits.

- Ànima: pot dubtar d'alguns (caminar o créixer) perquè són accions que també es donen en els somnis però hi ha una de la qual no pot dubtar: **el fet de pensar**.


Arriba llavors a donar una resposta: _No soc més que una cosa que pensa, és a dir, un esperit, un enteniment o una raó._. És una subtància pensant (res cogitans --> conjunt de pensaments, representacions, idees... Una subtància imperfecta dotada de raó.)


Descartes afirma que l'ànima és diferent i independent del cos perquè no necessita cap lloc ni depèn de cap cosa material per a existir. A més també és més fàcil de conèixer.


Llavors es pregunta: què és una cosa que pensa? Respon que és el subjecte de les activitats de dubtar, concebre, entendre, afirmar, negar, imaginar, sentir, voler i no voler. Totes aquestes son activitat que es donen en el pensament, per tant són pròpies de l'anima.

Descartes las ha anat exercitant en el procés de dubte metòdic i tenen per a ell el mateix grau de certes que la primera veritat. Superen la hipòtesis del geni maligne (tot i que m'engany en el que penso, es indubtable que penso) i la dificultat per distingir la vigília del somni (tant despert com dormit, penso). 

La segona meditació acaba amb una reflexió sobre el paper que tenen en el coneixement els sentits la imaginació i l'enteniment per demostrar que és més fàcil coneixer les coses que pertanyen a l'ànima que les que pertanyen a les coses corpòries.


## 3ª Meditació

Descartes es troba dintre del solipsisme (només puc afirmar l'existència del jo).

Les veritats que més s'assemblen al criteri de veritat establert són les veritats matemàtiques, però estan posades en dubte a partir de la hipòtesis del geni maligne. Si aconseguíssim demostrar que Déu existeix i que no vol que m'enganyi podríem eliminar aquesta hipòtesi i restablir aquestes veritats.

### Demostració de que Déu existeix
Descartes parteix d'una evidència: pensem idees. Per tant hem d'analitzar els continguts del pensament per comprovar si totes les idees tenen la mateixa naturalesa i fiabilitat. Per això investiga el contingut de la nostra ment: les idees.

Descobreix tres tipus:
- Idees adventícies (adquirides): aquelles que ens mostren els sentits.
- Idees factícies (artificials): idees que la imaginació forma a partir d'altres (sirena)
- Idees innates (naturals): sembla que han nascut amb mi, són clares i distintes (infinitud, perfecció)
	Són les unisques fiables, aquelles a partir de les quals es podrà intentar reconstruir el saber. 
	
La idea de Déu es troba dintre de la nostra ment però encara falta passar de la idea de Déu a demostrar la seva existencia. Utilitza tres arguments:
- Argument de la infinitud: tenim la idea de perfecció i infinitud (Déu). Aquestes idees no poden provenir d'algú finit i imperfecte (com jo) nomès poden ser originades de Déu. Ell ha posat en mi la idea d'una subtancia infinita.
- Argument de la causalitat aplicada al jo: El jo es finit i imperfecte. És evident que jo no me he creat a mi mateix perquè ho hauria fet mitjançant les idees innates que posseixo (finit-imperfecte), per tant la causa del meu ésser i de tot el que existeix té el seu origen en un ésser perfecte.
- Argument ontològic: Si Déu és un esser summament perfecte, no li pot mancar una de les seves perfeccions (Existir) per tant existeix.

## 4ª Meditació
Comença la quarta meditació amb l'estudi de la naturalesa de Déu. Déu es veraç (no m'enganya quan faig ús de la raó). 
Amb aquesta reflexió anul·lem la hipòtesi del geni maligne, el dubte deixa de ser hiperbòlic i Déu, en tant que substancia veraç i perfecta, es converteix en criteri d'evidencia. 

L'existència de Déu m'assegura que les idees clares i distintes que tenim sobre la realitat exterior son veritables.

L'únic que cal preguntar-se és d'on prové l'error (perquè no sempre podem distingir allò vertader i allò fals). Descartes diu que l'error prové de quan intentem anar més enllà de l'enteniment utilitzant la voluntat.

## 5ª Meditació
En aquesta meditació Descartes demostra l'existència de Déu a partir de la següent idea que ell considera innata: la idea d'un ésser summament perfecte. 

Diu que aquesta idea no pot ser adventícia perquè no hem tingut coneixement d'ella a través dels sentits i tampoc factícia perquè la nostra imaginació (que pertany a un ésser imperfecte) no pot produir una idea d'un ésser perfecte. Analitza aquesta idea innata i aplica el criter d'evidencia.

Per tal d'analitzar-la la compara amb l'idea d'un triangle. Amb un triangle tenim que la suma dels seus angles es sempre dos angles rectes. Descartes diu que es inconcebible que a Déu li manqui alguna de les seves perfeccions (perquè es perfecte) al igual que els angles d'un triangle no poden sumar un altre número que no sigui 180. Per tan, Déu existeix.

## La Concepció del coneixement: la demostració del món material
A la sisena meditació prova l'existència del món i assenyala la distinció entre el cos i l'anima en l'ésser humà.

Si ens examinem a nosaltres mateixos podem veure que el nostre cos i
sentits son diferents de la nostra ment (jo) que es caracteritza pel
pensament. Com Déu és veraç és capaç de crear dues substàncies diferents.
Món (inclòs el nostre cos: cosa material que forma part del món) és una
substància que es caracteritza per l’extensió (Res extensa.) Substància
material.
Aquesta és una visió mecanicista de la realitat física: el món és una gran
màquina que es compon de peces extenses amb moviment. Amb poques
lleis físiques i mecanicistes podem explicar tots els fenòmens naturals.

Per demostrar l'existència de les coses materials (món), Descartes s'interroga per l'origne de les idees de les coses sensibles (idees adventícies). Ell diu que si te la facultat de sentir hi ha d'haver una facultat activa capaç de formar i produir aquestes idees. És una demostració causal en la qual a partir de l'efecte es busca quina és la seva causa.

Descartes es pregunta si podria ser ell mateix la causa d'aquestes idees però diu que no pot ser perquè no depenen de la seva voluntat ni l'obeixen. També creu que la causa podria ser Déu però diu que Déu no és pero que li dona una podera inclinació per a creure que les idees sobre el món partiexen de les coses materials. Treu la conclusió de que l'origen de les idees sensibles són les coses materials.

Llavors Déu li garanteix que les idees aventicies tenen el seu origen i causa en les coses materials però no que aquestes siguien tal com les percebem a través dels sentits. Persisteix el dubte sobre la fiabilitat dels sentits.

Descartes diu que tenim una única evidencia; tenim idees en la ment per tant coneixem les idees i no pas directament les coses (idealisme cartesià).

Descartes pensa que Déu només ens garanteix que les nostres idees mostren el que les coses són respecte a les qualitats primàries (descrites per lleis físiques i mecàniques), però no respecte a les qualitats secundaries (donades pels sentits).

Qualitats primaries -> magnitud, figura, nombre, situació, durada i moviment.
Qualitats secundaries -> colors, olors, sabors, sons, etc.