# Resumen del Resumen Descartes

## Discurs del Métode
A Descartes li preocupava el error per tant quan va fer el seu métode (dubte cartesià) va establir quatre normes:
1. Evidència: només acceptar les coses que es presenten de manera clara i distinta.
2. Anàlisi: dividir les dificultats en parts més simples.
3. Sintesi: començar a resoldre les coses més simples i després les més dificils.
4. Enumeració: abans de donar per veritable un coneixement cal qüestionar-se tots els coneixement rebuts.

## 1ª Meditació
Raons per dubtar de les coses (en particular del món) i utilitat del dubte metòdic. Objectiu: trobar principis que siguin evidents i indubtables per construir l'edifici sencer de la filosofia i ciencia.

Passos:
1. Exposar una raó que ens permeti dubtar d'alguns aspectes del nostre coneixement.
2. Si resisteix el dubte, posem un criteri de dubte més exigent.
3. Aixó anirem proposant criteris de dubte, per veure si hi ha algun que els resisteixi tots.
4. Si els supera tots, trindrem certersa de que es una veritat totalment certa e indubtable.

### Criteris de dubte
1. **Errors del sentits:** Desconfiança de la informació que ens proporcionen els sentits.
2. **Distingir l'estat de vigília i els somnis:** I si la vida és un somni?
3. **La hipòtesis del geni maligne:** I si hi ha una consciència superior que m'enganya? 

## 2ª Meditació
Descartes diu que durant tot el procès de dubtar del dubte, no ha fet res més que pensar, potser les coses en les que estaba pensant eren erronies (el contingut) però es indubtable que ha pensat.

Aquest és el cogito cartesià (*Cogito Ergo Sum*, penso aleshores existeixo).

Utilitzarà aquesta veritat com criteri de evidència, nomès acceptarà com a vertader allò que es presenti en la raó amb el mateix grau de claredat i distinció que aquesta afirmació.

Després Descartes va començar a examinar el que creia ser. Dels seus pensaments dedueix l'existencia del cos i de l'ànima per separat (posats en dubte encara) i repassa els atributs que creia que tenien.

- Cos: dubte per la hipòtesi del Geni maligne i perquè aquests atributs els coneixem mitjançant els sentits.
- Ànima: pot dubtar d'alguns (caminar o créixer) perquè són accions que també es donen en els somnis però hi ha una de la qual no pot dubtar: **el fet de pensar**.

Arriba a la conclusió: *No sóc més que una cosa que pensa, és a dir, un esperit, un enteniment o una raó*. És una substància pensant (*res cogitans*).

Descartes afirma que l'ànima és diferent i independent del cos perquè no necessita cap lloca ni dpèn de cap cosa material per existir. Es més facil de coneixer.

Que es una cosa que pensa? Subjecte de activitats com dubtar, concebre, entendre, afirmar, negar, imaginar, sentir, voler i no voler. 

Com Descartes porta tot el procés dubtant, el fet de que ell pensa te el mateix grau de validesa que la primera veritat.

## 3ª Meditació
Volem aconseguir demostrar que Déu existeix i que no vol que m'enganyi.

Es pregunta si totes les idees tenen la mateixa naturalesa i fiabilitat. Per això investiga el contingut de la nostra ment: les idees.

Descobreix tres tipus:
- Idees adventícies (aquirides)
- Idees factícies (artificials)
- Idees innates (naturals)

Utilitza tres arguments:
- Argument de la infinud: tenim la idea de perfecció i infinitud
- Argument de la causalitat aplicada al jo: no puc tenir la idea de infinitut si no me la ha posat deu.
- Argument ontològic: si Déu es perfecte no li pot mancar una de les seves perfeccions: existir.

## 4ª Meditació

Comença la quarta meditació amb l'estudi de la naturalesa de Déu. Déu es veraç (no m'enganya quan faig ús de la raó).  
Amb aquesta reflexió anul·lem la hipòtesi del geni maligne, el dubte deixa de ser hiperbòlic i Déu, en tant que substancia veraç i perfecta, es converteix en criteri d'evidencia.

L'existència de Déu m'assegura que les idees clares i distintes que tenim sobre la realitat exterior son veritables.

L'únic que cal preguntar-se és d'on prové l'error (perquè no sempre podem distingir allò vertader i allò fals). Descartes diu que l'error prové de quan intentem anar més enllà de l'enteniment utilitzant la voluntat.

## 5ª Meditació

En aquesta meditació Descartes demostra l'existència de Déu a partir de la següent idea que ell considera innata: la idea d'un ésser summament perfecte.

Diu que aquesta idea no pot ser adventícia perquè no hem tingut coneixement d'ella a través dels sentits i tampoc factícia perquè la nostra imaginació (que pertany a un ésser imperfecte) no pot produir una idea d'un ésser perfecte. Analitza aquesta idea innata i aplica el criter d'evidencia.

Per tal d'analitzar-la la compara amb l'idea d'un triangle. Amb un triangle tenim que la suma dels seus angles es sempre dos angles rectes. Descartes diu que es inconcebible que a Déu li manqui alguna de les seves perfeccions (perquè es perfecte) al igual que els angles d'un triangle no poden sumar un altre número que no sigui 180. Per tan, Déu existeix.

## La concepció del coneixement: la demostració del món material
A la sisena meditació prova l'existència del món i distingeix entre el cos i l'ànima.

El nostre cos i els sentits son diferents a la nostra ment. Déu és veraç i llavors es capaç de  crear dues substàncies diferents. Aquesta es una visió mecanicista de la realitat física: món = gran màquina que es compon de peces amb moviment. 

Per demostrar l'existència de les coses s'interroga per l'origen de les idees de les coses sensibles. Si te la facultat de sentir hi ha d'haver una facultat activa capaç de formar i produir aquestes idees. Demostració causal a partir de l'efecte es busca quina és la seva causa.
 
 Descartes descarte ser ell el causant d'aquestes idees perquè no depenen de la seva voluntat ni l'obeixen. La causa podria ser Déu pero al final ho nega pero li dona una poderosa inclinació a creure sobre que les idees sensibles parteixen de les coses materials.
 
 Les idees adventicies tenen el seu origen i causa en les coses materials però no són tal com les percebem a través dels sentits. Persisteix el dubte sobre la fiabilitat dels sentits.
 
 Descartes diu que tenim idees en la ment per tant coneixem les idees i no pas direactament les coses.
 
 Déu només ens garanteix que les nostres idees mostren el que les coses són respecte a les qualitats primàries (descrites per lleis físiques i mecàniques) però no respecte a les qualitats secundaries (donades pels sentits).
 
 Qualitats primaries -> magnitud, figura, nombre, situació, durada i moviment.
 Qualitats secundaries -> colors, olors, sabors, sons, etc.