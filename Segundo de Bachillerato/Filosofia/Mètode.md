# Mètode

El metode indicava quins eren els passos que haviem de seguir per establir qualsevol nova veritat.

L'escolàstica utilitzava el mètode deductiu del sil·logisme aristotèlic. A partir d'una idea general (premissa major) i fent ús d'una idea particular (premissa menor) s'estableix una nova veritat (conclusió) que es deriva necessàriament de les anteriors.

- Tots els homes són mortals, sòcrates es home, per tant sòcrates es mortal

  

Aquest metode tenia el problema de que per tal de que la conclusió fosi valida, la premissa major havia de ser certa i la manera de justificar-ho era amb el criteri de veritat (posat en dubte). També té el problema de que no implicava cap nova veritat perquè tot el coneixement final ja estava present a la premissa mejor.