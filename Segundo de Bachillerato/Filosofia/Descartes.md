# Descartes

Es considerat el pare de la filosofia moderna, tota la filosofia posterior va estar influenciada per el seu pensament. La seva obra va ser el Discurs del mètode (aquest va ser el fet que marca l'inici de la filosofia moderna). La seva filosofia suposa una nova proposta filosòfica (Racionalisme Modern). Inicia un nou enfocament en la forma de fer filo (gnoseològic).

Descartes pensava que abans d'intentar respondre la pregunta *què es la realitat?* s'ha d'investigar tant el procés com les facultats amb les quals s'elaborarà la resposta.
El tema central per a ell serà **l'origen** i **constitució** del coneixement, les seves **possibilitats** i els seus **límits**. Descartes era racionalista (creia que la unica via possible per a tenir una coneixement verdader era la raò) i va criticar l'empirisme. 

Descartes va tenir molta prevenció abans d'elaborar el seu nou sistema filosofic perquè la filosofia escolàstica estava molt desacreditada i ell no volia que el seu nou sistema filosofic no caigui en el mateix que la filosofia escolàstica. 

Al segle XVI la filosofia escolàstica es considerava caduca i esteril. Això va succeir perquè els dos pilars sobre els que es construia es van posar en dubte, el **criteri de veritat** i el **mètode**. 

La filosofia escolàstica utilitzava com a criteri de veritat **la fé** en la veritat revelada i l'**argument de l'autoritat** (en aquell cas en com interpretava la biblia). Llavors la biblia era la font principal de coneixement i l'esglesia era l'autoritat última. 

El seu mètode indica quins són els passos que hem de seguir per establir qualsevol nova veritat. La filosofia escolàstica utilitzava el mètode deductiu del sil·logisme aristotelic. Consisteix en partir de una idea general (premisa major) i fent ús d'una idea particular (premisa menor) i s'estableix una nova veritat (conclusió). La conclusió deriva de l

*Tots els homes són mortals (idea general), Socrates és home (idea particular), per tant Socrates és mortal (conclusió).* 

Problemes del mètode:
1. Tot i que la deducció sigui correcta, necessitem que la premissa major (tots els homes són mortals) sigui certa.
2.  No aporta cap nova veritat perquè allò que s'afirmava en la conclusió ja està present en la premissa major (que Socrates és mortal va implicit dins de que tots els homes son mortals).

La qüestió és que la societat medieval havia canviat i no tot girava entorn a Deu. Per tant, el recurs a la fe es considerava insuficient. A més, des de la reforma protestant l'autoritat s'havia posat en qüestió també (Es demanava una interpretació personal de la bíblia).

L'humanisme renaixentista desauteritza l'intel·lectualisme aristotèlic. La revolució científica va elaborar teories alternatives sobre el funcionament de la natura i de l'univers. L'objectiu de la filosofia cartesiana era restablir l'edifici del saber sobre unes bases sòlides fermes. Per fer això necessitanva un **nou criteri de veritat** i un **nou mètode**.

A més ha de superar un altre obstacle: l'escèpticisme de l'hel·lenisme, que s'utilitzava per demostrar que de res podíem estar segurs. Per tal de superar aquest escepticisme Descartes arriba a la conclusió de que només sent radicalment escèptic podia superar l'escepticisme (dubte metòdic) però amb la intenció d'arribar a una veritat que fos totalment certa i indubtable. 

Una influència en el seu pensament va ser la nova ciència. D'aquí va prendre la idea de que la natura **es pot comprendre i expressar mitjançant proporcions matemàtiques simples**. Per a ella, el mètode de deomstració matemàtic representa el model ideal del saber rigorós i cert. 

## Les quatre regles del mètode
- Cal no admetre res que pugui ser dubtó i només acceptar com a vertader allò clar i distint.
- Dividr les idees complexes en parts simples. acceptant les més evidents com a vertaderes.
- Conduir per ordre els pensaments dels més simples als més complexos.
- Revisar tot el procès per estar segurs de no haver omès res.

Una veritat indubtable es evident, clara i distinta.


### La segona meditació
Descartes arriba, a través del dubte, a l'afirmació de la pròpia existència i al descobriment de la nostra naturales com a susbtància pensant. 

Per aconseguir-ho repren el dubte i el centra en la possibilitat de l'existencia del jo fent dues preguntes:
- Soc tan dependent del cos i dels sentits que sense aquests no puc ser?
- Si no hi ha res al món, jo tampoc existeixo?

Llavors arriba a la primera afirmació indubtable: en tot el procés no he fet més que pensar, i puc dubtar dels continguts (allò que penso) però no de la activitat (el fet de pensar). Penso, aleshores existeixo.

Aquesta primera veritat el proporciona el criteri de veritat que Descartes buscava. També li dona un model de veritat que pot ser acceptada com a veritat. Per a Descartes tota veritat habia de ser clara i distinta.

En aquesta segona meditació Descartes es pregunta que ell _es_ ero no sap amb clared _què és_. Comença a examinar el que _creia ser_.

Dels seus pensaments dedueix el cos i l'ànima i repassa els seus atributs per veure si resisteixen el dubte. 

**Cos:** dubta d'ells per la hipòtesi del Geni maligne i perquè aquests atributs els coneixem mitjançant els sentits.

**Ànima:** pot dubtar d'alguns (caminar o créixer) perquè són accions que també es donden en els somnis. Pero hi ha una de la qual no pot dubtar: _el fet de pensar_.

Llavors va arribar a una segona veritat: "No soc més que una cosa que pensa, és a dir, un esperit, un enteniment o una raó." I concluou: soc un ésser la naturalesa o essència del qual consisteix a pensar, soc una substància pensant (res cogitans)

separa el cos i l'anima perquè l'ànima no necessita cap lloc ni depèn de cap cosa material per a existir. A més, és més fàcil de conèixer.

Llavors es pergunta: què es una cosa que pensa?

És el subjecte de les activitats de dubtar, concebre, entendre, afirmar, negar, imaginar, sentir, voler i no volver. Totes aquestes activitats es donen en el pensament, per tant són pròpies de l'ànima. 

La segona meditació acaba amb una felexió sobre el paper que tenen en el coneixement els sentits, la imaginació i l'enteniment er desmostrar que és més facil coneixer coses de l'ànima. 

Analitza el sentit comú que considera que es coneixen millores les coses particulars que no pas un mateix. Posa l'exemple d'un tros de cera que sotmes a canvis desapareixen les qualitats inicials conegues pels sentits (qualitats secundàries (color, mida, olor, sabor)) i com allò que es manté en els canvis (qualitats primàries).

#### Conclusió de la segona meditació

Afirma que res no és més fàcil de conèixer que l'ànima, ja que del cos pot dubtar, però no de l'existència del pensament.

### Concepció del coneixement: demostrar que Déu existeix

- Descartes ha trobat una primera veritat i un criteri de veritat a partir dels quals pretén reconstruir l'edifici del saber sobre nous fonaments sòlids i ferms.
- Sap que existeix i que és una substància pensant.
- Quin és el camí que cal seguir? Analitzar quines de les coses posades en dubte es podrien recuperar, però ara amb un coneixement ben fonamentat.

Per restablir les veritat matemàtiques (posades en dubte a partir de la hipòtesis del geni maligne). Si aconseguíssim demostrar que Déu existeix i que no vol que m'engany que raono correctament, podríem eliminar aquest hipòtesi i restablir aquestes veritats.

Descartes parteix d'una evidència: *Pensem idees*. Per tant hem d'analitzar els continguts del pensament per comprovar si totes les idees tenen la mateixa naturalesa i fiabilitat.

Descobreix tres tipus d'idees:
- Idees adventícies: aquelles que ens mostren els sentits i pareix que procedeien de fore de mi (món material). Per exemple: un color, la calor, etc. Nosón fiables perquè els sentitsa vegades ens enganyen.
- Idees factícies: idees que la imaginació forma a partir d'altres idees. Per exemple:  una sirena o un hipogrif. No tenen realitat ni fiabilitat.
- Idees innates: aquelles que semblen que han nascut amb mi i que formen part del meu pensament. Per exemple: la idea de pensar o d'existir. 
	- Son les úniques fiables.
	- Aquelles a partir de les quals es podrà intentar reconstruir el saber.
	- Afirmar l'existència d'aquestes idees innates és un pressupòsit bàsic de tota teoria dle coneixement racionalista.