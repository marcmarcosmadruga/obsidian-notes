Argument de la infinitud: Entre les nostres idees hi ha la idea de perfecció i infinitud (Déu). Aquestes no poden provenir d'algú finit i imperfecte (com jo) només pot tenir com a causa un ésser infinit i perfecte que és Déu. Déu ha posat en mi la idea d'una subtància infinita. 

Argument de la causalitat aplicada al jo: Ejo es finit i imperfecte. És evident que jo no me he creat a mi mateix perquè ho hauria fet mitjançant les idees innates que posseeixo (finit-imperfecte). Per tant la causa del meu ésser i de tot le que existeix té el seu origen en un ésser perfecte. Déu com a causa del meu ésser i de tot el que existeix.

Argument ontològic: Pretén provar l'existència de Déu partint de la mateixa idea de Déu. Si Déu és un esser summament perfecte no li pot mancar una de les seves perfeccions (Existir) per tant existeix.

Comença la quarta meditació amb l'estudi de la naturalesa de Déu. Déu també ha de ser veraç (no m'enganya quan faig ús de la raó; les coses que jo veig de mode clar i distint son veritat (com passa en es veritats matemàtiques))

Amb aquesta reflexió anul·la la hipòtesi del geni maligne, el dubte diexa de ser hiperbòlic i Déu, en tant que substància veraç i perfecta, es converteix en criteri d'evidència. 