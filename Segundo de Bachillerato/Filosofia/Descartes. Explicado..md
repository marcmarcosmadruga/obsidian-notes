# Descartes. Explicado.

Descartes empieza a filosofar por su propia cuenta. Considerado el padre de la filosofia moderna. 

Descartes quería saber que es lo que podemos conocer de manera cierta. La otra gran qüestión fue la relación entre el alma y el cuerpo.

Se planteó si había un metodo similar al metodo cientifico pero para la reflexión filosófica.

Las cuatro reglas del metodo fueron.
- Evidencia: Solo aceptar como verdad aquello que se presente de forma clara y distinta.
- Análisis: Dividir un problema complejo en muchos problemas simples.
- Síntesis: Empezar por los problemas más simples.
- Enumeración: Repasar todos los pasos para asegurarnos que no nos hemos equivocado.

Descartes estaba convencido de que solo la razón puede proporcionarnos conocimientos seguros y que no nos podemos fiar de los sentidos. Descartes entonces pertenece a los racionalistas.

En la primera meditación explica que como punto de partida va a dudar de todo, librandose de todas las ideas viejas para empezar con fundamentos claros y distintos. No solo duda de las opiniones antiguas, sinó también duda de los sentidos, ya que no podemos distinguir el sueño de la vigilia. 

En la segunda meditación Descartes parté de que duda de todo, pero solo hay una cosa de la que puede estar seguro: de que duda. duda -> piensa -> pienso, luego existo. Esta verdad es una verdad clara y distinta (res cogitans).

En la tercera meditación Descartes se pregunta si hay algo igual de evidente que la existencia del yo y llega a la conclusión de que tiene una idea clara y definida de un ser perfecto. Siempre ha tenido esta idea y para él resulta evidente que la idea de un ser perfecto no puede proceder de un ser imperfecto (de él). La idea de Dios para Descartes es una idea innata. 

En la cuarta meditación se pregunta que como puede ser que nosotros seamos imperfectos habiendo sido creados por un ser perfecto. Descartes diferencia dos facultades. El entendimiento (conocer) limitado y la facultad de la voluntad (querer) que és ilimitada. El entendimiento encuentra rapidamente los limites pero la voluntad quiere continuar. Al pasar del entendimiento a la voluntad se comete a este error al relizar juicios sobre cosas que no etendemos (más allá del entendimiento). La culpa del error es la voluntad.

En la quinta meditación reconoce que la realidad exterior tiene cualidades que pueden ser reconocidas por la razón. Estas cualidades son las matemáticas. Aquí utiliza a Dios para afirmar que cualquier cosa que reconozcamos con la razón de la realidad exterior corresponde con algo real. 

En la sexta meditación afirma que aunque existe una realidad exterior pero hay otra realidad distinta donde està el pensamiento. Diferencia el alma (res cogitans) y la extensión (res extensa), Dios existe independientemente de estás dos substancias (res infinita). Las dos substancias son independientes la una de la otra, por esto Descartes es un dualista. Las dos substancias pese a ser independientes están relacionadas. 

Como se relacionan las dos substancias si son distintas? Descartes opina que mientras el alma esté en el cuerpo está relacionada con este gracias a la glándula pineal. Es en este glándula pineal donde se hace una relación entre el alma y la materia. Por esto el alma se ve confundida por las necesidades del cuerpo. El alma puede independizarse de estos sentimientos y operar por libres, por tanto el alma es superior al cuerpo, porque el cuerpo puede envejecer, pero que 1+1 siempre será 2 (mientras que conservemos la razón)