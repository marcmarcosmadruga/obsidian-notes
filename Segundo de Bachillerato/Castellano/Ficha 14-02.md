# Ficha 14-02

1. Par mínimo
	En a hay una perifrases modal de obligación mientras que en b hay una oración compuesta con un verbo volitibo que expresa voluntad o deseo.
	
2. Análisis Inverso
	Verbo auxiliar ->haber + participio, ser + participio, v. en forma perifràstica
	Adjetivo con afijo derivativo -> servible, 
	PN de CD -> lo/la
	Verbo semicopulativo -> volverse
	
	Se ha vuelto alocado desde que lo han dejado.
	Seguía ilusionada después de haberlo conocido.
	
3. Ambigüedad
	*Vi a mí hermano andando por la calle.*
	El sujeto del verbo puede ser tanto el emisor como mi hermano, porque el verbo al estar en gerundio concuerda con ambos. 
	
4. Identificar la oración
	b.
	

5. Par mínimo
	Deben ser etiquetados -> Obligación
	Deben de ser etquiteados -> Probablidad
	
6. Analisis Inverso
	Pn relativo -> que, quien, cual
	C.Agente -> pasiva
	Adverbio de foco -> solo, incluso
	V.Inacusativo -> 
	
	Solo María fue vista por quien la dejó entrar
	
	

## Tipos de subordinadas
-