# Tipos de 'se' 2
## Pronombres con función sintáctica
#### Pronombre Personal Átono Sustituto de _Le_
Le compré a Juan una cazadora --> Se la compré

#### Pronombre Reflexivo Anafórico
Concuerda con su antecedente que es un argumento del verbo.
Pau se peinaba con esmero ántes de su cita con Miriam.

#### Pronombre Recíproco Anafórico
Concuerda con su antecedente que es un argumento del verbo.
Los enamorados se miraban embelesados (entre ellos).

## Pronombres sin función sintáctica
#### Se pronominal.
Morfema del verbo. Concuerda en nº y persona con el sujeto.

Hay verbos pronominales inherentes -> acordarse, arrepentirse
Hay verbos no pronominales que se pueden usar como pronominales -> ocupar algo / ocuparse de algo.

