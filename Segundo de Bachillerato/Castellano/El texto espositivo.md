# El texto expositivo

## Estructura
- Presentación del tema (1 párrafo) --> Defino el tema.
- Cuerpo expositivo: Ideas nuevas sobre el tema principal. (2-3 párrafos) --> Detallo aspectos.
- Cierre: Resumen de las ideas importantes (1 párrafo final) --> Resumir.

## Tipos de texto expositivo
1. Exposición divulgativa:
2. Exposición especializada:

## Rasgos lingüísticos:
Objetividad: oraciones enunciativas.
3ª persona del singular / 1ª plural, plural de modestia.
O con indeterminación de agente (Oración pasiva refleja, Impersonal refleja)
Presente de indicativo, presente atemporal

Precisión: 
Tecnicismos i/o acepciones técnicas.
Adjetivos especificativos, relacionales i oraciones de relativos especificativos.

oligarquía, anarquía, monarquía, democracia.