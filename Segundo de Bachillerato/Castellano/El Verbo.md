# El Verbo

Solo tienen estructura argumental los verbos predicativos, los verbos copulativos no tienen estructura argumental porque tienen significado léxico.

- Juan tiene/hace
- Juan lee
Leer solo exige un participante mientras que tener y hacer seleccionan dos argumentos, por tanto, la primera oración es agramatical. 

Podemos clasificar los verbos según el número de argumentos:
- Avalentes: no seleecionan ningún argumento, suele coincidir con los verbos de fenómenos atmosféricos (amanecer, llover y tronar).
- Monovalentes: seleccionan solo un argumento. Este argumento suele ser un sujeto o un complemento directo (todos los verbos intransitivos).
- Bivalentes: seleccionan dos argumentos.
- Trivalentes: seleccionan tres argumentos (dar, enviar, colocar, poner, etc.)

Argumentos: sujeto, complemento directo, complemento de régimen verbal, complemento indirecto (solo en algunos casos), complemento locativo argumental.

- El tendero pesó el saco de naranjas.
- El saco de naranjas pesó tres kilos.
En a el saco de naranjas es el complemento directo mientras que en la segunda oración tres kilos es un complemento locativo argumental de medida. ^e089f7

Adjuntos: complemento indirecto (cuando no es seleccionado por el verbo), complementos circunstanciales, complementos agentes.

CCL/TM/Cantidad -> Adjuntos
C.Locativo Argumental -> Argumento (CC de lugar)
C.Argumental de medida -> Argumento (CC de cantidad)

El saco pesa dos kilos (_dos kilos_ -> C.Arg de Medida)
Maria pesa el saco de dos kilos. (_dos kilos_ -> C.N)

<br>

### Ejemplo par mínimo
a) \*Le película duró.
b) La película terminó.

La oración *a* es agramatical porque falta un complemento argumental de medida ya que el verbo durar lo exige.

