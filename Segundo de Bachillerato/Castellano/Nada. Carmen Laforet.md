# Nada. Carmen Laforet.

La protagonista se llama Andrea, es huérfana y acude a Barcelona durante un año a estudiar el primer año de la carrer de Letras. Andre cuenta toda la historia desde la distáncia, una vez ya está instalada en Madrid.

La novela está formada por tres partes:
- **1ª** (1-9): Llegada a Barcelona.
- **2ª** (10-18): El hambre.
- **3ª** (19-25): Nueva ilusión.

En Nada se nos presentan tres mundos:
- Calle Aribau
- La Universidad --> Casa Ena
- Estudio Guíxols

<br>

## Primera Parte
Andrea llega de noche sola a la Estación de Francia. Ella iba a Barcelona buscando libertad y al llegar a la Estación se siente libre. Andre tiene que subir al piso alumbrandose con una cerilla para encontrarse con una desilusión enorme. Ell recuerda el piso como muy luminoso abierto y recibida por unos personajes fantasmagoricos. Aquí se rompe toda la ilusión de Andrea. 
- Juan: es el tío de Andrea, ejerce una gran violencia física y verbal hacia cualquier situación controvertida, todo esto viene provocado por Román. Ha perdido completamente el juicio. 
- Angustias: también es la tía de Andrea pero esta ejerce el papel de educadora de Andrea y le coarta los sueños de libertad en Barcelona. Tiene una moral muy alta pero falsa e hipocrita, ya que se jacta de ser muy cristiana mientras luego demuestra lo contrario.

Andrea ya se acaba de dar cuenta de que los sueños que tenía no se iban a cumplir porque no encuentra nada de lo que pensaba que iba a encontrar en Barcelona. A esto se le suma la constante vigilancia de la tía Angustias que no le dejará dar ni un paso sin supervisión.

Angustias sufre una crisis personal relacionada con Jeronimo Sanz (su jefe). Ella se ausenta de la casa de Aribau durante un tiempo y solo vuelve para anunciar que renuncia definitivamente a su vida pecaminosa para irse a vivir a un convento de clausura. 

Al irse Angustias, Andrea se siente liberada y declara que no va a compartir ninguna comida más en la casa de Aribau, va a intentar aislarse lo más posible de los habitantes de dicha casa. Esto comporta la recuperación de la ilusión de Andrea. Además con la marcha de Angustias, Andrea empieza a administrar su propia paga.

- Román: vive en la buhardilla de la casa pero desde allí arriba controla todos los hilos de la casa. 

Después de pasar por unas fiebres que pasa Andrea se da cuenta que necesita abrirse más o se va a convertir en un habitante de la casa de Aribau. Con esta decisión Andrea conoce a Ena y a Pons de la universidad.

## Segunda Parte

Esta parte está marcada por su independización de su familia y por las dificultades económicas

La independencia de la familia le proporciona a Andrea libertad para hacer lo que ella esperaba hacer en Barcelona, ell recupera la ilusión. Aún así, la nefasta administración de la pensión de Andrea hace que se gaste la pensión de Febrero en aquellas cosas que Andrea considera importante en ese momento. Se compra jabón, perfume, una blusa nueva y rosas para la madre de Ena.

Además con esta pensión paga su ración de pan diario y ella intenta pagar la cama, pero la abuelita no le deja. 

Sale de la casa de Margarita (la madre de Ena) bajo los efectos del alcohol y sale con una gran **Sed de belleza**. Decide entonces visitar la catedral de Barcelona. Durante ese momento se encuentra con Gerardo (un amigo de la familia de Ena), su presencia arruina ese momento místico. Gerardo actúa como el alter ego de Angustias, él se empeña en acompañarla a casa. 

Ena le dice a Andrea que tiene muchas cosas que hacer y que ya no podrán estudiar juntas. Después descubrimos que este alejamiento se debe a que Ena quiere acercarse a Román. Andrea también se aleja de Ena porque es facilmente irascible. Esto provoca que Andrea se sienta abandonada. 

En esta segunda parte Pons y Andrea se vuelven muy cercanos. A Pons, Andrea le parece una mujer distinta y él tiene mucho interès en presentarsela a sus amigos bohemios. 

En esta segunda parte se unen los tres mundos cuando Ena y Román vayan a cenar a un cabaret y Iturriaga se los encuentre.

En este segunda parte también conocemos más a fondo la historia entre Gloria, Juan y Román. 