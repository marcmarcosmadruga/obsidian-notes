# Tipos de 'se'

- Se -> Morfema verbal (arrepentir-se)

## Se sin función sintáctica

- Se como marca de:
	- Pasiva refleja
	- Impersonal refleja


### Pasiva Refleja
Una pasiva refleja es una variación de una pasiva perifrástica (no es lo mismo). 

_Maria compró los caramelos de San Valentín_ -> Maria es el agente porque es ella la que realiza la acción. 
_Los caramelos fueron comprados por Maria_ -> Maria sigue siendo el agente pero se convierte en un complemento agente. **Pasiva perifrástica**
_Se compraron los caramelos de San Valentín_ -> **Pasiva refleja** <- Tiene que aparecer obligatoriamente el sujeto paciente, nunca puede aparecer el agente.

**Se + V (3ª sing / 3ª plur) + Sujeto Paciente**

<br>

### Impersonal Refleja
_Se busca a los actores alemanes._

Nadie en concreto busca a los actores. Detrás está un sintagma preposicional (que no puede ser sujeto). En este caso _a los actores alemanes_ actúa como un complemento directo.

\*_Se buscan a los actores alemanes._
Es agramatical porque la impersonal refleja tiene un complemento que está concordando en número con el verbo, y los complementos directos nunca concuerdan con el verbo.

**Se + V (3ª sing / 3ª plur) + CD(siempre referido a persona)/CC.Modo\*/CC.Tiempo) **
\*Se está bien aquí