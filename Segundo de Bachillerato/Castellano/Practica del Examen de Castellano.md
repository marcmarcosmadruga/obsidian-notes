# Practica del Examen de Castellano

Mecanismos de cohesión:
- Léxicos:
	- Palabras clave
	- Sinónimos, antónimos
	- HIperónimos > Hipónimos
	- Campo Asociativo
	- Familia Lèxica
- Gramaticales:
	- Anáfora, catáfora
	- Deixis
- Marcadores del discurso.