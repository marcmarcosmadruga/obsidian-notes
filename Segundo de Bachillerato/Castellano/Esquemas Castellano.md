# Esquemas Castellano
## Verbo inergativo vs verbo inacusativo
- Verbo inergativo -> María corre
- Verbo inacusativo -> María crece

<hr>

## Perifrasis
- Perifrasis modales:
	- Obligación: *tener que, haber de, haber que, deber*
	- Probabilidad: *deber de, venir a*
	- Posibilidad: *poder*
- Perifrasis aspectuales:
	- Ingresivas: *pasar a, estar a punto de*
	- Incoativas: *comenzar a, empezar a*
	- Frecuentativas: *soler, acostumbrar a*
	- Reiterativas: *volver a*
	- Durativas: *estar + gerundio, andar + gerundio*
	- Terminativas: *abar de + inf, continuar + gerundio*

<hr>

## Locuciones
- Adjetiva: *de segunda mano*
- Adverbial: *a quemarropa*
- Nominal: *media naranja*
- Conjuntiva: *no lo hemos dado*
- Pronominal: *solo alguno que otro*
- Preposicional: *en medio de*
- Verbal:  *eché de menos*
- Interjectiva: *¡Dios mío!*

<hr>

## El Pronombre
- Personal: (*Tónicos, Átonos*)
- Demostrativo -> *eso, esto, aquello*
- Indefinido: *alguien, algo, nadie, nada*
- Interrogativo: *¿Qué has dicho?*
- Exclamativo: *¡Qué dices!*
- Relativo: *que*

<hr>

## Clases de verbos según su propiedades sintácticas
- Predicativo:
	- Transitivo + CD:
		- Ditransitivo (Con CI argumental)
	- Intransitivo:
		- Inergativo: sujeto agentivo: *gritar, comer, esquiar, depender, bostezar*
		- Inacusativo: sujeto no agentivo, **no admite pasiva** (*llegar, crecer, florecer*)

<hr>

## Clasificación de los predicados por su valencia
Predicado verbal por nº de participantes que selecciona:
- Avalente: *Llover, nevar, tronar*. --> Impersonales
- Monovalente: *Ladrar, andar, crecer, sonreír, aparecer*
- Bivalente:
	- Hacer/ver (con CD)
	- Acordarse de (con CRV)
- Trivalente: *Dar, dedicar, enviar*

<hr>

## Clases de adverbios por su significado
- Lugar (lejos)
- Tiempo:
	- Demostrativo -> *ayer/hoy*
	- Relativo -> *cuando*
	- Interrogativo -> *cuándo*
	- Cuantificativo -> *siempre*
- Modo: (*bien, así*)
- Cantidad/Grado: (*muy, basdtante, poco, nada + adjetivo*)
- Afirmación/Negación
- Duda

<hr>

## Adverbio de foco
Solo, incluso, también, hasta

<hr>

## Clases de Adverbios Oracionales
- Del acto de habla:
	- Hacia el emisor -> *honradamente*
	- Hacia el receptor -> *francamente*
- Del enunciado:
	- Evaluativo -> *afortunadamente*
	- Modal -> *obligatoriamente*
	- Evidencial -> *supuestamente, obviamente*
- De tópico:
	- De marco espacio-temporal: *ayer*
	- De punto de vista: *económicamente*


<hr>

## Características de los textos expositivos
- Vocabulario especifico
- Elementos referenciales
- Objetividad máxima
- Léxico denotado
- 3ª persona e impersonales
- Orden deductivo o inductivo

<hr>

## Adjetivos
- Especificativos: el chico *francés* -> no admite grado.
- Calificativos: el coche grande -> admite grado (el coche muy grande).
- Determinativos: **tengo que preguntar a la Ana**
- Adverbiales: la presunta asesina -> no admite grado.
