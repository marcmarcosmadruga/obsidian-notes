# Clase Ana
Adjetivo explicativo(**Tiene que estar complementando un nombre, detrás del nombre y SIN COMAS**), además se pueden graduar. No puede estar detrás de un verbo copulativo, porque en ese caso es atributo. 

Adjetivo relacional: El problema *nacional*, no se pueden graduar. 

Nombre que derive de un adjetivo -> Agradable.

Repasar diferentes tipos de adverbio:
- Adverbio relativo: donde, cuando, como. *La plaza donde te conocí*
- Adverbio de foco: solo, también, incluso

Diferencias entre inergativos, inacusativos y transitivos.

Los intransitivos no seleccionan nunca complemento directo y por tanto no pueden formar pasivas (pero sí impersonales). 

Los verbos inacusativos tienen un participio que puede funcionar como complemento de un nombre:
Los verbos inergativos no tienen un participio que pueda funcionar como complemento de un nombre:
 > La niña reída
 > La niña desaparecida

Inergativo -> realiza voluntariamente la acción.
Inacustaivo -> le sucede la acción.

El verbo comer si no lleva complemento es un verbo inergativo:

> Maria ya ha comido
> Maria ya ha comido macarrones

Hay verbos que al añadirles se se vuelven intransitivos: hundir -> hundir-se.

Solo entran los dos se de pasiva refleja y de impersonal.

Se venden pisos -> Pasiva refleja, porque el verbo está en plural.
Se busca a la empleada -> Impersonal refleja porque después del a viene un CD y al no ser un sujeto es una impersonal refleja.

Si lleva un CC siempre es impersonal y si lleva a + persona detrás es impersonal. Si detrás es un Sintagma Nominal que al pasarlo a plural cambia, es una pasiva refleja. 

Tipos de marcadores discursivos para el texto expositivo. Adjetivos relacional o calificativo. Palabras clave. 

Para distinguir entre un adjetivo determinativo y un determinante, el determinante si le quitas el nombre sigue conservando el sentido, pero el adjetivo determinativo no.

#importante Los verbos inacusativos no seleccionan dos argumentos y por tanto no pueden formar pasivas. 

- Verbo inergativo
- Adjunto
- C.Predicativo
- Adverbio relativo

La plaza donde corrí ayer tenia la entrada barata.