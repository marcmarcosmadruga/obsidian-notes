# La Anarquia

La Anarquía es un proyecto político que se originó en la Antigua Grecia basado en la ausencia de Estado y de cualquier otro poder impuesto a los ciudadanos de una zona. 

Sin embargo, no fue hasta el siglo XVIII gracias al movimiento de la Ilustración que gracias a Rousseau se recuperó el concepto de anarquía. Él defendía que la raza humana no es mala por naturaleza, sino que es el Estado el que la hace nociva, y por ello quería acabar con el Estado y con cualquier otro tipo de poder contra el pueblo.

Aunque Rousseau sentó las bases del anarquismo moderno, Mijaíl Bakunin fue su máximo exponente. Él creía que la mejor forma de convivencia era vivir en pequeños grupos, ya que en estos sería imposible la aparición de clases sociales, clases las cuales él quería combatir. Él pensaba que estas comunas eran el principio, pero él pensaba que a partir de estas se podría llegar a implementar el anarquismo a nivel mundial.

En la actualidad el anarquismo se ha desglosado en múltiples tendencias diferentes como pueden ser: el anarcosindicalismo, anarcocapitalismo y el anarcoecologismo.

En resumen, la anarquía plantea una sociedad en la que el pueblo no está gobernado por ningún Estado ni regido por ningún poder más allá de la propia ética del individuo.