# Resumen Castellano 31-01

## El verbo
Los verbos exigen participantes para poder tener sentido.
- Avalentes: amanecer, llover y tronar
- Monovalentes: dormir, pestañear, bostezar
- Bivalentes: beber, comer
- Trivalentes: dar, enviar, colocar, poner (la mayoría)

Argumentos: sujeto, complemento directo, complemento de régimen verbal, complemento indirecto, complemento locativo argumental. Ejemplo en [[El Verbo]]. Los complementos locativo argumental y complemento argumental de medida son en realidad complementos circunstanciales de lugar y de cantidad.

Adjuntos: complemento indirecto (cuando no es selecionado por el verbo), complementos circunstanciales, complementos agentes. 

Ejemplo de un par mínimo en [[El Verbo]]

## El adverbio
### Adverbios pronominales
- Demostrativo de lugar: aquí, allí, ahí
- Demostrativo de tiempo: hoy, ayer, mañana
- Demostrativo de cantidad: tanto
- Demostrativo de modo: así (de este modo/de esta manera)
- Relativo: donde, cuando, como, cuanto (el lugar donde nací)

### Adverbio adjetival
- Los aviones vuelan alto.
- La profesora habla claro.

Adverbio bloqueado en masculino singular. Se puede sustituir por un -mente a veces (claro --> claramente).

 ### Adverbio interrogativo
 - Dime cuándo vendrás
 - Miguel me preguntó dónde estabas.
 
 ### Adverbio de foco
 - Vino **incluso** Ana.
 - Los turistas **también** visitaron el museo.


### Adverbio oracional
- Sinceramente, ví como el asesino cometía el crimen.
- Legalmente, sus acciones son incorrectas.
- Eticamente, no actúo del a mejor manera.
 