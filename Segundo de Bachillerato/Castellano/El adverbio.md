# El Adverbio
## Que es lo importante?
Adverbio:
- Adverbio Pronominal
- Demostrativo
- Adjetival
- Relativo
- De foco

### Adverbio pronominal 
- Demostrativo de lugar, tiempo cantidad y modo: *aquí, mañana, tanto, así.*

#### Relativo (donde, cuando, como, cuanto)
- *El lugar donde nací*. (Introduce oraciones de relativo)


#### Adverbio adjetival
- Complemento de un verbo, es un adjetivo bloqueado en masculino singular. Muchos se pueden sustituir por un adverbio acabado en -mente (pero no siempre).
	- Los aviones vuelan alto.
	- La profesora habla claro.


### Adverbio Interrogativo
- Dime cuándo vendrás
- Miguel me preguntó dónde estabas.


### Adverbio de foco
- Adverbio que incide sobre una palabra concreta del enunciado para destacarla del resto: solo, incluso, también, hasta.
- *Vino incluso Ana al cine.*
-  Los turistas también visitaron el museo (ambiguo) -> Los turistas visitaron también el museo (desecha la amigüedad).

#### Lectura ambigua ejemplo
a) Compré la fruta barata.
- barata puede ser tanto complemento nominal de fruta o como complemento predicativo del complemento directo.

Para deshacer la amigüedad podemos cambiarlo a: *Compré barata la fruta*. En este caso barata es un complemento predicativo del complemento directo.

#### Par mínimo sobre adverbios de focos
a) Los turistas también visitaron el museo.
b) Los turistas visitaron también el museo.

En *a* el adverbio de foco incide sobre la acción mientras que en *b* el complemento de foco incide en el complemento directo. En *a* los turistas además de hacer otras cosas también visitaron el museo mientras que en *b* **visitaron** otras cosas y también el museo.
<br>

a) Solo como macarrones con tomate.
b) Come macarrones solo con tomate.
c) Solo Juan come macarrones con queso.

En *a* el adverbio de foco incide en el complemento directo mientras que en *b* incide sobre el complemento del nombre. En *a* significa que la unica comida que come son macarrones con tomate mientras que en *b* significa que cuando come tomate, solo los come si llevan tomate. En *c* se expresa que Juan es la única persona que come macarrones con queso.

### Adverbio oracional
Un adverbio oracional es el que está fuera de la oración porque afecta a toda la oración expresando una actitud del hablante o una valoración del hablante sobre el hecho anunciado.

a) *Sinceramente, la profesora nos habló*
b) *la profesoa nos habló sinceramente*

En la oración a, sinceramente es una adverbio oracional (ya que está fuera de la oración) mientras que en b, sinceramente es un complemento circunstancial de modo (adverbio) ya que incide sobre la accion. En la primera sinceramente define la actitud del hablante mientras que en b dice como les habló la profesora.